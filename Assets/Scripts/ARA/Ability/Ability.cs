﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class Ability : MonoBehaviour
    {

        [HideInInspector]
        private Character _parentCharacter;
        [HideInInspector]
        public bool isPlayerAbility;

        [HideInInspector]
        public string id;

        public Character parentCharacter {
            get {
                return this._parentCharacter;
            }

            set {
                this._parentCharacter = value;
                this.isPlayerAbility  = this._parentCharacter.gameObject.layer == LayerMask.NameToLayer("Player"); 
            }
        }

        public void Start() {
            
		}

        public virtual int Exec() {

            return 0;
        }

        public virtual void ForceStop() {

		}
    }
}