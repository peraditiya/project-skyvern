﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilityGrappling : Ability
    {

        public WeaponGrappler grappler {get; private set;}
        public AbilitySword sword {get; private set;}
        private IEnumerator ropeRoutine;
        
        void Awake() {
            
            this.id = "Grappling";
        }

        // Use this for initialization
        void Start()
        {
            base.Start();

            this.grappler = this.parentCharacter.GetComponentInChildren<WeaponGrappler>();

            this.sword = this.parentCharacter.GetAbility<AbilitySword>("Sword");
        }

        public override int Exec()
        {

            int ret = Character.CHARACTER_STATE_IDLE;

            if (!this.parentCharacter.states.isGrappling && !this.parentCharacter.states.isHangingOnTheWall)
            {

                if (this.parentCharacter.input.GetButtonDown("Grap"))
                {

                    Vector2 direction = new Vector2(this.parentCharacter.input.GetAxisRaw ("Move Horizontal"), this.parentCharacter.input.GetAxisRaw ("Move Vertical"));

                    if (direction.x == 0 && direction.y == 0) direction = new Vector2(this.parentCharacter.states.isFacingRight ? 1 : -1, 0);

                    this.grappler.direction = direction;

                    this.grappler.Activate();

                    if (this.ropeRoutine == null)
                    {
                        this.ropeRoutine = this.OnRopeActive();
                        this.StartCoroutine(this.ropeRoutine);
                    }
                }
            }
            else
            {

                ret = this.parentCharacter.states.isJump || this.parentCharacter.states.isHooked ? Character.CHARACTER_STATE_ACTIVE : Character.CHARACTER_STATE_ACTIVE_BREAK;
            }

            return ret;
        }

        IEnumerator OnRopeActive()
        {

            while (true)
            {
                if (this.parentCharacter.states.isSwing || this.parentCharacter.states.isRetract)
                {

                    if (this.parentCharacter.input.GetButtonDown("Grap")) this.Retract();

                }
                else if (this.parentCharacter.states.isHanging)
                {
                    float horizontalInput = this.parentCharacter.input.GetAxisRaw ("Move Horizontal");
                    float verticalInput = this.parentCharacter.input.GetAxisRaw ("Move Vertical");

                    if (this.parentCharacter.input.GetButtonDown("Grap")) this.Retract();
                    else if (verticalInput == -1) this.grappler.Crawl();
                    else if (verticalInput == 1) this.grappler.Crawl(true);
                    else if (horizontalInput == 1) this.grappler.Push();
                    else if (horizontalInput == -1) this.grappler.Push(true);

                    this.parentCharacter.PlayAnim ("Aria_Hanging", 0, true);
                }
                else if (this.parentCharacter.states.isHangingOnTheWall)
                {
                    float verticalInput = this.parentCharacter.input.GetAxisRaw ("Move Vertical");
                    
                    if (verticalInput == -1) this.grappler.Crawl();
                    else if(this.sword != null) this.sword.Exec();

                    if(!this.parentCharacter.states.isAttacking) this.parentCharacter.PlayAnim ("Aria_Hanging", 0, true);
                }

                this.ropeRoutine = null;

                yield return new WaitForFixedUpdate();
            }
        }

        public void Retract()
        {
            this.parentCharacter.states.isRetract = true;

            if (this.parentCharacter.states.isHanging) this.parentCharacter.states.isHanging = false;
            else if (this.parentCharacter.states.isSwing) this.parentCharacter.states.isSwing = false;

            this.grappler.ropeLength = 0f;
            this.grappler.Hook();
        }

        public void Stop() {

            this.StopCoroutine(this.ropeRoutine);
            this.ropeRoutine = null;
        }
    }
}