﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilityJump : Ability
    {
        private IEnumerator jumpRoutine;
        private int indexBase = 0;

        [HideInInspector]
        public int index = 0;

        public float height = 5f;
        public float speed = 0.6f;

        [HideInInspector]
        public float modifier = 1f;

        private bool isJumpButtonHolded;

        AbilityGrappling abilityGrappling;

        void Awake() {
            
            this.id = "Jump";
        }

        void Start()
        {
            base.Start();

            float jumpCumulative = 0f;

            this.isJumpButtonHolded = false;

            while (jumpCumulative < this.height)
            {

                this.indexBase++;

                jumpCumulative += (this.speed * this.indexBase);
            }

            this.abilityGrappling = this.parentCharacter.abilities.ContainsKey("Grappling") ? this.parentCharacter.abilities["Grappling"] as AbilityGrappling : null;
        }

        public override int Exec()
        {
            
            if (!this.parentCharacter.states.isJump)
            {
                if (this.parentCharacter.input.GetButtonDown("Jump"))
                {   
                    this.isJumpButtonHolded = true;

                    this.index = this.indexBase;

                    //if(this.parentCharacter.states.isHooked) if(this.parentCharacter.input.GetButtonDown("Jump")) this.index = 0;

                    this.Trigger(this.index);
                }
            } 
            
            if (this.parentCharacter.input.GetButtonUp("Jump") && this.index > 0) this.index = 0;
                
            return this.parentCharacter.states.isJump ? Character.CHARACTER_STATE_ACTIVE : Character.CHARACTER_STATE_IDLE;
        }

        public void Trigger(int index = -100) {

            this.parentCharacter.states.isJump = true;

            this.index = index == -100 ? this.indexBase : index;

            if(this.parentCharacter.states.isHooked) this.abilityGrappling.grappler.DeActivate();

            this.jumpRoutine = this.Jump();
            this.StartCoroutine(this.jumpRoutine);
        }

        IEnumerator Jump()
        {
            if(this.isPlayerAbility) this.parentCharacter.PlayAnim("Aria_Jump Up");

            this.parentCharacter.rigidbody.gravityScale = 0;
            
            float verticalMovement;

            while (this.parentCharacter.states.isJump)
            {

                verticalMovement = this.speed * this.index * this.modifier;

                if (this.index <= 0)
                {

                    this.parentCharacter.states.isFalling = true;
                    
                    if(this.isPlayerAbility && !this.parentCharacter.states.isAttacking) this.parentCharacter.PlayAnim("Aria_Jump Down");
                
                    float scanDistance = Mathf.Abs(verticalMovement) + 0.05f;
                    RaycastHit2D hit = Helper.HitScan(this.parentCharacter.body, Vector2.down, scanDistance, "Ground");
                    
                    if (hit.collider != null)
                    {
                        verticalMovement = 0;

                        this.parentCharacter.body.y -= hit.distance;

                        this.JumpStop();
                    }
                }

                this.parentCharacter.body.y += verticalMovement;
                
                this.index--;

                yield return new WaitForFixedUpdate();
            }
        }

        public void JumpStop()
        {
            if(this.jumpRoutine != null) this.StopCoroutine(this.jumpRoutine);
            
            this.parentCharacter.states.isJump = false;
            this.parentCharacter.states.isFalling = false;
            this.parentCharacter.states.isHit = false;
            this.parentCharacter.rigidbody.gravityScale = this.parentCharacter.gravityScale;
            
            if(this.parentCharacter.states.isGrappling) {
                
                this.parentCharacter.GetAbility<AbilityGrappling>("Grappling").grappler.DeActivate();
            }

            //if(this.isPlayerAbility) this.parentCharacter.animator.SetBool("isGrounded", true);

            //Debug.Log(this.parentCharacter.animator.GetBool("isGrounded"));
        }

        public void JumpBreak()
        {
            this.modifier = 0.1f;
        }

        public void JumpResume()
        {
            this.modifier = 1f;
        }
    }
}