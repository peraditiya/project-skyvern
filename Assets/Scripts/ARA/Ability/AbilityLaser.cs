﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilityLaser : Ability
    {
		WeaponLaser laser;

		void Awake() {
            
            this.id = "Laser";
        }

        // Use this for initialization
        void Start()
        {
            base.Start();

			Transform tgo = this.parentCharacter.transform.Find("Laser");
			if(tgo != null) this.laser = tgo.gameObject.GetComponent<WeaponLaser>();
        }

		public override int Exec()
        {
			if(!this.parentCharacter.states.isCasting) {

				if(Input.GetKey(ControlMap.KEY_SPELL)) {

					this.parentCharacter.animator.SetBool("laser", true);
					this.parentCharacter.states.isCasting = true;
					
					Debug.Log(this.parentCharacter.animator.GetBool("laser"));

					this.laser.Shoot(this.parentCharacter.body.renderer.flipX);	
				}
			
			} else {

				if(Input.GetKeyUp(ControlMap.KEY_SPELL)) {
					
					this.parentCharacter.animator.SetBool("laser", false);
					this.parentCharacter.states.isCasting = false;

					this.laser.StopShoot();
				}
			}

			return this.parentCharacter.states.isCasting ? Character.CHARACTER_STATE_ACTIVE_BREAK : Character.CHARACTER_STATE_IDLE;
		}
    }
}