﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilityShield : Ability
    {

		WeaponShield shield;

		private IEnumerator shieldRoutine;

		void Awake() {
            
            this.id = "Shield";
        }

        // Use this for initialization
        void Start()
        {
			base.Start();	

			this.id = "Shield";

			Transform tgo = this.parentCharacter.transform.Find("Shield");
			if(tgo != null) {
				this.shield = tgo.gameObject.GetComponent<WeaponShield>();
				this.shield.activator = this;
			}
		}

        // Update is called once per frame
        public override int Exec()
        {
			if(!this.parentCharacter.states.isBlock && !this.parentCharacter.states.isHit && !this.parentCharacter.states.isHooked) {

				if(this.parentCharacter.input.GetButtonDown("Shield")) {
				
					this.parentCharacter.states.isBlock = true;

					this.parentCharacter.PlayAnim("Aria_Shield");

					this.shieldRoutine = this.Shield();
					this.StartCoroutine(this.shieldRoutine);
				} 
			
			} else {

				if(this.parentCharacter.input.GetButtonUp("Shield")) {

					this.parentCharacter.states.isBlock = false;
				}
			}

			return this.parentCharacter.states.isBlock ? Character.CHARACTER_STATE_ACTIVE_BREAK : Character.CHARACTER_STATE_IDLE;
        }

		private IEnumerator Shield() {
            
			yield return new WaitForSeconds(0.2f);

			this.shield.Activate();

			while(true) {

				if(this.parentCharacter.states.isBlock == false) {
					
					this.shield.Release();

					yield return new WaitForSeconds(0.1f);

					this.shield.DeActivate();

					break;
				}

				yield return null;
			}
        }

		public override void ForceStop() {

			this.parentCharacter.states.isBlock = false;

			//this.parentCharacter.animator.SetInteger("shield", 0);

			this.StopCoroutine(this.shieldRoutine);
		}
    }
}