﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilitySword : Ability
    {

        private float lastComboTime;
        private int currentCombo;
        public int comboCount = 0;
        public float comboAnimationTresshold = .7f;
        
        private float animStart = 0;
        
        public float animSpeedMultiplier = 2f;
        public float[] attackAnimTime = new float[] { 0.444f, 0.5f, 0.611f };
        
        private bool coyoteAttack = false;
        public float coyoteTime = .2f;

        private IEnumerator attackRoutine;

        [HideInInspector]
        public WeaponSword sword { get; private set; }
        // Use this for initialization

        void Awake() {
            
            this.id = "Sword";
        }

        void Start()
        {
			base.Start();

            Transform tgo = this.parentCharacter.transform.Find("Sword");
            if(tgo != null) this.sword = tgo.GetComponent<WeaponSword>();
        }

        // Update is called once per frame
        public override int Exec()
        {
            
            if (Time.time > lastComboTime + comboAnimationTresshold) currentCombo = 1;

            if (this.parentCharacter.input.GetButtonDown("Attack"))
            {
                lastComboTime = Time.time;

                if (this.parentCharacter.states.isAttacking)
                {
                    if (Time.time > animStart + attackAnimTime[currentCombo - 1] - coyoteTime && !coyoteAttack)
                    {
                        coyoteAttack = true;
                        comboCount++;
                    }
                }
                else
                {
                    comboCount++;
                }

            }

            if (comboCount > 0 && !this.parentCharacter.states.isAttacking)
            {
                this.parentCharacter.states.isAttacking = true;
                animStart = Time.time;
                coyoteAttack = false;
                
                this.attackRoutine = this.Attack();
                this.StartCoroutine(this.attackRoutine);
            }

            return this.parentCharacter.states.isAttacking ? (this.parentCharacter.states.isJump ? Character.CHARACTER_STATE_ACTIVE : Character.CHARACTER_STATE_ACTIVE_BREAK) : Character.CHARACTER_STATE_IDLE;
        }

        private IEnumerator Attack() {
            
            comboCount--;
                
            if(this.parentCharacter.states.isJump || this.parentCharacter.states.isHooked) this.parentCharacter.PlayAnim("Aria_Aerial_Attack0"+currentCombo);
            else { this.parentCharacter.PlayAnim("Aria_Attack_Combo0"+currentCombo); }

			this.sword.Activate(currentCombo);
            yield return new WaitForSeconds(this.sword.animationDelay[currentCombo - 1]);
                
            currentCombo++;
            if (currentCombo > this.sword.maximumCombo) currentCombo = 1;
            this.parentCharacter.states.isAttacking = false;

            yield return new WaitForFixedUpdate();
        }
    }
}