﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class AbilityWalk : Ability
    {

        public float speed;

        void Awake() {
            
            this.id = "Walk";
        }

        // Use this for initialization
        void Start()
        {
            base.Start();

            this.id = "Walk";
        }

        public override int Exec()
        {

            int ret = 0;
            this.parentCharacter.movement.x = this.parentCharacter.input.GetAxisRaw ("Move Horizontal");
            
            this.parentCharacter.movement.x *= this.speed;

            ret = this.parentCharacter.movement.x != 0 ? Character.CHARACTER_STATE_ACTIVE : Character.CHARACTER_STATE_IDLE;

            //this.parentCharacter.animator.SetFloat("velocity_x", Mathf.Abs(this.parentCharacter.rigidbody.velocity.x));

            if(ret == Character.CHARACTER_STATE_ACTIVE && !this.parentCharacter.states.isHooked) {
                
                this.parentCharacter.isFacingRight = this.parentCharacter.movement.x > 0;
            
                if(this.isPlayerAbility && !this.parentCharacter.states.isJump && !this.parentCharacter.states.isHooked) this.parentCharacter.PlayAnim("Aria_Run");
            } 

            return ret;
        }
    }
}