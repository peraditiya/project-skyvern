﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

namespace ARA
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]

    public class Character : MonoBehaviour
    {

        public static int CHARACTER_STATE_IDLE = 0;
        public static int CHARACTER_STATE_ACTIVE = 1;
        public static int CHARACTER_STATE_ACTIVE_BREAK = 2;

        [HideInInspector]
        public Rigidbody2D rigidbody;

        [HideInInspector]
        public Body body;

        [HideInInspector]
        public Body visualBody;

        [HideInInspector]
        public Vector3 movement;
        public Dictionary<string, ColliderBound> colliderBounds;
        public ColliderBound previousColliderBound;

        protected IEnumerator onCharacterPlayRoutine;
        protected IEnumerator onCharacterFixedUpdateRoutine;

        public Animator animator { get; private set; }

        public CharacterStates states { get; private set; }

        public Dictionary<string, Ability> abilities { get; private set; }

        public float gravityScale { get; private set; }

        public Rewired.Player input;

        public bool isFacingRight {
            get { return this.states.isFacingRight; }
            set {

                this.states.isFacingRight = value;
                
                Vector3 currentScale = this.visualBody.transform.localScale;
                currentScale.x = Mathf.Abs(currentScale.x) * (this.states.isFacingRight ? -1 : 1); 

                this.visualBody.transform.localScale = currentScale;
            }
        }

        // Use this for initialization
        public virtual void Awake()
        {
            this.rigidbody = this.GetComponent<Rigidbody2D>();
            this.body = new Body(this.gameObject);

            this.colliderBounds = new Dictionary<string, ColliderBound>();
            
            this.colliderBounds.Add("Default", new ColliderBound{ size = this.body.collider.size, offset = this.body.collider.offset, adjustment = Vector3.zero });
            
            this.colliderBounds.Add("Aria_Hanging", new ColliderBound{ size = this.body.collider.size, offset = new Vector2(-0.4f, 1.4f), adjustment = new Vector3(0.45f, 0, 0) });

            Transform tgo = this.transform.Find("Visual");
            if (tgo != null) this.visualBody = new Body(tgo.gameObject);
            
            this.abilities = new Dictionary<string, Ability>();
            
            foreach (Ability ability in this.GetComponents<Ability>()) {

                if(!ability.enabled) continue;

                ability.parentCharacter = this;
                this.abilities.Add(ability.id, ability);
            } 

            if (this.visualBody != null)
            {

                this.animator = this.visualBody.gameObject.GetComponent<Animator>();
                //this.body.renderer = this.visualBody.renderer;
            }
            else
                this.animator = this.GetComponent<Animator>();

            this.states = new CharacterStates();

            this.input = ReInput.players.GetPlayer (0);
        }

        public virtual void Start()
        {
            this.gravityScale = this.rigidbody.gravityScale;

            this.onCharacterPlayRoutine = this.OnCharacterPlay();
            this.StartCoroutine(this.onCharacterPlayRoutine);

            this.onCharacterFixedUpdateRoutine = this.OnCharacterFixedUpdate();
            this.StartCoroutine(this.onCharacterFixedUpdateRoutine);
        }

        public virtual IEnumerator OnCharacterPlay()
        {
            yield return null;
        }

        public virtual IEnumerator OnCharacterFixedUpdate()
        {
            yield return null;
        }

        public virtual void OnCharacterHit(float damage = 10) {
            
           if(!this.states.isInvisible && !this.states.isHit) {

               this.states.isHit = true;

               this.animator.SetBool("isHit", true);
           }
		}

        public void PlayAnim (string animName, float normalizedTime = float.NegativeInfinity, bool force = false) {

            if (this.animator.GetCurrentAnimatorStateInfo (0). IsName (animName) && !force) return;

            //ColliderBound tColliderBound = this.colliderBounds["Default"];
            //if(this.colliderBounds.ContainsKey(animName)) tColliderBound = this.colliderBounds[animName];
            
            //this.body.collider.size = tColliderBound.size;
            //this.body.collider.offset = tColliderBound.offset;

            //this.previousColliderBound = tColliderBound;

            this.animator.Play (animName, 0, normalizedTime);     
        }

        public T GetAbility<T>(string abilityId) where T : class {
            
            return this.abilities.ContainsKey(abilityId) ? this.abilities[abilityId] as T : null;
        }
    }

    [System.Serializable]
    public struct ColliderBound
    {
        public Vector2 size;
        public Vector2 offset;
        public Vector3 adjustment;
    }
}