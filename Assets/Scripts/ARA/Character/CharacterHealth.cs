﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class CharacterHealth : MonoBehaviour
    {
        public UIPlayer uiPLayer;
        public float maxHealth = 100f;
        public float currentHealth = 0;
        public bool isDead = false;

        void Start()
        {
            currentHealth = maxHealth;
        }

        public bool Hit(float damage)
        {
            currentHealth -= damage;
            if(uiPLayer)
                uiPLayer.UpdateHealthBar(currentHealth, maxHealth);

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Heal(float amount)
        {
            currentHealth += amount;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
        }

        public void Reset()
        {
            currentHealth = maxHealth;
        }
    }
}
