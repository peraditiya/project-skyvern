﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA {

    public class CharacterStates {

        public bool isIdle;
        public bool isWalk;
        public bool isJump;
        public bool isAttacking;
        public bool isGrappling;
        public bool isRetract;
        public bool isHangingOnTheWall;
        public bool isHanging;
        public bool isFacingBack;
        public bool isSwing;
        public bool isHit;
        public bool isHitEnemy;
        public bool isFalling;
        public bool isFreeze;
        public bool isBind;
        public bool isBound;
        public bool isPulling;
        public bool isPulled;
        public bool isBlock;
        public bool isParry;
        public bool isAirStop;
        public bool isInvisible;
        public bool isKnocked;

        public bool isBossRide = false;

        public bool isJumpDown; //basically same as isFalling but it triggered by user initiate (jump + down)

        public bool isFacingRight;

        public bool isCasting;

        //for enemy
        public bool isOnTask;

        public CharacterStates() { }

        public void Reset() {

            this.isJump = this.isAttacking = this.isGrappling = this.isRetract = this.isHangingOnTheWall = this.isFacingBack = this.isHit  = this.isBind = this.isBound = this.isPulling = this.isPulled = this.isFreeze = this.isKnocked = this.isAirStop = this.isFacingRight = this.isCasting = false;
        }

        /* 
        public bool isIdle {

            get {
                return !this.isAttacking && !this.isJump && !this.isGrappling && !this.isRetract && !this.isHanging && !this.isSwing && !this.isHit && !this.isFalling && !this.isBound && !this.isPulling && !this.isPulled && !this.isKnocked && !this.isAirStop;
            }
        }
        */

        public bool isHooked {

            get {
                return this.isRetract || this.isHangingOnTheWall || this.isHanging || this.isSwing;
            }
        }
        
        /* 
        public void Debug() {

            Debug.Log("===========================================");
            Debug.Log("jump " + "= " + this.isJump);
            Debug.Log("jumpDown " + "= " + this.isJumpDown);
            Debug.Log("walk " + "= " + this.isWalk);
            Debug.Log("wttack " + "= " + this.isAttacking);
            Debug.Log("grappling " + "= " + this.isGrappling);
            Debug.Log("retract " + "= " + this.isRetract);
            Debug.Log("swing " + "= " + this.isSwing);
            Debug.Log("hanging " + "= " + this.isHanging);
            Debug.Log("hangingOnTheWall " + "= " + this.isHangingOnTheWall);
            Debug.Log("===========================================");

        }
        */
    }
}