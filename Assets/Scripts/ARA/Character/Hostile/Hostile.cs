﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class Hostile : Character
    {
        public void Awake()
        {
            base.Awake();
		}

        // Use this for initialization
        public void Start()
        {
			base.Start();

            Physics2D.IgnoreCollision(this.body.collider, Root.player.body.collider, true);

            Root.room.hostiles.Add(this.name, this.GetComponent<Hostile>());
		}
		
        public override void OnCharacterHit(float damage = 10) {

			Debug.Log("waaa");
		}

        // Update is called once per frame
    }
}