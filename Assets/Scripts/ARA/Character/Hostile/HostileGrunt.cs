﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class HostileGrunt : Hostile {

		public enum State
        {
            Idle, Walk, Hit, Attack, Dead
        }
		
		public struct Edge
        {
            public float left;
            public float right;
        }

        public State state;
        public GameObject blood;
        public GameObject ragdoll;
        [Header("Properties")]
        public float health = 100f;
        
        [Header("Walk Setting")]
        public float walkSpeed;

        [Header("Attack Setting")]
        [Range(5, 100)]
        public float attackChance = 50f;
        public float distanceToAttack = 5f;

        [HideInInspector]
        public Transform target;

        [HideInInspector]
        public Transform _transform;
        private Rigidbody2D _rigidbody;
        private SpriteRenderer[] _sprites;

        [HideInInspector]
        public float startingX;

        UnityEngine.UI.Text stateText;

        private Vector2 destination;
        private Coroutine currentCoroutine;

        private bool isShielded = false;

        private Color[] startColor;
        private bool isTinting = false;

        private WeaponBlunt blunt;

		public void FaceSprite()
        {
            this.states.isFacingRight = _transform.position.x < target.position.x ? true : false;
            // print("fac"+facingRight+" "+_transform.position.x+" "+target.position.x);
            _transform.localScale = this.states.isFacingRight ? new Vector2(1f, _transform.localScale.y) : new Vector2(-1f, _transform.localScale.y);
        }
        void Awake()
        {
			base.Awake();
			
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody2D>();
            startingX = _transform.position.x;
            stateText = GetComponentInChildren<UnityEngine.UI.Text>();
            _sprites = GetComponentsInChildren<SpriteRenderer>();

            this.blunt = this.transform.Find("Attack").gameObject.GetComponent<WeaponBlunt>();
        }

        void Start()
        {
			base.Start();

            target = Root.player.transform;
            startColor = new Color[_sprites.Length];
            for (int i = 0; i < startColor.Length; i++)
            {
                startColor[i] = _sprites[i].color;
            }
            StartCoroutine("Base");
	    }

        void ChangeState(State newState)
        {
            state = newState;
            if (currentCoroutine != null)
                StopCoroutine(currentCoroutine);
            currentCoroutine = StartCoroutine(newState.ToString());
        }
        public void ChangeText(string newText, Color textColor)
        {
            stateText.text = newText;
            stateText.color = textColor;
        }

        IEnumerator Base()
        {
            ChangeState(state);
            yield return null;
        }

        IEnumerator Idle(){
            ChangeText(state.ToString(), Color.green);
            this.animator.Play("idle");
            FaceSprite();
            
            while (state == State.Idle)
            {

                float idleTime = Random.Range(1f, 3f);
                yield return new WaitForSeconds(idleTime);
                // decide
                if(Mathf.Abs(_transform.position.x - target.position.x) <= distanceToAttack){
                    // attack chance
                    if(Random.Range(0, 101) < attackChance){
                        // attack
                        print("attack");
                        ChangeState(State.Attack);
                    }else{
                        print("not attack");
                    }
                }else{
                    ChangeState(State.Walk);
                }
            }
        }

        IEnumerator Walk()
        {
            ChangeText(state.ToString(), Color.yellow);
            this.animator.Play("walk");
			FaceSprite();
            float walkDistance = Random.Range(2f, 5f) * (this.states.isFacingRight ? 1 : -1);
            float destX = _transform.position.x + walkDistance;
            int dir = this.states.isFacingRight ? 1 : -1;
            while (state == State.Walk)
            {
                if(Mathf.Abs(_transform.position.x - destX)>.3f){
                    // _transform.Translate(Vector3.right * walkSpeed * dir * Time.deltaTime);
                    _rigidbody.velocity = new Vector2(walkSpeed * dir, _rigidbody.velocity.y);
                    yield return null;
                }else{
                    ChangeState(State.Idle);
                }
            }
        }

        IEnumerator Attack()
        {
            ChangeText(state.ToString(), Color.yellow);
            this.animator.Play("attack");

            while (state == State.Attack)
            {
                // attacking

                this.blunt.Activate();

                yield return new WaitForSeconds(2f);
                ChangeState(State.Idle);
            }
        }

        IEnumerator Hit(){
            ChangeText(state.ToString(), Color.grey);
            GameObject go = Instantiate(blood, _transform.position, Quaternion.Euler(-90,0,0));
            Destroy(go, 2f);
            this.animator.Play("hit");
            int dir = _transform.position.x > target.position.x ? 1 : -1;
            _rigidbody.position = _rigidbody.position + new Vector2(.2f * dir, 0) ;
            
            while (state == State.Hit)
            {
                yield return new WaitForSeconds (.5f);
                ChangeState(State.Idle);
            }
        }

        IEnumerator Dead(){
            ChangeText(state.ToString(), Color.black);
            while (state == State.Dead)
            {
                GameObject go =  Instantiate(ragdoll, _transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<BreakableRadgoll>().Explode(_transform.position.x > target.position.x ? true : false);
                Destroy(this.gameObject);
                yield return null;
            }
        }
        

        IEnumerator Tint(){
            isTinting = true;
            while(_sprites[0].color != startColor[0]){
                for (int i = 0; i < _sprites.Length; i++)
                {
                    _sprites[i].color = Color.Lerp(_sprites[i].color, startColor[i], Time.deltaTime * 3);    
                }
                
                yield return null;
            }
            isTinting = false;
        }

		public override void OnCharacterHit(float damage = 10) {

			health -= 40;
            if(health <= 0){
                health = 0;
                ChangeState(State.Dead);
            }
            ChangeState(State.Hit);
            // _sprite.color = Color.white;
            for (int i = 0; i < _sprites.Length; i++)
            {
                _sprites[i].color = Color.white;
            }
            if(!isTinting){
                StartCoroutine("Tint");
            }
		}
    }
}