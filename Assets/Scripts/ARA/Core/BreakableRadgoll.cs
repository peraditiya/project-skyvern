﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class BreakableRadgoll : MonoBehaviour
    {


        private Rigidbody2D[] _rigidbodies;

        void Awake()
        {
            _rigidbodies = GetComponentsInChildren<Rigidbody2D>();
        }

        // void OnEnable(){
        // 	Explode(true);
        // }

        public void Explode(bool right)
        {
            Invoke("DestroySelf", 3f);
            int dir = right ? 1 : -1;
            for (int i = 0; i < _rigidbodies.Length; i++)
            {
                _rigidbodies[i].AddForce(new Vector2(Random.Range(100, 200) * dir, Random.Range(150, 300)));
                _rigidbodies[i].AddTorque(Random.Range(-50, 50));
            }
        }

        void DestroySelf()
        {
            Destroy(this.gameObject);
        }
    }
}
