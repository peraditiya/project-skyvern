using UnityEngine;

namespace ARA {

	public class ControlMap {

        public static KeyCode KEY_JUMP = KeyCode.DownArrow;
        public static KeyCode KEY_RIGHT = KeyCode.D;
        public static KeyCode KEY_LEFT = KeyCode.A;
        public static KeyCode KEY_UP = KeyCode.W;
        public static KeyCode KEY_DOWN = KeyCode.S;
        public static KeyCode KEY_ATTACK = KeyCode.LeftArrow;
        public static KeyCode KEY_SPELL = KeyCode.RightArrow;
        public static KeyCode KEY_BLOCK = KeyCode.UpArrow;
        public static KeyCode KEY_GRAPPLING = KeyCode.Space;
    }
}