using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA {

	public class Helper {

		static public float DistanceToDegress(Vector3 basePosition, Vector3 targetPosition) {

			Vector3 diffPosition = basePosition - targetPosition;
			float phyta = Mathf.Sqrt(Mathf.Pow(diffPosition.x, 2f) + Mathf.Pow(diffPosition.y, 2f));
			float ret 	= Mathf.Rad2Deg*Mathf.Asin(diffPosition.y/phyta);

			if( (diffPosition.x < 0 && diffPosition.y < 0) || diffPosition.x < 0 ) ret = 180 - ret;
			else if(diffPosition.y < 0 ) ret += 360;

			return 360 - ret;
		}

		static public float DirectionToDegress(Vector3 direction) {

			float ret = 0;
				
			if(direction.z == 0) ret = direction.x < 0 ? 180 : 0; 				
			else if(direction.x == 0) ret = direction.z < 0 ? 90 : 270; 
			else if(direction.x == 1) ret = direction.z < 0 ? 45 : 315;
			else if(direction.x == -1) ret = direction.z < 0 ? 135 : 225;

			return ret;
		}

		static public Vector3 DegressToDirection(float degress) {

			Vector3 ret = Vector3.zero;
			//Debug.Log(degress);
			//Debug.Log("(" + degress + " == " + 225 + ") = " + (degress == 225));
			
			if(degress == 0) ret = new Vector3(1, 0, 0); 				
			else if(degress == 45) ret = new Vector3(1, 0, -1);
			else if(degress == 90) ret = new Vector3(0, 0, -1);
			else if(degress == 135) ret = new Vector3(-1, 0, -1);
			else if(degress == 180) ret = new Vector3(-1, 0, 0);
			else if(degress == 225) ret = new Vector3(-1, 0, 1);
			else if(degress == 270) ret = new Vector3(0, 0, 1);
			else if(degress == 315) ret = new Vector3(1, 0, 1);
			
			return ret;
		}

		static public Vector3 GetDistance(Vector3 self, Vector3 target) {

			return target - self;
		}

		static public int RandomPercent() {

			return Random.Range(1, 101);
		}

        static public RaycastHit2D HitScan(Body body, Vector2 direction, float distance = 0.1f, string layer = "Default", float modifier = 0f, float gap = 0.005f) {

            RaycastHit2D ret = new RaycastHit2D();

            Vector2[] scanPosition = new Vector2[2];

			int layerMask = LayerMask.GetMask(layer);

			if(direction == Vector2.down) scanPosition = new Vector2[2] { new Vector2(body.left + modifier, body.bottom - gap), new Vector2(body.right - modifier, body.bottom - gap) };
			else if(direction == Vector2.left) scanPosition = new Vector2[2] { new Vector2(body.left - gap, body.top - modifier), new Vector2(body.left - gap, body.bottom + modifier) };
			else if(direction == Vector2.right) scanPosition = new Vector2[2] { new Vector2(body.right + gap, body.top - modifier), new Vector2(body.right + gap, body.bottom + modifier) };
			else if(direction == Vector2.up) scanPosition = new Vector2[2] { new Vector2(body.left + modifier, body.top + gap), new Vector2(body.right - modifier, body.top + gap) };
			else if(direction.x == 1 && direction.y == 1) scanPosition = new Vector2[2] { new Vector2(body.right + gap, body.y + modifier), new Vector2(body.x + modifier, body.top + gap) };
			else if(direction.x == -1 && direction.y == 1) scanPosition = new Vector2[2] { new Vector2(body.left - gap, body.y + modifier), new Vector2(body.x - modifier, body.top + gap) };
			
            for (int x = 0; x < scanPosition.Length; x++) {

				Debug.DrawLine(scanPosition[x], scanPosition[x] + (direction * distance), Color.red);
                RaycastHit2D[] scanHit = Physics2D.RaycastAll(scanPosition[x], direction, distance, layerMask);
				
				if(scanHit.Length > 0) {

					foreach(RaycastHit2D r in scanHit) {

						if(r.collider.tag != body.tag) {
							
							ret = r;
							break;
						}
					}
				}
            }

            return ret;
        }

		static public RaycastHit2D[] HitScanAll(Body body, Vector2 direction, float distance = 0.1f, string layer = "Default", float modifier = 0f, float gap = 0.005f, bool returnAll = true) {

			RaycastHit2D[] ret = new RaycastHit2D[1];

            Vector2[] scanPosition = new Vector2[2];

			int layerMask = LayerMask.GetMask(layer);

			if(direction == Vector2.down) scanPosition = new Vector2[2] { new Vector2(body.left + modifier, body.bottom - gap), new Vector2(body.right - modifier, body.bottom - gap) };
			else if(direction == Vector2.left) scanPosition = new Vector2[2] { new Vector2(body.left - gap, body.top - modifier), new Vector2(body.left - gap, body.bottom + modifier) };
			else if(direction == Vector2.right) scanPosition = new Vector2[2] { new Vector2(body.right + gap, body.top - modifier), new Vector2(body.right + gap, body.bottom + modifier) };
			else if(direction == Vector2.up) scanPosition = new Vector2[2] { new Vector2(body.left + modifier, body.top + gap), new Vector2(body.right - modifier, body.top + gap) };
			else if(direction.x == 1 && direction.y == 1) scanPosition = new Vector2[2] { new Vector2(body.right + gap, body.y + modifier), new Vector2(body.x + modifier, body.top + gap) };
			else if(direction.x == -1 && direction.y == 1) scanPosition = new Vector2[2] { new Vector2(body.left - gap, body.y + modifier), new Vector2(body.x - modifier, body.top + gap) };
			
            for (int x = 0; x < scanPosition.Length; x++) {

				Debug.DrawLine(scanPosition[x], scanPosition[x] + (direction * distance), Color.red, 3000, true);
				ret = Physics2D.RaycastAll(scanPosition[x], direction, distance, layerMask);
	
			}

            return ret;
		}

		/* 
		static public float SmoothDecimal(float d, int mock = 2) {
            
			float r = (float)System.Math.Round(d, 0);
			Debug.Log((float)System.Math.Round(d, 1));
			return r + (((float)System.Math.Round((((float)System.Math.Round(d, 1) - r) * 10)/mock, 0) * mock)/10);
        }
		*/

		static public float SmoothDecimal(float d) {
            
			return ((float)System.Math.Round((d * 2f), 0) / 2f);
        }
	}
}