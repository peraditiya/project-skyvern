﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class Viewport : MonoBehaviour
    {

        public CameraProperty property;
        // public Transform child;

        public float speed = .2f;
        public Transform target;

        private Transform _transform;

        Vector3 velocity;
        bool shaking = false;

        bool isCameraSwingComplete = false;

        public static Viewport instance;

        void Awake()
        {
            instance = this;
            _transform = transform;
        }

        void Start()
        {

        }

        void FixedUpdate()
        {
            if (!target)
                return;

            Vector3 previousPos = _transform.position;
            Vector3 targetPos = new Vector3(target.position.x, target.position.y + property.height, property.zDistance);
            
            targetPos = new Vector3(Mathf.Clamp(targetPos.x, property.bound.left, property.bound.right), Mathf.Clamp(targetPos.y, property.bound.bottom, property.bound.top), targetPos.z);
            
            /* 
            if (Root.player.unit.state.isHooked)
            {

                if (Root.player.unit.state.isRetract)
                {

                    if (isCameraSwingComplete)
                    {

                        targetPos.x = Root.player.currentHold.body.x;
                        targetPos.y = previousPos.y < Root.player.currentHold.body.y + 3.5f ? previousPos.y + (Root.player.grappler.retractSpeed * 0.8f) : previousPos.y;

                    }
                    else
                        targetPos.y = previousPos.y;
                }
                else if (Root.player.unit.state.isHanging)
                {

                    if (Root.player.unit.state.isSwing == false)
                    {

                        targetPos = previousPos;
                    }
                }
                else if (Root.player.unit.state.isSwing)
                {

                    if (Root.player.unit.state.isSwing && isCameraSwingComplete == false)
                    {

                        isCameraSwingComplete = (Root.player.grappler.grapplingDirection.x > 0 && targetPos.x > Root.player.currentHold.body.x) || (Root.player.grappler.grapplingDirection.x < 0 && targetPos.x < Root.player.currentHold.body.x) || Root.player.grappler.grapplingDirection == Vector2.up;

                    }
                    else if (Root.player.unit.state.isSwing && isCameraSwingComplete == true)
                    {

                        targetPos.x = Root.player.currentHold.body.x;
                        targetPos.y = previousPos.y;
                    }
                }
            }
            */

            _transform.position = Vector3.SmoothDamp(_transform.position, targetPos, ref velocity, speed);

        }
    }

    [System.Serializable]
    public struct CameraProperty
    {
        public float zDistance;
        public float height;
        public Bound bound;
    }

    [System.Serializable]
    public struct Bound
    {
        public float left;
        public float right;
        public float top;
        public float bottom;
    }
}