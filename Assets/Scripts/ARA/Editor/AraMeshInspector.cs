﻿using UnityEngine;
using UnityEditor;

namespace ARA
{
    [CustomEditor(typeof(AraMesh))]
    public class AraMeshInspector : Editor
    {

        public override void OnInspectorGUI()
        {

            DrawDefaultInspector();

            AraMesh araMesh = (AraMesh)target;

            if (GUILayout.Button("Snap")) araMesh.Snap();
        }
    }
}