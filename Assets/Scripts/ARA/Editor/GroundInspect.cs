﻿using UnityEngine;
using UnityEditor;

namespace ARA
{
	[CustomEditor(typeof(Ground))]
    public class GroundInspect : Editor
    {

		private Ground ground;

		public override void OnInspectorGUI()
        {
			DrawDefaultInspector();

			this.ground = (Ground)target;

			if (GUILayout.Button("Add Mesh")) this.ground.AddMesh();
		}
    }
}