﻿using UnityEngine;
using UnityEditor;

namespace ARA
{
	[CustomEditor(typeof(HangPoint))]
    public class HangPointInspect : Editor
    {

		private HangPoint hangPoint;

		public override void OnInspectorGUI()
        {
			DrawDefaultInspector();

			this.hangPoint = (HangPoint)target;

			if (GUILayout.Button("Add Mesh")) this.hangPoint.AddMesh();
		}
    }
}