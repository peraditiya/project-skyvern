﻿using UnityEngine;
using UnityEditor;

namespace ARA
{
    [CustomEditor(typeof(Level))]
	public class LevelInspect : Editor
    {

		private Level level;

        Color[] gizmoColors = new Color[] { Color.cyan, Color.red, Color.yellow, Color.green, Color.white, Color.gray };

        // Use this for initialization
        public override void OnInspectorGUI()
        {
			DrawDefaultInspector();

			this.level = (Level)target;

			if (GUILayout.Button("Create Room")) {

                GameObject tgo = new GameObject("Room_0_0");
                tgo.transform.parent    = this.level.transform;
                tgo.transform.position  = Vector3.zero;
                Room a = tgo.AddComponent<Room>();
                a.Install();
                a.gizmoColor = this.gizmoColors[this.level.transform.childCount%6];
			}
        }
    }
}