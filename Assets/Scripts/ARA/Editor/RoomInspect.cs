﻿using UnityEngine;
using UnityEditor;

namespace ARA
{
	[CustomEditor(typeof(Room))]
    public class RoomInspect : Editor
    {

		private Room room;

        public override void OnInspectorGUI()
        {
			DrawDefaultInspector();

			this.room = (Room)target;

			if (GUILayout.Button("Apply")) this.room.Install();
		}
    }
}