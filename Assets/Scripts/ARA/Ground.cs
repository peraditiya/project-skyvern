﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class Ground : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        public GameObject AddMesh()
        {
            GameObject tgo = new GameObject();
			tgo.transform.parent = this.transform;
			tgo.transform.position = Vector3.zero;
			tgo.name = "Ground_" + this.transform.childCount;
			tgo.AddComponent<AraMesh>().isPlatform = false;

			return tgo;
        }
    }
}