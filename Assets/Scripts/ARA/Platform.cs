﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class Platform : Ground
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        public GameObject AddMesh()
        {
            GameObject tgo = base.AddMesh();

			tgo.GetComponent<AraMesh>().isPlatform = true;

			return tgo;
        }
    }
}