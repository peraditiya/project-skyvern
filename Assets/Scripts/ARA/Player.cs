﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA {
    public class Player : Character {

        AbilityJump abilityJump;
        private SpriteRenderer[] _sprites;

        public void Awake () {
            base.Awake ();

            Root.player = this;

            this._sprites = GetComponentsInChildren<SpriteRenderer> (true);
        }

        // Use this for initialization
        public override void Start () {
            base.Start ();

            this.PlayAnim("Aria_Idle");

            //Debug.Break();

            this.abilityJump = this.GetAbility<AbilityJump>("Jump");
        }

        public override IEnumerator OnCharacterPlay () {

            int idleState;

            while (true) {

                if (!this.states.isHit) {

                    this.movement = Vector3.zero;

                    idleState = Character.CHARACTER_STATE_IDLE;

                    foreach (KeyValuePair<string, Ability> ability in this.abilities) {

                        if (idleState != Character.CHARACTER_STATE_IDLE) {

                            if (ability.Value.Exec () == Character.CHARACTER_STATE_ACTIVE_BREAK) break;

                        } else
                            idleState = ability.Value.Exec ();

                        if (idleState == Character.CHARACTER_STATE_ACTIVE_BREAK) break;
                    }

                    if (idleState == Character.CHARACTER_STATE_IDLE) this.PlayAnim("Aria_Idle");
                }

                yield return null;
            }
        }

        public override IEnumerator OnCharacterFixedUpdate () {
            while (true) {
                
                if (!this.states.isFreeze) this.rigidbody.velocity = new Vector2 (this.movement.x, this.rigidbody.velocity.y);

                yield return new WaitForFixedUpdate ();
            }
        }

        public override void OnCharacterHit (float damage = 10) {

            //if (!this.states.isBlock) {
                
                if (!this.states.isInvisible && !this.states.isHit) {
                    
                    this.states.isBlock = false;
                    this.states.isHit = true;
                    this.states.isInvisible = true;

                    //this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);

                    if (!this.states.isHooked) {

                        this.abilityJump.Trigger (8);
                    
                    } else {

                        this.states.isHit = false;
                    } 

                    this.StartCoroutine (this.HitPhase ());
                }
            //}
        }

        public IEnumerator HitPhase () {

            float startInvisibility = Time.time;
            float amount = 1f;
            
            while (Time.time - startInvisibility < 1.5f || amount >= 0) {
                
                if(amount <= 0) amount = 1f;
                
                for (int i = 0; i < _sprites.Length; i++) {
                    _sprites[i].material.SetColor("_FlashColor", Color.white) ;
                    _sprites[i].material.SetFloat ("_FlashAmount", amount);  
                } 

                amount -= Time.deltaTime * 2;
                
                //this.body.renderer.material.color = this.body.renderer.material.color == Color.white ? Color.red : Color.white;

                yield return new WaitForFixedUpdate ();
            }

            this.states.isInvisible = false;
            //this.body.renderer.material.color = Color.white;
        }

        void OnCollisionEnter2D (Collision2D other) {

            if(other.collider.gameObject.layer == LayerMask.NameToLayer("Ground")) {

                if(this.states.isHooked || this.states.isBossRide) this.OnCharacterHit ();
                
                if(other.collider.tag == "Enemy") this.states.isBossRide = true;
            }
        }

        void OnCollisionExit2D (Collision2D other) {
            
            if (other.collider.gameObject.layer == LayerMask.NameToLayer("Ground") && this.states.isJump == false && this.states.isHooked == false) {

                RaycastHit2D[] groundHit = Helper.HitScanAll(this.body, Vector2.down, 0.5f, "Ground");

                if(groundHit.Length == 0) this.abilityJump.Trigger(0);

                if(other.collider.tag == "Enemy") this.states.isBossRide = false;                
            }   
        }
    }
}