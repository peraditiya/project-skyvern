﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    [RequireComponent(typeof(RectTransform))]
    public class Room : MonoBehaviour
    {

        public int vCell = 3;
		public int hCell = 3;

		public int x;
		public int y;

        public bool use = false;

        public Body body;

        [HideInInspector]
        public Dictionary<string, Hostile> hostiles;

        private RectTransform rect;
        public Color gizmoColor;

        // Use this for initialization
        
        void Awake()
        {
            this.hostiles = new Dictionary<string, Hostile>();
            if(this.use) Root.room = this;
        }

        void Start()
        {
            this.body = new Body(this.gameObject);
            this.rect = this.GetComponent<RectTransform>();

            this.hostiles = new Dictionary<string, Hostile>();
        }

        public void Install()
        {
            if(this.body == null) this.Start();

            this.rect.sizeDelta = new Vector2(this.hCell * Root.cellWidth, this.vCell * Root.cellHeight);

            this.body.left = this.x * Root.cellWidth;
            this.body.bottom = this.y * Root.cellHeight;
            this.body.size = this.rect.sizeDelta;

			Transform tgo = this.transform.Find("Ground");
			if(tgo == null) {

                tgo = new GameObject("Ground").transform;
                tgo.parent = this.transform;
                tgo.position = Vector3.zero;
                tgo.gameObject.AddComponent<Ground>();
            } 

            tgo = this.transform.Find("Platform");
			if(tgo == null) {

                tgo = new GameObject("Platform").transform;
                tgo.parent = this.transform;
                tgo.position = Vector3.zero;
            } 

            tgo = this.transform.Find("HangPoint");
			if(tgo == null) {

                tgo = new GameObject("HangPoint").transform;
                tgo.parent = this.transform;
                tgo.position = Vector3.zero;
                tgo.gameObject.AddComponent<HangPoint>();
            } 
        }

        void OnDrawGizmos() {
            
            if(this.body == null) this.Start();

            Gizmos.color = this.gizmoColor;
            Gizmos.DrawWireCube(this.body.position, new Vector3(this.rect.sizeDelta.x, this.rect.sizeDelta.y, 10));
        }
    }
}