﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
	//[RequireComponent(typeof(Rigidbody2D))]
    //[RequireComponent(typeof(BoxCollider2D))]
    public class Weapon : MonoBehaviour
    {

		protected BoxCollider2D collider;
        protected Character owner;

        public Ability activator;

        // Use this for initialization
        public void Start()
        {
            this.collider           = this.GetComponent<BoxCollider2D>();
            
            if(this.collider != null) {

                this.collider.enabled   = false;
                this.collider.isTrigger = true;
            }
            
            this.transform.localPosition = Vector3.zero;
            this.owner = this.transform.parent.gameObject.GetComponent<Character>();
        }

		public virtual int Exec()
        {


            return 0;
        }
    }
}