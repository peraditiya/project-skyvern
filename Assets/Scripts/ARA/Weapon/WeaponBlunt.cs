﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class WeaponBlunt : Weapon
    {

		public Vector3 size;
		public Vector3 position;
		public float hitDelay;
        public float animationDelay;

        public float damage = 10f;

        private IEnumerator doActivateRoutine;

        // Use this for initialization
        void Start()
        {
			base.Start();
        }

		void OnTriggerEnter2D(Collider2D collider)
        {
            if(collider.tag != this.transform.parent.gameObject.tag) {
                
                string layerVictim = LayerMask.LayerToName(collider.gameObject.layer);
                
				switch(layerVictim) {

                    case "Enemy":
                        Hostile victim = Root.room.hostiles[collider.name];
                        victim.OnCharacterHit();
                    break;

                    case "Player":
                        Root.player.OnCharacterHit(this.damage);
                    break;
                }

                this.StopCoroutine(this.doActivateRoutine);
                this.Deactivate();
            }
   		}

        public void Activate(){
			
            this.doActivateRoutine = this.DoActivate(); 
            this.StartCoroutine(this.doActivateRoutine);
		}
        
		IEnumerator DoActivate(){
			yield return new WaitForSeconds(this.hitDelay);
			this.collider.enabled = true;
			this.collider.size 	  = this.size;
			this.transform.localPosition = this.position;
			yield return new WaitForSeconds(this.animationDelay);
			Deactivate();
		}

		private void Deactivate(){
			this.collider.enabled = false;
            this.transform.localPosition = Vector3.zero;
            this.collider.size = new Vector3(1, 1, 0.1f);
			//transform.Translate(Vector3.left * .05f);
		}

        // Update is called once per frame

    }
}