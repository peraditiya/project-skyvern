﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA {
    public class WeaponShield : Weapon {

        public float parryDelay;

        private Animator animator;
        private SpriteRenderer renderer;
        private float shieldStart;

        // Use this for initialization
        void Start () {
            base.Start ();

            this.animator = this.gameObject.GetComponent<Animator> ();
            this.renderer = this.gameObject.GetComponent<SpriteRenderer> ();

            this.DeActivate ();
        }

        public void Activate () {

            this.animator.SetInteger ("status", 1);

            this.collider.enabled = true;
            this.renderer.flipX = !this.owner.isFacingRight;
            this.renderer.enabled = true;
            this.transform.localPosition = (new Vector3 (1.2f, 0, 0)) * (!this.owner.isFacingRight ? -1 : 1) + (Vector3)this.owner.body.collider.offset;
            this.shieldStart = Time.time;
        }

        public void Release () {

            this.animator.SetInteger ("status", 2);
        }

        public void DeActivate () {

            this.animator.SetInteger ("status", 0);

            this.collider.enabled = false;
            this.renderer.enabled = false;

            this.owner.states.isParry = false;

            this.shieldStart = 0f;
            //this.transform.localPosition = Vector3.zero;
        }

        void OnTriggerEnter2D (Collider2D collider) {

            string layerCollider = LayerMask.LayerToName (collider.gameObject.layer);

            bool isParried = (Time.time - this.shieldStart) < this.parryDelay;

            switch (layerCollider) {

                case "Bullet":

                    SKY.Bullet bullet = collider.GetComponent<SKY.Bullet> ();

                    if (bullet.isParryable) {

                        if (isParried) {
                            bullet.prevDirection = bullet.direction;
                            bullet.direction *= (-1 * bullet.parryMult);
                            bullet.bulletType = SKY.Bullet.BulletType.ParryBullet;
                        } else
                            bullet.Recycle ();
                    }

                    break;
            }

            /* 
            if (collider.tag == "Attack")
            {

                this.StartCoroutine(this.Full());

                if ((Time.time - this.shieldStart) < this.parryDelay)
                {
                    this.owner.states.isParry = true;

                    Debug.Log(LayerMask.LayerToName(collider.gameObject.layer));

                    switch (collider.transform.parent.tag)
                    {

                        case "Hostile":
                            Hostile victim = Root.room.hostiles[collider.transform.parent.name];
                            victim.OnCharacterHit();
                            break;

                        case "Player":
                            Root.player.OnCharacterHit();
                            break;
                    }
                } 
            }
            */
        }

        IEnumerator Full () {

            this.animator.SetInteger ("status", 3);
            yield return new WaitForSeconds (0.3f);
            this.activator.ForceStop ();
            this.DeActivate ();
        }
    }
}