﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ARA
{
    public class WeaponSword : Weapon
    {
		
		public Vector3[] size;
		public Vector3[] position;
		public float[] hitDelay;
        public float[] animationDelay;

        public float damage = 10;

        public int maximumCombo;
		
        // Use this for initialization
        void Start()
        {
			base.Start();
        }

        public override int Exec()
        {

            return 0;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {
            if(collider.tag != this.transform.parent.gameObject.tag) {
                
                string layerVictim = LayerMask.LayerToName(collider.gameObject.layer);

                switch(layerVictim) {

                    case "Enemy":
                        //Hostile victim = Root.room.hostiles[collider.name];
                        //victim.OnCharacterHit();

                        if(collider.tag == "Boss") {

                            SKY.IHE_Boss victim = collider.GetComponent<SKY.IHE_Boss>();
                            victim.Hit(this.damage);

                        } else {

                            SKY.IHE_Enemy victim = collider.GetComponent<SKY.IHE_Enemy>();
                            victim.Hit(this.damage);
                        }
                        
                    break;

                    case "Player":
                        Root.player.OnCharacterHit();
                    break;
                }
            }
   		}

        public void Activate(int combo){
			
            this.StartCoroutine(DoActivate(combo));
		}
        
		IEnumerator DoActivate(float delay, float duration){
			yield return new WaitForSeconds(delay);
			this.collider.enabled = true;
			//transform.Translate(Vector3.right * .05f);
            this.transform.localPosition = new Vector3(1, 0, 0) * (!this.owner.isFacingRight ? -1 : 1);
			yield return new WaitForSeconds(duration);
			Deactivate();
		}

        IEnumerator DoActivate(int combo){
			yield return new WaitForSeconds(.1f);
			this.collider.enabled = true;
			//transform.Translate(Vector3.right * .05f);
            Vector3 hitBoxPosition = this.position[combo];
            hitBoxPosition.x *= (this.owner.isFacingRight ? 1 : -1);
            this.transform.localPosition = hitBoxPosition;
            this.collider.size = this.size[combo];
			yield return new WaitForSeconds(.1f);
			Deactivate();
		}

		private void Deactivate(){
			this.collider.enabled = false;
            this.transform.localPosition = Vector3.zero;
            this.collider.size = new Vector3(1, 1, 0.1f);
			//transform.Translate(Vector3.left * .05f);
		}
    }
}