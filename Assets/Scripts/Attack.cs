﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class Attack : MonoBehaviour
    {

        public Body body;

        private Dictionary<string, AttackSet> attackSets = new Dictionary<string, AttackSet>();
        private bool isPlay = false;

        private Character _parentCharacter = null;

        void Awake()
        {
            this.body = new Body(this.gameObject);
            this.Reset(true);
        }

        public void CreateSet(string attackId, Vector3 position, Vector2 size, int startIndex = 1, int endIndex =2)
        {

            this.attackSets.Add(attackId, new AttackSet() { position = position, size = size, startIndex = startIndex, endIndex = endIndex });
        }

        public void Play(string attackId)
        {
            if (this.isPlay == false)
            {
                AttackSet attackSet = this.attackSets[attackId];

                Vector3 hitPosition = Vector3.zero;

                hitPosition.x = (this.parentCharacter.body.renderer.flipX ? -1 : 1) * attackSet.position.x;
                hitPosition.y = (this.parentCharacter.body.renderer.flipY ? -1 : 1) * attackSet.position.y;

                this.body.transform.localPosition = hitPosition;
                this.body.size = attackSet.size;
                this.body.collider.size = this.body.size;
                this.body.collider.enabled = true;

                this.isPlay = true;
            }
        }

        public void Reset(bool force = false)
        {

            if (this.isPlay || force)
            {
                this.body.transform.localPosition = Vector3.zero;
                this.body.size = new Vector2(1, 1);
                this.body.collider.size = this.body.size;
                this.body.collider.enabled = false;
                this.isPlay = false;
            }
        }

        public Character parentCharacter
        {
            get { return this._parentCharacter; }
            set
            {
                this._parentCharacter = value;
                this.body.parent = this._parentCharacter.unit;

                Physics2D.IgnoreCollision(this.body.parent.collider, this.body.collider, true);
            }
        }

        public void OnTriggerEnter2D(Collider2D collider)
        {

            if (collider.tag == "Hostile")
            {

                Hostile hostile = Stage.hostiles[collider.name];
                
                this._parentCharacter.unit.state.isHitEnemy = true;

                hostile.Hit();
                if(this.parentCharacter.spriteAnimator.currentAnimation == Character.ATTACK_FINISHER_ID) hostile.Knocked();
            }
        }
    }

    public class AttackSet
    {
        public Vector3 position = Vector3.zero;
        public Vector2 size = Vector2.zero;

        public int startIndex = 1;
        public int endIndex = 1;
    }
}