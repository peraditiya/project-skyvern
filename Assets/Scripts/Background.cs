﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Background : MonoBehaviour {

		public Body body;

		// Use this for initialization
		void Start () {

			this.body = new Body(this.gameObject);
			
			Root.viewport.background.Add(this);	
		}		
	}
}