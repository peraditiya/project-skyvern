﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace SKY {
    public class Bullet : MonoBehaviour {
        public enum BulletType {
            EnemyBullet,
            PlayerBullet,
            ParryBullet,
            EmptyBullet
        }

        public BulletType bulletType;

        public bool isParryable = true;
        public float parryMult = 2f;

        public float speed = 10f;
        public float curSpeed = 0;

        public float damage = 10f;
        public bool autoDestroy = true;
        public float timeToDestroy = 5f;

        public Transform target;

        protected Transform _transform;

        public Vector3 direction = Vector3.up;
        public Vector3 prevDirection = Vector3.up;

        private IEnumerator followCoroutine;
        protected List<Tweener> currentTween = new List<Tweener> ();
        public int bounce = 0;

        Vector2 edge = new Vector2 (8.8f, 5f);

        public virtual void Awake () {
            _transform = transform;
            curSpeed = speed;
        }

        public virtual void OnEnable () {
            StartCoroutine ("BulletMovement");
            if (autoDestroy)
                Invoke ("Recycle", timeToDestroy);
        }

        public virtual void OnDisable () {
            // for gravitation
            Rigidbody2D rb = GetComponent<Rigidbody2D> ();
            if (rb && !rb.isKinematic) {
                rb.velocity = Vector2.zero;
                Destroy (rb);
            }

            for (int i = 0; i < currentTween.Count; i++) {
                currentTween[i].Kill ();
            }

            _transform.rotation = Quaternion.identity;
            for (int i = 0; i < currentTween.Count; i++) {
                currentTween[i].Kill ();
            }
            currentTween.Clear ();
            StopAllCoroutines ();
            CancelInvoke ("Recycle");

        }

        public void ChangePosition (Vector3 newPos) {
            _transform.position = newPos;
        }

        public void ChangeSpeed (float newSpeed) {
            curSpeed = newSpeed;
        }

        public void ChangeSpeed (float newSpeed, float time, float delay = 0) {
            Tweener t = DOTween.To (() => curSpeed, x => curSpeed = x, newSpeed, time).SetDelay (delay);
            currentTween.Add (t);
        }

        public void ChangeSize (float newSize, float time, float delay = 0) {
            Tweener t = _transform.DOScale (new Vector3 (newSize, newSize * .75f, 1f), time).SetDelay (delay);
            currentTween.Add (t);
        }

        public void ChangeAngle (float newRotation) {
            _transform.rotation = Quaternion.Euler (0, 0, newRotation);
        }
        public void ChangeAngle (float newRotation, float time, float delay = 0) {
            Tweener t = _transform.DORotate (new Vector3 (0, 0, newRotation), time).SetDelay (delay);
            currentTween.Add (t);
        }
        public void AddRotation (float addedRotation, float time, float delay = 0) {
            Tweener t = _transform.DORotate (new Vector3 (_transform.rotation.eulerAngles.x, _transform.rotation.eulerAngles.y, _transform.rotation.eulerAngles.z + addedRotation), time).SetDelay (delay).SetEase (Ease.Linear);
            currentTween.Add (t);
        }

        public void ChangeDirection (Vector3 newDirection, float time, float delay = 0) {
            Tweener t = DOTween.To (() => direction, x => direction = x, newDirection, time).SetDelay (delay);
            currentTween.Add (t);
        }

        void ChangeDirection (Vector3 newDirection) {
            direction = newDirection;
        }

        public void FollowPlayer (float followSpeed, float followDuration, float delay) {
            followCoroutine = DoFollowPlayer (followSpeed, followDuration, delay);
            StartCoroutine (followCoroutine);
        }

        IEnumerator DoFollowPlayer (float followSpeed, float followDuration, float delay) {
            yield return new WaitForSeconds (delay);
            while (target) {
                if (followDuration > 0) {
                    followDuration -= Time.deltaTime;
                    Vector3 dir = target.transform.position - _transform.position;
                    float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
                    _transform.rotation = Quaternion.Lerp (_transform.rotation, Quaternion.AngleAxis (angle - 90, Vector3.forward), followSpeed * Time.deltaTime);
                }
                yield return null;
            }
        }

        public void StopFollowPlayer (float delayTime) {
            StartCoroutine (DoStopFollowPlayer (delayTime));
        }

        IEnumerator DoStopFollowPlayer (float delayTime) {
            yield return new WaitForSeconds (delayTime);
            if (followCoroutine != null)
                StopCoroutine (followCoroutine);
        }

        public void Bounce (int bounceAmount) {
            StartCoroutine (DoBounce (bounceAmount));
        }

        IEnumerator DoBounce (int bounceAmount) {
            bounce = bounceAmount;
            while (bounce > 0) {
                if (_transform.position.x > edge.x || _transform.position.x < -edge.x) {
                    Vector2 gDir = _transform.TransformDirection (direction);
                    Vector2 newDir = _transform.InverseTransformDirection (new Vector2 (-gDir.x, gDir.y));
                    direction = newDir;
                    yield return new WaitForSeconds (.05f);
                    bounce--;
                } else if (_transform.position.y > edge.y || _transform.position.y < -edge.y) {
                    Vector2 gDir = _transform.TransformDirection (direction);
                    Vector2 newDir = _transform.InverseTransformDirection (new Vector2 (gDir.x, -gDir.y));
                    direction = newDir;
                    yield return new WaitForSeconds (.05f);
                    bounce--;
                }
                yield return null;
            }
        }

        public virtual IEnumerator BulletMovement () {
            while (true) {
                _transform.Translate (direction * curSpeed * Time.deltaTime);
                yield return null;
            }
        }

        public virtual void Gravity (float force, float angle, float gravityScale) {
            StopCoroutine ("BulletMovement");
            Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D> () as Rigidbody2D;
            rb.gravityScale = gravityScale;
            Vector2 dir = (Vector2) (Quaternion.Euler (0, 0, angle + 90) * Vector2.right);
            rb.AddForce (dir * force);
        }

        public virtual void OnTriggerEnter2D (Collider2D col) {

            string layerCollider = LayerMask.LayerToName(col.gameObject.layer);
            
            switch (bulletType) {
                case BulletType.PlayerBullet:
                    // Enemy e = col.GetComponent<Enemy>();
                    // if (e)
                    // {
                    //     BulletParams bp = new BulletParams(bulletStrength, owner);
                    //     e.GetDamage(bp);
                    //     Master.BFObject.SpawnBulletHit(_transform.position, col.transform, _transform.eulerAngles.z);
                    // }

                    break;
                case BulletType.EnemyBullet:
                    
                    /* 
                    AriaController ac = col.GetComponent<AriaController> ();
                    if (ac) {
                        ac.Hit (damage, _transform.position.x > col.transform.position.x);
                        Recycle ();
                    }
                    */

                    if(layerCollider == "Player") {
                        ARA.Root.player.OnCharacterHit(this.damage);
                        Recycle();
                    }

                    // ARA.Root.player.OnCharacterHit(this.damage);
                    
                    break;
                case BulletType.ParryBullet:
                   
                    if(layerCollider == "Enemy") {

                        if(col.tag == "Boss") {

                            SKY.IHE_Boss victim = col.GetComponent<SKY.IHE_Boss>();
                            victim.Hit(this.damage);
                        
                        } else {

                            IHE_Enemy enemy = col.GetComponent<IHE_Enemy>();
                            enemy.Hit(this.damage);
                        }
                        
                        //this.bulletType = BulletType.EnemyBullet;
                        //this.direction = this.direction / (this.parryMult * -1);
                        //this.direction = this.prevDirection; 
                        Recycle();
                    }
                    
                    break;
            }

        }

        public virtual void Recycle () {
            if (gameObject.activeSelf) {
                
                if(this.bulletType == BulletType.ParryBullet) {

                    this.bulletType = BulletType.EnemyBullet;
                    this.direction = this.direction / (this.parryMult * -1);
                }
                gameObject.Recycle ();           
            }
        }
    }
}