﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class Shooter : MonoBehaviour
    {
		public GameObject bullet;
		public Transform source;
		private Transform target;
		public ShooterValueFloat speed;
		public ShooterValueInt bulletCount;
		public ShooterValueInt burstCount;
		public ShooterValueFloat fireRate;
		public float autoDestroyTime;

		public bool aimPlayer = false;
		public ShooterValueFloat baseAngle;
		public bool roundAngles = false;
		public ShooterValueFloat gapAngle;
		public float errorAngle;

		public bool changeSpeed = false;
		public float newSpeed;
		public float changeSpeedDelay;
		public float changeSpeedTransition;

		public bool changeAngle = false;
		public float newAngle;
		public float changeAngleDelay;
		public float changeAngleTransition;

		public bool followPlayer = false;
		public float followDelay = 0;
		public float followSpeed = 0;
		public float followDuration = 0;

		public bool addGravity = false;
		public float force = 500f;
		public float gravityScale = 1;

		private Transform theSource;

		void Start(){
			Init();
		}

		void Init(){
			bullet.CreatePool();
			theSource = source ? source : transform;
		}

		public void SetTarget(Transform target){
			this.target = target;
		}

		private float GetAimAngle(){
			if(!theSource || !target)
				return 0;
			Vector3 temp = (target.transform.position - theSource.position);
			float bangle = Mathf.Atan2(temp.y, temp.x) * Mathf.Rad2Deg;
			return Quaternion.AngleAxis(bangle - 90, Vector3.forward).eulerAngles.z;
		}

		public void SetBaseAngle(float angle){
			this.baseAngle.min = angle;
		}

		private Bullet[] Spawn(GameObject bullet, Transform source, Vector3 basePos, float speed, int count, float baseAngle = 180, float angle = 30f, float errorAngle = 0, float destroyTime = 0)
		{
			Bullet[] bs = new Bullet[count];
			if (source)
			{
				float startAngle = baseAngle - (((float)count * .5f) - .5f) * angle;
				float curAngle = startAngle;
				if(errorAngle > 0)
						curAngle += Random.Range(-errorAngle, errorAngle);
				for (int i = 0; i < count; i++)
				{
					bs[i] = bullet.Spawn(basePos).GetComponent<Bullet>();
					bs[i].ChangeAngle(curAngle);
					bs[i].ChangeSpeed(speed);
					curAngle += angle;
					if(destroyTime > 0){
						bs[i].autoDestroy = true;
						bs[i].timeToDestroy = destroyTime;
					}
				}
			}
			return bs;
		}

		public Bullet[] Shoot(){
			int currentBurstCount = burstCount.GetValue();
			float currentBaseAngle = aimPlayer ? GetAimAngle() : baseAngle.GetValue();
			float currentSpeed = speed.GetValue();
			float curretGapAngle = roundAngles ? 360f / currentBurstCount : gapAngle.GetValue();
			// baseAngle = errorAngle > 0 ? baseAngle : baseAngle + Random.Range(-errorAngle, errorAngle);
			Bullet[] bs = Spawn(bullet, theSource, theSource.position, currentSpeed, currentBurstCount, currentBaseAngle, curretGapAngle, errorAngle, autoDestroyTime);
			if(changeSpeed){
				for (int i = 0; i < bs.Length; i++)
				{
					bs[i].ChangeSpeed(newSpeed, changeSpeedTransition, changeSpeedDelay);
				}
			}
			if(changeAngle){
				for (int i = 0; i < bs.Length; i++)
				{
					bs[i].ChangeAngle(newAngle, changeAngleTransition, changeAngleDelay);
				}
			}
			if(followPlayer){
				print(this.target);
				for (int i = 0; i < bs.Length; i++)
				{
					bs[i].target = this.target;
					bs[i].FollowPlayer(followSpeed, followDuration, followDelay);
				}
			}
			if(addGravity){
				for (int i = 0; i < bs.Length; i++)
				{
					bs[i].Gravity(force, bs[i].transform.eulerAngles.z, gravityScale);
				}
			}
			return bs;
		}

		public float GetShootDuration(){
			return bulletCount.GetValue() * fireRate.GetValue();
		}

		// public int GetBulletCount(){
		// 	if(bulletCount.choice == ShooterValue<int>.Choice.Random)
		// 		return Random.Range(bulletCount.min, bulletCount.max+1);
			
		// 	return bulletCount.min;
		// }
    }
}
