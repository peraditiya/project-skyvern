﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class CameraFollow : MonoBehaviour
    {

        public CameraProperty property;
        // public Transform child;

        public float speed = .2f;
        public Transform target;

        public float zoom = 1f;

        public bool X_AxisLocked;
        public bool Y_AxisLocked;

        private Transform _transform;

        Vector3 velocity;
        Vector3 vibration;

        bool shaking = false;

        bool isCameraSwingComplete = false;

        public static CameraFollow instance;

        private IEnumerator vibrateCoroutine;

        private GameObject screenEffect;

        void Awake()
        {
            instance = this;
            _transform = transform;
            vibrateCoroutine = DoVibrate(0,0,0,0);

            ARA.Root.camera = this;

            //this.GetComponent<Camera>().transparencySortMode = TransparencySortMode.Orthographic;

            this.screenEffect = GameObject.Find("Screen");

            _transform.position = new Vector3(_transform.position.x, _transform.position.y + this.property.height, _transform.position.z);
        }

        void Update()
        {
            if(Input.GetKeyDown(KeyCode.P))
                Vibrate(1f, .1f, 1f, 5f);
        }

        void FixedUpdate()
        {
            if (!target)
                return;

            Vector3 previousPos = _transform.position;
            Vector3 targetPos = new Vector3(target.position.x, target.position.y + property.height, property.zDistance / this.zoom);
            
            targetPos = new Vector3(Mathf.Clamp(targetPos.x, property.bound.left, property.bound.right), Mathf.Clamp(targetPos.y, property.bound.bottom, property.bound.top), targetPos.z);
            
            //if(this.X_AxisLocked) targetPos.x = previousPos.x;
            //if(this.Y_AxisLocked) targetPos.y = previousPos.y;

            // targetPos.y += 1;

            // if (Root.player.unit.state.isHooked)
            // {

            //     if (Root.player.unit.state.isRetract)
            //     {

            //         if (isCameraSwingComplete)
            //         {

            //             targetPos.x = Root.player.currentHold.body.x;
            //             targetPos.y = previousPos.y < Root.player.currentHold.body.y + 3.5f ? previousPos.y + (Root.player.grappler.retractSpeed * 0.8f) : previousPos.y;

            //         }
            //         else
            //             targetPos.y = previousPos.y;
            //     }
            //     else if (Root.player.unit.state.isHanging)
            //     {

            //         if (Root.player.unit.state.isSwing == false)
            //         {

            //             targetPos = previousPos;
            //         }
            //     }
            //     else if (Root.player.unit.state.isSwing)
            //     {

            //         if (Root.player.unit.state.isSwing && isCameraSwingComplete == false)
            //         {

            //             isCameraSwingComplete = (Root.player.grappler.grapplingDirection.x > 0 && targetPos.x > Root.player.currentHold.body.x) || (Root.player.grappler.grapplingDirection.x < 0 && targetPos.x < Root.player.currentHold.body.x) || Root.player.grappler.grapplingDirection == Vector2.up;

            //         }
            //         else if (Root.player.unit.state.isSwing && isCameraSwingComplete == true)
            //         {

            //             targetPos.x = Root.player.currentHold.body.x;
            //             targetPos.y = previousPos.y;
            //         }
            //     }
            // }

            Vector3 smooth = Vector3.SmoothDamp(_transform.position, targetPos, ref velocity, speed);

            _transform.position = smooth + new Vector3(vibration.x, vibration.y, 0);
            _transform.localRotation = Quaternion.Euler(0,0,vibration.z);

            if(this.screenEffect != null) this.screenEffect.transform.position = new Vector3(this._transform.position.x, this._transform.position.y, 0);
        }

        public void Vibrate(float shake, float maxPos, float maxAngle, float time){
            StopCoroutine(vibrateCoroutine);
            vibrateCoroutine = DoVibrate(shake, maxPos, maxAngle, time);
            StartCoroutine(vibrateCoroutine);
        }

        IEnumerator DoVibrate(float shake, float maxPos, float maxAngle, float time){
            float curShake = shake;
            float rate = shake / time;
            while(time > 0){
                vibration = new Vector3(maxPos * curShake * Random.Range(-1,2) ,
                                        maxPos * curShake * Random.Range(-1,2),
                                        maxAngle * curShake * Random.Range(-1,2));
                curShake -= rate * Time.deltaTime;
                time -= Time.deltaTime;
                yield return null;
            }
            vibration = Vector3.zero;
        }

        
    }

    [System.Serializable]
    public struct CameraProperty
    {
        public float zDistance;
        public float height;
        public Bound bound;
    }

    [System.Serializable]
    public struct Bound
    {
        public float left;
        public float right;
        public float top;
        public float bottom;
    }
}