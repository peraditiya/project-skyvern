﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    public abstract class Character : MonoBehaviour
    {

        [HideInInspector]
        public Unit unit;

        [HideInInspector]
        public SpriteAnimator spriteAnimator;

        [HideInInspector]
        public Animator animator;

        [HideInInspector]
        public Body body = null;

        [HideInInspector]
        public Attack attack = null;

        [HideInInspector]
        public Vector3 movement = Vector3.zero;

        [HideInInspector]
        public int verticalIndex = -100;

        [HideInInspector]
        public float gravityScale;

        [HideInInspector]
        public Color spriteColor;

        [HideInInspector]
        public int animationState = SpriteAnimator.ANIMATION_IS_DELAYED;

        public static string EMPTY_ID = "Empty";
        public static string IDLE_ID = "Idle";
        public static string WALK_ID = "Forward";
        public static string JUMP_UP_ID = "JumpUp";
        public static string JUMP_DOWN_ID = "JumpDown";
        public static string GRAPPLING_FORWARD_ID = "GrapplingForward";
        public static string GRAPPLING_UP_FORWARD_ID = "GrapplingUpForward";
        public static string GRAPPLING_UP_ID = "GrapplingUp";
        public static string JUMP_GRAPPLING_FORWARD_ID = "JumpGrapplingForward";
        public static string JUMP_GRAPPLING_UP_FORWARD_ID = "JumpGrapplingUpForward";
        public static string JUMP_GRAPPLING_UP_ID = "JumpGrapplingUp";
        public static string ATTACK_01_ID = "Melee_01";
        public static string ATTACK_02_ID = "Melee_02";
        public static string ATTACK_03_ID = "Melee_03";
        public static string ATTACK_FINISHER_ID = "Melee_04";
        public static string JUMP_ATTACK = "JumpAttack";
        public static string WITHDRAW = "Withdraw";

        public Vector3 speed = new Vector3(1, 1, 4);

        protected int jumpIndex = 20;
        public float jumpSpeed = 0.2f;

        public float jumpHeight = 3f;

        public virtual void Awake()
        {

            this.unit = new Unit(this);

            Transform tBody = this.transform.Find("Body");
            if (tBody != null) this.body = new Body(tBody.gameObject);

            Transform tAttack = this.transform.Find("Attack");
            if (tAttack != null)
            {

                this.attack = tAttack.GetComponent<Attack>();
                this.attack.parentCharacter = this;
            }

            this.unit.pivot = new Vector2(0.5f, 0.5f);
            this.body.pivot = Vector2.zero;

            this.animator = this.GetComponentInChildren<Animator>();

            this.gravityScale = this.unit.rigidBody.gravityScale;

            float jumpCumulative = 0f;

            while (jumpCumulative < this.jumpHeight)
            {

                this.jumpIndex++;

                jumpCumulative += (this.jumpSpeed * this.jumpIndex * UnityEngine.Time.deltaTime);
            }

            this.jumpIndex = this.jumpIndex / 2;

            this.spriteColor = this.body.renderer.color;
        }

        public virtual void Start() { }

        public virtual void Update() { }

        public virtual void FixedUpdate() { 

            if(this.movement != Vector3.zero) {

                this.unit.movement = Vector3.Scale(this.movement, this.speed) * Time.deltaTime;
                this.movement = Vector3.zero;
            } 
        }

        protected virtual void Walk()
        {

            this.unit.movement = Vector3.Scale(this.movement, this.speed);
        }

        protected virtual void Idle()
        {

            this.unit.movement = Vector3.zero;
            //this.animator.SetInteger("State", CharacterAnimation.IDLE_ID);
        }

        protected virtual void Attack()
        {

            //this.animator.SetInteger("State", CharacterAnimation.ATTACK_01_ID);
        }

        protected virtual void Jump()
        {
            
            if(this.unit.state.isJump == false) {

                this.unit.state.isJump = true;

                this.verticalIndex = this.verticalIndex == -100 ? this.jumpIndex : this.verticalIndex;

                this.StartCoroutine(this.JumpRoutine());
            }
            //this.animator.SetInteger("State", CharacterAnimation.ATTACK_01_ID);
        }

        protected virtual IEnumerator JumpRoutine()
        {

            while (this.unit.state.isJump == true)
            {
                
                this.movement.y = (this.jumpSpeed * this.verticalIndex);

                if (this.verticalIndex <= 0)
                {

                    this.unit.state.isFalling = true;

                    float scanDistance = Mathf.Abs(this.movement.y * this.speed.y * Time.deltaTime + 0.05f);
                    RaycastHit2D hit = Util.HitScan(this.unit, Vector2.down, scanDistance);

                    if (hit.collider != null)
                    {

                        if (hit.collider.tag == "Wall" || hit.collider.tag == "Platform")
                        {

                            bool isLandedValid = false;

                            Collider2D leftHit = Util.HitScan(this.unit, Vector2.left, 0.01f, 0.05f).collider;
                            Collider2D rightHit = Util.HitScan(this.unit, Vector2.right, 0.01f, 0.05f).collider;
                            Collider2D upHit = null;
                            RaycastHit2D[] upHitInfo = Physics2D.RaycastAll(new Vector2(hit.point.x, this.unit.bottom + 0.5f), Vector2.up, 0.1f);

                            if (upHitInfo.Length > 0)
                            {

                                foreach (RaycastHit2D a in upHitInfo) if (a.collider.tag == "Platform") upHit = a.collider;
                            }

                            if (upHit != null)
                                isLandedValid = false;
                            else if (leftHit != null || rightHit != null)
                            {
                                hit = Physics2D.Raycast(new Vector2(leftHit != null ? this.unit.right - 0.1f : this.unit.left + 0.1f, this.unit.bottom - 0.01f), Vector2.down, 0.01f);
                                isLandedValid = hit.collider != null;

                            }
                            else
                                isLandedValid = true;

                            if (isLandedValid == true)
                            {
                               
                                this.JumpStop((0 - hit.distance));
                            }
                        }
                    }
                }

                this.verticalIndex--;

                yield return new WaitForFixedUpdate();
            }
        }

        protected virtual void JumpStop(float jumpRemain = 0)
        {
            this.unit.state.isJump = false;
            this.unit.state.isFalling = false;
            this.unit.state.isJumpDown = false;
            this.unit.rigidBody.gravityScale = this.gravityScale;

            if (jumpRemain < 0) this.unit.movement = new Vector3(0, jumpRemain, 0);
            this.movement.y = 0;

            this.verticalIndex = -100;
        }

        protected ColliderInfo ScanCollider(Vector3 direction, float distance = 0.1f)
        {

            ColliderInfo ret = new ColliderInfo();

            Vector3[] rayPosition = new Vector3[3];

            if (direction == Vector3.left)
            {

                rayPosition[0] = new Vector3(this.body.left - 0.1f, this.body.bottom + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left - 0.1f, this.body.bottom + this.body.heightH, this.body.z);
                rayPosition[2] = new Vector3(this.body.left - 0.1f, this.body.top - 0.1f, this.body.z);

            }
            else if (direction == Vector3.right)
            {

                rayPosition[0] = new Vector3(this.body.right + 0.1f, this.body.bottom + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.right + 0.1f, this.body.bottom + this.body.heightH, this.body.z);
                rayPosition[2] = new Vector3(this.body.right + 0.1f, this.body.top - 0.1f, this.body.z);

            }
            else if (direction == Vector3.up)
            {

                rayPosition[0] = new Vector3(this.body.left + 0.1f, this.body.top + 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left + this.body.widthH, this.body.top + 0.1f, this.body.z);
                rayPosition[2] = new Vector3(this.body.right - 0.1f, this.body.top + 0.1f, this.body.z);

            }
            else if (direction == Vector3.down)
            {

                rayPosition[0] = new Vector3(this.body.left + 0.1f, this.body.bottom - 0.1f, this.body.z);
                rayPosition[1] = new Vector3(this.body.left + this.body.widthH, this.body.bottom - 0.1f, this.body.z);
                rayPosition[2] = new Vector3(this.body.right - 0.1f, this.body.bottom - 0.1f, this.body.z);
            }

            for (int i = 0; i < rayPosition.Length; i++) ret.AddCollider(Physics2D.Raycast(rayPosition[i], direction, distance));

            return ret;
        }
    }
}
