﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SKY.AutoMesh))]
public class MOE_AutoMeshInspector : Editor {

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SKY.AutoMesh autoMesh = (SKY.AutoMesh)target;

        if (GUILayout.Button("Snap")) {

            autoMesh.Snap();
        }
    }
}

