﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SKY.Wall))]
public class MOE_WallInspector : Editor {

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        SKY.Wall wall = (SKY.Wall)target;

        if (GUILayout.Button("Snap")) {

            wall.Snap();
        }
    }
}

