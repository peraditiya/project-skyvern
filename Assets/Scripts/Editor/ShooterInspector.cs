﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SKY
{
    [CustomEditor(typeof(Shooter)), CanEditMultipleObjects]
    public class ShooterInspector : Editor
    {
        public SerializedProperty bullet, source, speed,
        bulletCount, burstCount, fireRate, autoDestroyTime,
        aimPlayer, baseAngle, roundAngles, angle, errorAngle,
        changeSpeed, newSpeed, changeSpeedDelay, changeSpeedTransition,
        changeAngle, newAngle, changeAngleDelay, changeAngleTransition,
        followPlayer, followDelay, followSpeed, followDuration,
        addGravity, force, gravityScale;

        void OnEnable()
        {
            bullet = serializedObject.FindProperty("bullet");
            source = serializedObject.FindProperty("source");
            speed = serializedObject.FindProperty("speed");
            bulletCount = serializedObject.FindProperty("bulletCount");
            burstCount = serializedObject.FindProperty("burstCount");
            fireRate = serializedObject.FindProperty("fireRate");
            autoDestroyTime = serializedObject.FindProperty("autoDestroyTime");
            aimPlayer = serializedObject.FindProperty("aimPlayer");
            baseAngle = serializedObject.FindProperty("baseAngle");
            roundAngles = serializedObject.FindProperty("roundAngles");
            angle = serializedObject.FindProperty("gapAngle");
            errorAngle = serializedObject.FindProperty("errorAngle");
            changeSpeed = serializedObject.FindProperty("changeSpeed");
            newSpeed = serializedObject.FindProperty("newSpeed");
            changeSpeedDelay = serializedObject.FindProperty("changeSpeedDelay");
            changeSpeedTransition = serializedObject.FindProperty("changeSpeedTransition");
            changeAngle = serializedObject.FindProperty("changeAngle");
            newAngle = serializedObject.FindProperty("newAngle");
            changeAngleDelay = serializedObject.FindProperty("changeAngleDelay");
            changeAngleTransition = serializedObject.FindProperty("changeAngleTransition");
            followPlayer = serializedObject.FindProperty("followPlayer");
            followDelay = serializedObject.FindProperty("followDelay");
            followSpeed = serializedObject.FindProperty("followSpeed");
            followDuration = serializedObject.FindProperty("followDuration");
            addGravity = serializedObject.FindProperty("addGravity");
            force = serializedObject.FindProperty("force");
            gravityScale = serializedObject.FindProperty("gravityScale");
        }

        override public void OnInspectorGUI()
        {
            serializedObject.Update();
            // Shooter s = target as Shooter;

            if (Application.isPlaying)
            {
                if (GUILayout.Button("Test Shoot"))
                {
                    for (int i = 0; i < targets.Length; i++)
                    {
                        (targets[i] as Shooter).gameObject.GetComponent<Shooter>().Shoot();
                    }

                }
            }

            EditorGUILayout.PropertyField(bullet, true);
            EditorGUILayout.PropertyField(source, true);

            EditorGUILayout.PropertyField(speed, true);
            EditorGUILayout.PropertyField(bulletCount, true);
            EditorGUILayout.PropertyField(burstCount, true);
            EditorGUILayout.PropertyField(fireRate, true);
            EditorGUILayout.PropertyField(autoDestroyTime, true);
            EditorGUILayout.PropertyField(aimPlayer, true);

            if (!aimPlayer.boolValue)
            {
                EditorGUILayout.PropertyField(baseAngle, true);
            }
            EditorGUILayout.PropertyField(errorAngle, true);
            EditorGUILayout.PropertyField(roundAngles, true);
            if (!roundAngles.boolValue)
            {
                EditorGUILayout.PropertyField(angle, true);
            }

            if (!addGravity.boolValue && !changeAngle.boolValue)
            {
                EditorGUILayout.PropertyField(followPlayer, true);
                if (followPlayer.boolValue)
                {
                    EditorGUILayout.PropertyField(followSpeed, true);
                    EditorGUILayout.PropertyField(followDuration, true);
                    EditorGUILayout.PropertyField(followDelay, true);
                }
            }

            if (!addGravity.boolValue && !followPlayer.boolValue)
            {
                EditorGUILayout.PropertyField(changeAngle, true);
                if (changeAngle.boolValue)
                {
                    EditorGUILayout.PropertyField(newAngle, true);
                    EditorGUILayout.PropertyField(changeAngleTransition, true);
                    EditorGUILayout.PropertyField(changeAngleDelay, true);
                }
            }

            if (!addGravity.boolValue)
            {
                EditorGUILayout.PropertyField(changeSpeed, true);
                if (changeSpeed.boolValue)
                {
                    EditorGUILayout.PropertyField(newSpeed, true);
                    EditorGUILayout.PropertyField(changeSpeedTransition, true);
                    EditorGUILayout.PropertyField(changeSpeedDelay, true);
                }
            }

            if (!followPlayer.boolValue && !changeAngle.boolValue)
            {
                EditorGUILayout.PropertyField(addGravity, true);
                if (addGravity.boolValue)
                {
                    EditorGUILayout.PropertyField(force, true);
                    EditorGUILayout.PropertyField(gravityScale, true);
                }
            }

            serializedObject.ApplyModifiedProperties();

        }

    }
}