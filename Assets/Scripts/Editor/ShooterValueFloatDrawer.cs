﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ShooterValueFloat))]
public class ShooterValueFloatDrawer : PropertyDrawer {
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label){
		// Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

		SerializedProperty sp = property.FindPropertyRelative("type");
		
		float w = position.width / 3;
        // Calculate rects
		
        var minRect = new Rect(position.x, position.y, w, position.height);
        var maxRect = new Rect(position.x + w + 5, position.y, w, position.height);
        var typeRect = new Rect(position.x + w+w+10, position.y, w-10, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
		if(sp.boolValue){
			EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);
			EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);
			EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);	
		}else{
			EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);
			// EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);
			EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
		}
        

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
	}

}

[CustomPropertyDrawer(typeof(ShooterValueInt))]
public class ShooterValueIntDrawer : PropertyDrawer {
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label){
		// Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.
		EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

		SerializedProperty sp = property.FindPropertyRelative("type");
		
		float w = position.width / 3;
        // Calculate rects
		
        var minRect = new Rect(position.x, position.y, w, position.height);
        var maxRect = new Rect(position.x + w + 5, position.y, w, position.height);
        var typeRect = new Rect(position.x + w+w+10, position.y, w-10, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
		if(sp.boolValue){
			EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);
			EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);
			EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);	
		}else{
			EditorGUI.PropertyField(minRect, property.FindPropertyRelative("min"), GUIContent.none);
			// EditorGUI.PropertyField(maxRect, property.FindPropertyRelative("max"), GUIContent.none);
			EditorGUI.PropertyField(typeRect, property.FindPropertyRelative("type"), GUIContent.none);
		}
        

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
	}

}
