﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderProperties : MonoBehaviour {
    public float left;
    public float right;

    public int currentPosID = 0;
    public float handleHeight;

    private bool isInGame = false;
    private Vector3 handlePos;

    public void ChangePos () {
        currentPosID++;
        if (currentPosID > 1) currentPosID = 0;
    }

    void Start(){
        isInGame = true;
        handlePos = transform.position;
    }

    public float[] GetWanderPos(){
        return new float[] { transform.position.x + left, transform.position.x + right };
    }

    // private void OnValidate () {
    //     if (!pos) {
    //         GameObject posGo = new GameObject ("Wander Position");
    //         posGo.transform.SetParent (this.transform);
    //         pos = posGo.transform;
    //         pos.localPosition = Vector3.zero;

    //         GameObject leftGo = new GameObject ("Left");
    //         leftGo.transform.SetParent (pos);
    //         leftTransform = leftGo.transform;

    //         GameObject rightGo = new GameObject ("Right");
    //         rightGo.transform.SetParent (pos);
    //         rightTransform = rightGo.transform;
    //     }
    // }

    private void OnDrawGizmosSelected () {
            Gizmos.color = Color.red;
            if(!isInGame){
                handlePos = transform.position;
            }
            Gizmos.DrawLine (handlePos + new Vector3 (left, handleHeight * .5f, 0), handlePos + new Vector3 (left, -handleHeight * .5f, 0));
            Gizmos.DrawLine (handlePos + new Vector3 (right, handleHeight * .5f, 0), handlePos + new Vector3 (right, -handleHeight * .5f, 0));
            
    }
}