﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class HostileDragun : Hostile
    {

        
        public override void Start() {

            base.Start();

            this.unit.rigidBody.gravityScale = 0f;
        }

        public override void Update() {

            base.Update();
        }

        public override void FixedUpdate() {

            base.FixedUpdate();
        }

        void OnCollisionEnter2D(Collision2D collision) {

            base.OnCollisionEnter2D(collision);
        }
    }
}