﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace SKY {

    public class IHE_Boss : IHE_Enemy {

        struct RockProp {
            public float position;
            public bool side;
        }

        public enum State { Phase1, Phase2, Phase3, PhaseDead }

        [Header ("Prefabs")]
        public GameObject[] prefabs;

        [Header ("Properties")]
        public State state;
        public Transform[] pos;
        public Transform pathTransform;

        public HangPos hangPosA;
        public HangPos hangPosB;
        public HangPos hangPosC;
        Vector3[] path;

        // private Shooter[] shooters;
        Dictionary<string, Shooter> shooters = new Dictionary<string, Shooter> ();

        private bool coroutineStopper = false;

        CameraFollow cameraFollow;

        Coroutine waveShootCoroutine;

        Transform sphere;

        ARA.WeaponBlunt dashAttack;

        public override void Start () {
            base.Start ();
            cameraFollow = FindObjectOfType<CameraFollow> ();
            this.dashAttack = this._transform.Find("DashAttack").gameObject.GetComponent<ARA.WeaponBlunt>();

            for (int i = 0; i < prefabs.Length; i++) {
                prefabs[i].CreatePool ();
            }
            //target = GameObject.FindWithTag ("Player").transform;
            target = ARA.Root.player.transform;
            foreach (Shooter s in GetComponentsInChildren<Shooter> ()) {
                s.SetTarget (this.target);
                shooters.Add (s.gameObject.name, s);
            }

            //init path
            Transform[] temp = pathTransform.GetComponentsInChildren<Transform> ();
            path = new Vector3[temp.Length - 1];
            for (int i = 0; i < path.Length; i++) {
                path[i] = temp[i + 1].position;
            }

            ChangeState (state);
        }

        private void ChangeState (State newState) {
            
            if(state != State.PhaseDead) {
                state = newState;

                if (currentCoroutine != null) StopCoroutine (currentCoroutine);
                currentCoroutine = StartCoroutine (newState.ToString ());
            }
        }

        IEnumerator Phase1 () {
            this.cameraFollow.property.height = 11;

            ChangeText (state.ToString (), Color.green);

            // assign positions for boss movement
            Vector3[] destPos = new Vector3[] {
                pos[0].position,
                pos[1].position
            };
            Vector3[] destPos2 = new Vector3[] {
                pos[3].position,
                pos[4].position
            };

            Vector3 centerPos = pos[2].position;
            int curPos = 0;
            while (state == State.Phase1) {
                PlayAnimCrossFade ("Boss Idle");
                yield return new WaitForSeconds (1f);

                // move left-right + bullet pattern
                for (int i = 0; i < 2; i++) {
                    FaceSprite (_transform.position.x < centerPos.x);
                    MoveWithParabolic (_transform, destPos[curPos], -3f, 2f);
                    PlayAnimCrossFade ("Boss Flying");
                    yield return new WaitForSeconds (2f);
                    FaceSprite (_transform.position.x < destPos2[curPos].x);
                    MoveWithParabolic (_transform, destPos2[curPos++], 1f, 1.5f, Ease.OutCirc);
                    yield return new WaitForSeconds (1.5f);
                    PlayAnimCrossFade ("Boss Idle");
                    yield return new WaitForSeconds (1.5f);
                    PlayAnimCrossFade ("Boss Flying Roar");
                    yield return new WaitForSeconds (1f);
                    if (curPos >= destPos.Length) curPos = 0;
                    StartCoroutine (Shoot (shooters["Red Bullet"]));
                    StartCoroutine (Shoot (shooters["Green Bullet"]));
                    yield return new WaitForSeconds (shooters["Red Bullet"].GetShootDuration () + 1f);
                    PlayAnimCrossFade ("Boss Idle");
                }
                yield return new WaitForSeconds (2f);

                // throwing rocks phase

                MoveWithParabolic (_transform, centerPos, 1f, 1f);
                PlayAnimCrossFade ("Boss Flying");
                yield return new WaitForSeconds (1f);

                PlayAnimCrossFade ("Boss Flying Roar");
                yield return new WaitForSeconds (1f);
                // throws the damn rocks
                float left = 29f;
                float right = 65f;
                int count = shooters["Rocks"].bulletCount.GetValue ();
                float error = .5f;
                float range = (right - left) / (count + 1);
                List<RockProp> rockPos = new List<RockProp> ();
                for (int i = 1; i <= count; i++) {
                    RockProp rp = new RockProp ();
                    rp.position = left + (range * i + Random.Range (-error, error));
                    rp.side = (i == 1 || i == count || i == (int) count / 2) ? true : false;
                    rockPos.Add (rp);
                    // print(range * i);
                }

                for (int i = 0; i < count; i++) {
                    int random = Random.Range (0, rockPos.Count);
                    float posX = rockPos[random].position;
                    bool side = rockPos[random].side;
                    rockPos.RemoveAt (random);

                    IHE_Rock r = shooters["Rocks"].bullet.Spawn (new Vector3 (posX, 25f, 0)).GetComponent<IHE_Rock> ();
                    r.Gravity (1f, 0, 3);
                    r.destroyOnGround = true;
                    if (side)
                        r.destroyOnGround = false;
                    else
                        r.destroyOnGround = true;
                    yield return new WaitForSeconds (.3f);
                }
                PlayAnimCrossFade ("Boss Idle");
                yield return new WaitForSeconds (3f);

                // dash
                // this.Flashing(Color.red, .5f);
                PlayAnimCrossFade ("Boss Diving");
                //Vector3 divingTarget = target.position;
                Vector3 divingTarget = ARA.Root.player.transform.position;
                yield return new WaitForSeconds (1.2f);
                FaceTo (divingTarget);
                _transform.DOMove (divingTarget, .5f).SetEase (Ease.InQuad);
                yield return new WaitForSeconds (.5f);
                this.dashAttack.Activate();
                IHE_Rock[] rocks = FindObjectsOfType<IHE_Rock> ();
                if(ARA.Root.player.states.isRetract || ARA.Root.player.states.isGrappling) yield return new WaitForSeconds (1f);
                for (int i = 0; i < rocks.Length; i++) {
                    if (rocks[i].transform.Find ("Aria 1")) {

                        ARA.AbilityGrappling grappling = ARA.Root.player.abilities["Grappling"] as ARA.AbilityGrappling;
                        ARA.AbilityJump jump = ARA.Root.player.abilities["Jump"] as ARA.AbilityJump;
                        grappling.grappler.DeActivate ();
                        jump.Trigger ();
                    }
                    rocks[i].Recycle ();
                }
                cameraFollow.Vibrate (1f, .2f, 2f, 1.5f);
                yield return new WaitForSeconds (1.5f);
            }
        }

        IEnumerator Shoot (Shooter s) {
            for (int i = 0; i < s.bulletCount.GetValue (); i++) {
                if (coroutineStopper) yield break;
                s.Shoot ();
                yield return new WaitForSeconds (s.fireRate.GetValue ());
            }
        }

        void MoveWithParabolic (Transform tran, Vector3 dest, float y, float duration, Ease ease = Ease.OutQuad) {
            tran.DOPath (new Vector3[] { new Vector3 (_transform.position.x + (dest.x - _transform.position.x) * .5f, dest.y + y, 0), dest }, duration, PathType.CatmullRom).SetEase (ease);
        }

        IEnumerator Idle () {
            while (true) {
                Tweener tUp = _transform.DOMoveY (_transform.position.y + 1f, 1f);
                yield return tUp.WaitForCompletion ();
                Tweener tDown = _transform.DOMoveY (_transform.position.y - 1f, 1f);
                yield return tDown.WaitForCompletion ();
            }
        }

        IEnumerator Phase2 () {
            this.cameraFollow.property.height = 11;

            ChangeText (state.ToString (), Color.green);
            print ("phase 2 started");

            Vector3 centerPos = pos[2].position;
            MoveWithParabolic (_transform, centerPos, 1f, 1f);
            PlayAnimCrossFade ("Boss Flying");
            yield return new WaitForSeconds (1f);

            // check hangPos
            hangPosA.gameObject.SetActive (true);
            PlayAnimCrossFade ("Boss Idle");
            Shooter s = shooters["Falling Bullet"];
            float shootTimer = s.fireRate.GetValue ();
            float flyingSpeed = 5f;
            int curID = 0;
            while (true) {
                if (hangPosA.isUsed || (this.target.position.x > 200)) {
                    if (Vector2.Distance (_transform.position, path[curID]) >.5f) {
                        Vector2 dir = (path[curID] - _transform.position).normalized;
                        _transform.Translate (dir * flyingSpeed * Time.fixedDeltaTime);
                    } else {
                        if (curID < path.Length - 1) curID++;
                        else {
                            ChangeState (State.Phase3);
                            yield break;
                        }
                    }
                    if (curID < 3) {
                        shootTimer -= Time.deltaTime;
                        if (shootTimer <= 0) {
                            s.Shoot ();
                            shootTimer = s.fireRate.GetValue ();
                        }
                    } else {

                        flyingSpeed = 12f;
                        this.cameraFollow.property.height = 5;
                    }
                }

                yield return new WaitForFixedUpdate ();
            }

        }
        IEnumerator Phase3 () {
            this.cameraFollow.property.height = 12;

            this.hangPosA.gameObject.active = true;
            this.hangPosB.gameObject.active = true;
            //this.hangPosC.gameObject.active = true;

            this._transform.Find("Platform").gameObject.active = true;

            this.sphere = this.shooters["Sphere"].transform;
            
            FaceSprite (false);

            float bossMaxVerticalPosition = 115f;
            float bossMinVerticalPosition = 85f;
            float bossMaxHorizontalPosition = 323f;
            float bossMinHorizontalPosition = 290f;

            int doPickup = 0;

            float rand;
            int peak = 0;

            ChangeText (state.ToString (), Color.green);
            print ("phase 3 started");
            _transform.position = path[path.Length - 1];

            PlayAnim ("Boss Flying");

            while (true) {

                if (this.target.position.y < bossMinVerticalPosition && doPickup == 0) doPickup = 1;
                else if ((this.hangPosA.isUsed || this.hangPosB.isUsed /*|| this.hangPosC.isUsed*/ ) && doPickup == 1) doPickup = 2;

                if (doPickup > 0) {

                    Vector3 currentPosition = this._transform.position;

                    if (doPickup == 1) {

                        if (this._transform.position.x > bossMinHorizontalPosition) currentPosition.x -= 0.2f;
                        else if (this._transform.position.y > bossMinVerticalPosition) currentPosition.y -= 0.2f;

                    } else if (doPickup == 2) {

                        if (this._transform.position.y < bossMaxVerticalPosition) currentPosition.y += 0.2f;
                        else if (this._transform.position.x < bossMaxHorizontalPosition) currentPosition.x += 0.2f;
                        else
                            doPickup = 0;
                    }

                    this._transform.position = currentPosition;

                } else if (doPickup == 0) {

                    if (peak == 0) peak = Random.Range (4, 8);

                    this.cameraFollow.property.height = 5;

                    if (peak > 1) {

                        if (this.waveShootCoroutine == null) {

                            yield return new WaitForSeconds (1f);

                            this.waveShootCoroutine = this.StartCoroutine (WaveShoot ());
                        }

                        peak--;
                    
                    } else {

                        if (this.waveShootCoroutine == null && (ARA.Root.player.states.isBossRide || ARA.Root.player.states.isHooked)) {
                            
                            doPickup = -1;

                            Tweener tLeft = _transform.DOMoveX (_transform.position.x - 7f, 1f);
                            yield return tLeft.WaitForCompletion ();

                            tLeft = _transform.DOMoveX (_transform.position.x + 50f, 1f);
                            yield return tLeft.WaitForCompletion ();

                            tLeft = _transform.DOMoveX (_transform.position.x - 43f, 3f);
                            yield return tLeft.WaitForCompletion ();

                            doPickup = 0;
                        }

                        peak = 0;
                    }
                }

                yield return new WaitForFixedUpdate ();
            }
        }

        IEnumerator WaveShoot () {

            //yield return new WaitForSeconds (100f);

            bool isShootingDone = false;

            float delay = 0.05f;

            Vector3 temp = (ARA.Root.player.transform.position - this.sphere.position);
            float bangle = Mathf.Atan2 (temp.y, temp.x) * Mathf.Rad2Deg;
            float deg = Quaternion.AngleAxis (bangle - 90, Vector3.forward).eulerAngles.z;

            Shooter s = this.shooters["Sphere"];

            yield return new WaitForSeconds (0.5f);

            for (int i = 0; i < 20; i++) {
                if (!isShootingDone) {

                    delay = 0.05f;

                    this.sphere.localScale = new Vector3 (this.sphere.localScale.x + 0.1f, this.sphere.localScale.y + 0.1f, 1);

                    if (i == 9) isShootingDone = true;

                } else {

                    delay = 0.3f;

                    this.sphere.localScale = new Vector3 (this.sphere.localScale.x - 0.1f, this.sphere.localScale.y - 0.1f, 1);

                    s.baseAngle.max = deg;
                    s.baseAngle.min = deg;
                    s.Shoot ();

                    if (this.sphere.localScale.x == 0) isShootingDone = false;
                }

                yield return new WaitForSeconds (delay);
            }

            this.waveShootCoroutine = null;
        }

        public override void Hit (float damage) {

            this.Hit(damage, this._transform);
        }

        public override void Hit (float damage, Transform source) {
            base.Hit (damage, source);

            Debug.Log(health.currentHealth);

            health.Hit (damage);
            if (health.currentHealth <= 0) {

                if(ARA.Root.player.states.isHooked) {

                    ARA.Root.player.GetAbility<ARA.AbilityJump>("Jump").Trigger(8);    
                    ARA.Root.player.GetAbility<ARA.AbilityGrappling>("Grappling").grappler.DeActivate();
                } 

                ChangeState (State.PhaseDead);
            }

            if (health.currentTresshold >= health.phaseTresshold.Length) return;
            
            if (!health.phaseTresshold[health.currentTresshold].started && health.currentHealth < health.phaseTresshold[health.currentTresshold].healthMin) {
                health.phaseTresshold[health.currentTresshold].started = true;
                health.currentTresshold++;
                coroutineStopper = true;
                if(state != State.PhaseDead) ChangeState ((State) (health.currentTresshold));
                print ("tres");
            }
        }

        IEnumerator PhaseDead(){
            ChangeText (state.ToString (), Color.green);
            print("Dead");
            foreach(Collider2D col in GetComponentsInChildren<Collider2D>()){
                col.enabled = false;
            }

            yield return new WaitForSeconds(1);
            // _transform.DOMoveY(_transform.position.y - 50f, 15f).SetEase(Ease.Linear);
            for (int i = 0; i < 5; i++)
            {
                Flashing(Color.white, .3f);
                yield return new WaitForSeconds(.32f);
            }
            
            ChangeColor(Color.white, 2f);
            yield return new WaitForSeconds(2f);
            FadeOut(3f);
            yield return new WaitForSeconds(3.5f);

            Destroy (gameObject);   
        }
    }
}