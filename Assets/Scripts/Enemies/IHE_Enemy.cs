﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace SKY
{
	// [RequireComponent(typeof(BoxCollider2D))]
	// [RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(IHE_UnitHealth))]
    public class IHE_Enemy : MonoBehaviour
    {

        [Header("Base Properties")]
        public IHE_UnitHealth health;
		public bool autoFindTarget = false;
        public Transform target;
        public Transform visual;

		[Header("Animations")]
        protected Animator _animator;

        
		protected bool facingRight = true;

		protected Transform _transform;
		protected Rigidbody2D _rigidbody;

		private UnityEngine.UI.Text stateText;

        private bool isTinting = false;
		private SpriteRenderer[] _sprites;

        protected Coroutine currentCoroutine;
        
        private Vector2 scale = Vector2.one;
        private string curAnim = "";

		public virtual void Awake(){
			_transform = transform;
            _rigidbody = GetComponent<Rigidbody2D>();
            stateText = GetComponentInChildren<UnityEngine.UI.Text>();
            _sprites = GetComponentsInChildren<SpriteRenderer>();
            _animator = GetComponentInChildren<Animator>();
            health = GetComponent<IHE_UnitHealth>();
            if(visual) scale = visual.localScale;
            else Debug.LogWarning("Visual is null on this: "+gameObject.name);
		}

		public virtual void Start(){
			if(autoFindTarget)
				target = GameObject.FindGameObjectWithTag("Player").transform;
		}

        protected void PlayAnim(string animName){
            if(curAnim.Equals(animName))
                return;
            
            _animator.Play(animName);
            curAnim = animName;
        }

        protected void PlayAnimForce(string animName, float startTime){
            _animator.Play(animName, 0, startTime);
            curAnim = animName;
        }
        
        protected void PlayAnimCrossFade(string animName, float transitionDuration = .1f){
            if(curAnim.Equals(animName))
                return;
            
            _animator.CrossFade(animName, transitionDuration);
            curAnim = animName;
        }

		protected virtual void FaceSprite(bool right)
        {
			facingRight = right;
            visual.localScale = facingRight ? new Vector2(-scale.x, scale.y) : new Vector2(scale.x, scale.y);
        }

        protected int GetDir(){
            return facingRight ? 1 : -1;
        }

        protected void FaceTo(Vector3 targetPos){
            int dir = _transform.position.x > targetPos.x ? -1 : 1;
            FaceSprite(dir == 1);
        }

		protected void ChangeText(string newText, Color textColor)
        {
            stateText.text = newText;
            stateText.color = textColor;
        }

		public virtual void Hit(float damage){
                
                StopCoroutine("Tint");
                StartCoroutine("Tint");
        }

        public virtual void Hit(float damage, Transform source){
            
            StopCoroutine("Tint");
            StartCoroutine("Tint");
            target = source;
        }

		private IEnumerator Tint(){
            float amount = 1f;
            while(amount > 0){
                for (int i = 0; i < _sprites.Length; i++)
                {
                    _sprites[i].material.SetColor("_FlashColor", Color.white) ;
                    _sprites[i].material.SetFloat("_FlashAmount", amount) ;
                }
                amount -= Time.deltaTime * 2;
                yield return null;
            }
        }

        public void Flashing(Color color, float duration){
            StartCoroutine(DoFlashing(color, duration));
        }

        public void ChangeColor (Color color, float duration) {
            StartCoroutine (DoChangeColor (color, duration));
        }

        private IEnumerator DoChangeColor (Color color, float duration) {
            float amount = 0;
            DOTween.To (() => amount, x => amount = x, 1f, duration);
            while (amount <= 1f) {
                for (int i = 0; i < _sprites.Length; i++) {
                    _sprites[i].material.SetColor ("_FlashColor", color);
                    _sprites[i].material.SetFloat ("_FlashAmount", amount);
                }
                yield return null;
            }
        }

        private IEnumerator DoFlashing(Color color, float duration){
            float amount = 1f;
            float rate = amount / duration;
            while(duration > 0){
                for (int i = 0; i < _sprites.Length; i++)
                {
                    _sprites[i].material.SetColor("_FlashColor", color) ;
                    _sprites[i].material.SetFloat("_FlashAmount", amount) ;
                }
                duration -= Time.deltaTime;
                amount -= rate * Time.deltaTime;
                yield return null;
            }
        }

        public void FadeOut(float duration){
            StartCoroutine(DoFadeOut(duration));
        }

        private IEnumerator DoFadeOut(float duration){
            float amount = 1f;
            DOTween.To (() => amount, x => amount = x, 0, duration);
            while (amount >= 0) {
                for (int i = 0; i < _sprites.Length; i++) {
                    _sprites[i].color = new Color(1,1,1,amount);
                }
                yield return null;
            }
        }
    }
}
