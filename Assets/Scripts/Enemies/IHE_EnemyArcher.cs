﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
    public class IHE_EnemyArcher : IHE_Enemy {
        [Header ("Prefabs")]
        public GameObject blood;
        public GameObject ragdoll;

        public enum State {
            Idle,
            Walk,
            DoHit,
            Attack,
            AttackHeavy,
            Jump,
            Dead
        }

        [Header ("Properties")]
        public State state;
        public float walkSpeed;
        [Range (5, 100)]
        public float attackChance = 50f;
        public float minRangeToAttack = 2f;
        public float maxRangeToAttack = 5f;

        private Shooter shooter;
        private Shooter shooterHeavy;
        private bool justAttacking = false;

        private ARA.AbilityJump jump;
        public ARA.Character character;

        public override void Awake () {
            base.Awake ();
        }

        public override void Start () {
            base.Start ();

            Transform shooterContainer = this._transform.Find ("Shooter");

            shooter = shooterContainer.Find ("Spread").gameObject.GetComponent<Shooter> ();
            shooterHeavy = shooterContainer.Find ("Focus").gameObject.GetComponent<Shooter> ();
            shooter.SetTarget (target);
            shooterHeavy.SetTarget (target);

            this.jump = this.GetComponent<ARA.AbilityJump> ();
            this.jump.parentCharacter = this.GetComponent<ARA.Character> ();

            ChangeState (state);
        }

        private void ChangeState (State newState) {

            if (this.state != newState || this.state == State.Idle) {

                state = newState;
                if (currentCoroutine != null) StopCoroutine (currentCoroutine);

                currentCoroutine = StartCoroutine (newState.ToString ());
            }
        }

        IEnumerator Idle () {
            ChangeText (state.ToString (), Color.green);
            _animator.Play ("idle");
            ChangeFacing ();

            while (state == State.Idle) {

                float idleTime = Random.Range (0.1f, 1f);
                yield return new WaitForSeconds (idleTime);

                if (Random.Range (0, 101) < attackChance && justAttacking == false) {

                    print ("attack");

                    if (this.targetDistanceAbsolute > (this.maxRangeToAttack - 1)) ChangeState (State.AttackHeavy);
                    else if (Random.Range (0, 101) > 70) ChangeState (State.Jump);
                    else { ChangeState (State.Attack); }
                    justAttacking = true;

                } else {

                    ChangeState (State.Walk);
                }
            }
        }

        IEnumerator Jump () {
            
            Debug.Log("Jump");
            ChangeText (state.ToString (), Color.yellow);
            this._animator.Play ("jump");
            this.jump.Trigger ();

            yield return new WaitForSeconds (0.2f);
            this.jump.JumpBreak ();
            yield return new WaitForSeconds (0.5f);
            this.shooter.Shoot ();
            yield return new WaitForSeconds (0.2f);
            this.jump.JumpResume ();

            while (this.jump.parentCharacter.states.isJump) yield return new WaitForFixedUpdate ();
            ChangeState (State.Idle);

            Debug.Log("Selesai");
        }

        IEnumerator Walk () {

            ChangeText (state.ToString (), Color.yellow);
            _animator.Play ("walk");
            ChangeFacing ();

            float destiny = this.target.position.x;
            float aggressivenessDistance = Random.Range (this.minRangeToAttack, this.maxRangeToAttack);
            float defensiveDistance = Random.Range (this.minRangeToAttack - 0.1f, aggressivenessDistance);
            bool isTargetOnTheRightSide = false;

            while (state == State.Walk) {
                isTargetOnTheRightSide = (this.target.position.x > this._transform.position.x);

                float horizontalMovement = 0f;

                if (!this.justAttacking) {

                    if (this.targetDistanceAbsolute > aggressivenessDistance) {

                        horizontalMovement = walkSpeed * (isTargetOnTheRightSide ? 1 : -1);

                    } else if (this.targetDistanceAbsolute < defensiveDistance) {

                        horizontalMovement = walkSpeed * (isTargetOnTheRightSide ? -1 : 1);
                    }

                } else
                    this.justAttacking = false;

                Debug.Log (horizontalMovement);

                if (horizontalMovement != 0) _rigidbody.velocity = new Vector2 (horizontalMovement, _rigidbody.velocity.y);
                else { ChangeState (State.Idle); }

                yield return null;
            }
        }

        IEnumerator Attack () {
            ChangeText (state.ToString (), Color.yellow);
            _animator.Play ("shoot");

            while (state == State.Attack) {
                // attacking
                //sword.Activate(1.3f, .1f);
                yield return new WaitForSeconds (0.6f);
                shooter.Shoot ();
                yield return new WaitForSeconds (2f);
                ChangeState (State.Idle);
            }
        }

        IEnumerator AttackHeavy () {
            ChangeText (state.ToString (), Color.yellow);
            _animator.Play ("shoot");

            while (state == State.AttackHeavy) {
                // attacking
                //sword.Activate(1.3f, .1f);
                yield return new WaitForSeconds (1f);
                shooterHeavy.Shoot ();
                yield return new WaitForSeconds (2f);
                ChangeState (State.Idle);
            }
        }

        public override void Hit (float damage) {
            base.Hit (damage);
            if (health.Hit (damage)) {
                ChangeState (State.Dead);
            }
            ChangeState (State.DoHit);
        }

        IEnumerator DoHit () {
            ChangeText (state.ToString (), Color.grey);
            GameObject go = Instantiate (blood, _transform.position, Quaternion.Euler (-90, 0, 0));
            Destroy (go, 2f);
            _animator.Play ("hit");
            int dir = _transform.position.x > target.position.x ? 1 : -1;
            _rigidbody.position = _rigidbody.position + new Vector2 (.2f * dir, 0);

            while (state == State.DoHit) {
                yield return new WaitForSeconds (.5f);
                ChangeState (State.Idle);
            }
        }

        IEnumerator Dead () {
            ChangeText (state.ToString (), Color.black);
            while (state == State.Dead) {
                GameObject go = Instantiate (ragdoll, _transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<IHE_Ragdoll> ().Explode (_transform.position.x > target.position.x ? true : false);
                Destroy (this.gameObject);
                yield return null;
            }
        }

        private void ChangeFacing () {
            FaceSprite (_transform.position.x < target.position.x);
        }

        private float targetDistance {
            get { return this.transform.position.x - this.target.position.x; }
        }

        private float targetDistanceAbsolute {
            get { return Mathf.Abs (this.targetDistance); }
        }
    }
}