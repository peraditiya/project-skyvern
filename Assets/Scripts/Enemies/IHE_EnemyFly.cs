﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SKY
{
    public class IHE_EnemyFly : IHE_Enemy
    {
		public enum State
        {
            Idle, Chase, DoHit, Attack, Dead
        }
		[Header("Prefabs")]
		public GameObject bullet;
		public GameObject blood;
        public GameObject ragdoll;

		[Header("Properties")]
		public State state;
		public float speed;
		public float attackTime;

		// private BulletManager bm;
		private Tweener curTween;

		public override void Start(){
			base.Start();
			// bm = GameObject.FindObjectOfType<BulletManager>();
			ChangeState(state);
		}

		private void ChangeState(State newState)
        {
            state = newState;
            if (currentCoroutine != null)
                StopCoroutine(currentCoroutine);
            currentCoroutine = StartCoroutine(newState.ToString());
        }

		IEnumerator Idle(){
            ChangeText(state.ToString(), Color.green);
			// float amp = .5f;
			// float idle = .3f;
			// Vector3 stay = _transform.position;
			while(state == State.Idle){
				// Vector3 dest = stay + new Vector3(Random.Range(-amp, amp), Random.Range(-amp, amp), 0);
				// yield return new WaitForSeconds(idle);
				// curTween = _transform.DOMove(dest, .3f);
				// yield return curTween.WaitForCompletion();

				if(target)
					ChangeState(State.Chase);

				yield return null;
			}
		}

		IEnumerator Chase(){
			ChangeText(state.ToString(), Color.yellow);
			Vector3 velocity = Vector3.zero;
			float x = Random.Range(4f, 7f);
			x *= _transform.position.x < target.position.x ? -1 : 1;
			Vector3 roam =  new Vector3(x, Random.Range(2f, 4f));
			float curSpeed = speed * 3;
			float timeToAttack = 2f;
			while(state == State.Chase){
				
				if(!target){
					ChangeState(State.Idle);
					break;
				}

				FaceSprite(_transform.position.x > target.position.x);
					

				_transform.position = Vector3.SmoothDamp(_transform.position, target.position + roam, ref velocity, curSpeed);
				if(Vector3.Distance(_transform.position, target.position) < 4f)
					curSpeed = speed;

				timeToAttack -= Time.deltaTime;
				if(timeToAttack <= 0)
					ChangeState(State.Attack);

				yield return null;
			}
		}

		IEnumerator Attack(){
			ChangeText(state.ToString(), Color.red);
			while(state == State.Attack){
				yield return new WaitForSeconds(.5f);
				// bm.ShootAim(bullet, _transform, _transform.position, target, 7f, 2, 5f, 5f, 5f);
				yield return new WaitForSeconds(1f);
				ChangeState(State.Idle);
			}
			
		}

		public override void Hit(float damage){
            base.Hit(damage);
			GameObject go = Instantiate(blood, _transform.position, Quaternion.Euler(-90,0,0));
            Destroy(go, 2f);
			if(health.Hit(damage)){
                ChangeState(State.Dead);
            }
		}

		IEnumerator Dead(){
            ChangeText(state.ToString(), Color.black);
            while (state == State.Dead)
            {
				GameObject go =  Instantiate(ragdoll, _transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<IHE_Ragdoll>().Explode(_transform.position.x > target.position.x ? true : false);
                Destroy(this.gameObject);
                yield return null;
            }
        }

    }
}
