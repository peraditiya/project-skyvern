﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
	public class IHE_EnemyPart : MonoBehaviour {

		IHE_Enemy enemy;

		private void Awake() {
			enemy = GetComponentInParent<IHE_Enemy>();	
		}

		public void Hit(float damage, Transform source){
            enemy.Hit(damage, source);
        }
	}
}