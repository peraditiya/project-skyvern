﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    public class IHE_EnemyTurret : IHE_Enemy
    {
		public enum State
        {
            Idle, DoHit, Attack, Dead
        }

		[Header("Properties")]
		public State state;
		public float fireRate;
		public GameObject bullet;

		// private BulletManager bm;
		private Shooter shooter;

		public override void Start(){
			base.Start();
			ChangeState(state);
			bullet.CreatePool(5);
			// bm = GameObject.FindObjectOfType<BulletManager>();
			shooter = GetComponentInChildren<Shooter>();
		}

		private void ChangeState(State newState)
        {
            state = newState;
            if (currentCoroutine != null)
                StopCoroutine(currentCoroutine);
            currentCoroutine = StartCoroutine(newState.ToString());
        }

		IEnumerator Idle(){
            ChangeText(state.ToString(), Color.green);
			while(state == State.Idle){
				if(target)
					ChangeState(State.Attack);
				yield return null;
			}
		}

		IEnumerator Attack(){
			ChangeText(state.ToString(), Color.green);
			shooter.SetTarget(target);
			// float angle = 20;
			while(state == State.Attack){
				while(target){
					yield return new WaitForSeconds(fireRate);
					// bm.Shoot(bullet, _transform, _transform.position, 7f, 3, 90f, 15f, 15f, 5f);
					// bm.ShootAim(bullet, _transform, _transform.position, target, 2f, 3, 30f, 5f, 5f);
					if(target){
						shooter.SetTarget(target);
						shooter.Shoot();	
					}
				}
				ChangeState(State.Idle);
			}
		}
		
		public override void Hit(float damage){
            base.Hit(damage);
			if(health.Hit(damage)){
                ChangeState(State.Dead);
            }
            // ChangeState(State.DoHit);
		}
		

		// IEnumerator DoHit(){
        //     ChangeText(state.ToString(), Color.grey);
            
        //     while (state == State.DoHit)
        //     {
        //         yield return new WaitForSeconds (.2f);
        //         ChangeState(State.Idle);
        //     }
        // }

		IEnumerator Dead(){
            ChangeText(state.ToString(), Color.black);
            while (state == State.Dead)
            {
                Destroy(this.gameObject);
                yield return null;
            }
        }

    }
}
