﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
    [System.Serializable]
    public class IHE_EnemyA : IHE_Enemy {
        [Header ("Prefabs")]
        public GameObject blood;
        public GameObject ragdoll;

        const float attackAnimDuration = 1f / 2.667f;

        public enum State {
            None,
            Idle,
            Wander,
            Chase,
            Attack,
            DashAttack,
            GetHit,
            Dead
        }

        [Header ("Properties")]
        public State state;
        public float wanderSpeed;
        public float chaseSpeed;
        public float visionRange = 10f;
        public float rangeToDash = 8f;
        public float rangeToChase = 5f;
        public float rangeToAttack = 1f;
        public float attackDuration = 1f;
        public float chargeTime = 1f;
        public float dashDuration = 1f;

        private float[] wanderPos;

        private Vector3 startPos;
        // private int currentPosID;
        WanderProperties wanderProperties; 

        private IHE_Sword sword;


        public override void Start () {
            base.Start ();
            wanderProperties = GetComponent<WanderProperties>();
            wanderPos = new float[] { _transform.position.x + wanderProperties.left, _transform.position.x + wanderProperties.right };
            sword = GetComponentInChildren<IHE_Sword> ();

            ChangeState (state);
            
        }

        private void ChangeState (State newState) {
            state = newState;
            if (currentCoroutine != null)
                StopCoroutine (currentCoroutine);
            currentCoroutine = StartCoroutine (newState.ToString ());
        }


        

        IEnumerator None () {
            _animator.Play ("none");
            yield return null;
        }

        IEnumerator Idle () {
            ChangeText (state.ToString (), Color.green);
            PlayAnim("idle");
            float idleTime = 1f;
            while (state == State.Idle) {
                idleTime -= Time.deltaTime;
                if (idleTime <= 0)
                    ChangeState (State.Wander);

                yield return null;
            }
        }

        IEnumerator Wander () {
            ChangeText (state.ToString (), Color.green);
            PlayAnim("walk");
            FaceTo (new Vector3 (wanderPos[wanderProperties.currentPosID], 0, 0));
            while (state == State.Wander) {
                _transform.Translate (Vector3.right * wanderSpeed * base.GetDir () * Time.deltaTime);
                if (Mathf.Abs (_transform.position.x - wanderPos[wanderProperties.currentPosID]) < .2f) {
                    wanderProperties.ChangePos();

                    ChangeState (State.Idle);
                }

                if (Vision ()) {
                    ChangeState (State.Chase);
                }

                yield return null;
            }

        }

        IEnumerator Chase () {
            ChangeText (state.ToString (), Color.yellow);
            PlayAnim("walk");
            float visionTresshold = 1f;
            while (state == State.Chase) {
                if(!target) ChangeState (State.Idle);

                // calculate target range
                float range = Mathf.Abs (_transform.position.x - target.position.x);
                if (range < visionRange + visionTresshold) {
                    FaceTo (target.position);
                    _transform.Translate (Vector3.right * chaseSpeed * base.GetDir () * Time.deltaTime);
                    if (range < rangeToDash && range > rangeToChase) {
                        ChangeState (State.DashAttack);
                    } else if (range < rangeToAttack) {
                        ChangeState (State.Attack);
                    }
                } else {
                    ChangeState (State.Idle);
                }
                yield return null;
            }
        }

        IEnumerator Attack () {
            ChangeText (state.ToString (), Color.red);
            yield return null;
            while (state == State.Attack) {
                if(!target) ChangeState (State.Idle);

                // attacking
                _animator.SetFloat("animSpeed", attackDuration / attackAnimDuration);
                PlayAnimForce("attack", 0);
                bool right = target.position.x > _transform.position.x;
                FaceSprite(right);
                sword.Activate(0, this.facingRight);
                yield return new WaitForSeconds (attackDuration + .5f);
                float range = Mathf.Abs (_transform.position.x - target.position.x);
                float visionTresshold = 1f;
                if (range > rangeToAttack + visionTresshold)
                    ChangeState (State.Chase);
            }
        }

        IEnumerator DashAttack () {
            ChangeText (state.ToString (), Color.red);

            while (state == State.DashAttack) {
                if(!target) ChangeState (State.Idle);

                Vector3 targetPos = target.position;
                // charge
                _animator.Play ("none");
                yield return new WaitForSeconds (chargeTime);
                _animator.SetFloat("animSpeed", dashDuration / attackAnimDuration);
                PlayAnimForce("attack", 0);
                sword.Activate(1, this.facingRight);
                // dash
                _transform.DOPath (new Vector3[] {
                        _transform.position,
                        new Vector3 (_transform.position.x + ((targetPos.x - _transform.position.x) * .5f), _transform.position.y + 2, 0),
                        new Vector3 (targetPos.x, _transform.position.y, 0)
                        }, dashDuration, PathType.CatmullRom);
                sword.Activate (.1f, dashDuration - .1f);
                yield return new WaitForSeconds (dashDuration);
                yield return new WaitForSeconds (.4f);
                ChangeState (State.Chase);
                // float range = Mathf.Abs (_transform.position.x - target.position.x);
                // float visionTresshold = 1f;
                // if (range > rangeToAttack + visionTresshold)
                //     ChangeState (State.Chase);
            }
        }

        public override void Hit (float damage) {
            base.Hit (damage);
            // if(state != State.Attack){
            //     StartCoroutine("PlayHitAnim");
            // }
            GameObject go = Instantiate (blood, _transform.position, Quaternion.Euler (-90, 0, 0));
            Destroy (go, 2f);
            if (health.Hit (damage)) {
                ChangeState (State.Dead);
            }
            // ChangeState (State.GetHit);
            // sword.Cancel ();
        }

        // IEnumerator PlayHitAnim(){
        //     string temp = curAnim;
        //     float current =_animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        //     PlayAnim("hit", false, 0);
        //     yield return new WaitForSeconds(.12f);
        //     PlayAnim(temp, false, current);

        // }

        // IEnumerator GetHit () {
        //     ChangeText (state.ToString (), Color.grey);
        //     GameObject go = Instantiate (blood, _transform.position, Quaternion.Euler (-90, 0, 0));
        //     Destroy (go, 2f);
        //     _animator.Play ("hit");
        //     int dir = _transform.position.x > target.position.x ? 1 : -1;
        //     _rigidbody.position = _rigidbody.position + new Vector2 (.2f * dir, 0);

        //     while (state == State.GetHit) {
        //         yield return new WaitForSeconds (.3f);
        //         ChangeState (State.Chase);
        //     }
        // }

        IEnumerator Dead () {
            ChangeText (state.ToString (), Color.black);
            while (state == State.Dead) {
                GameObject go = Instantiate (ragdoll, _transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<IHE_Ragdoll> ().Explode (_transform.position.x > target.position.x ? true : false);
                Destroy (this.gameObject);
                yield return null;
            }
        }

        private bool Vision () {
            RaycastHit2D hit = Physics2D.Raycast (_transform.position, Vector2.right * base.GetDir (), visionRange, 1 << LayerMask.NameToLayer ("Player"));
            this.target = hit.transform;
            return hit;
        }

        private void OnDrawGizmosSelected () {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine (transform.position, transform.position + (Vector3.right * visionRange * base.GetDir ()));
        }
    }
}