﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace SKY {
    [System.Serializable]
    public class IHE_GruntTank : IHE_Enemy {
        [Header ("Prefabs")]
        public GameObject blood;
        public GameObject ragdoll;

        public enum State {
            None,
            Idle,
            Wander,
            Chase,
            Attack,
            GetHit,
            Dead
        }

        [Header ("Properties")]
        public State state;
        public float wanderSpeed;
        public float chaseSpeed;
        public float visionRange = 10f;
        public float attackRange = 1f;
        public float attackDuration = 1f;
        public float chargeTime = 1f;
        public float dashDuration = 1f;

        private float[] wanderPos;

        private Vector3 startPos;
        private float currentSpeed;
        // private int currentPosID;
        WanderProperties wanderProperties;

        private IHE_Sword sword;

        public override void Start () {
            base.Start ();
            wanderProperties = GetComponent<WanderProperties> ();
            wanderPos = wanderProperties.GetWanderPos ();
            sword = GetComponentInChildren<IHE_Sword> ();

            ChangeState (state);

        }

        private void ChangeState (State newState) {
            state = newState;
            if (currentCoroutine != null)
                StopCoroutine (currentCoroutine);
            currentCoroutine = StartCoroutine (newState.ToString ());
        }

        IEnumerator None () {
            _animator.Play ("none");
            yield return null;
        }

        IEnumerator Idle () {
            ChangeText (state.ToString (), Color.green);
            PlayAnimCrossFade ("Enemies Tanker Idle");
            float idleTime = 1f;
            while (state == State.Idle) {
                idleTime -= Time.deltaTime;
                if (idleTime <= 0)
                    ChangeState (State.Wander);

                yield return null;
            }
        }

        public void PauseSpeed () {
            currentSpeed = wanderSpeed * .5f;
        }

        public void UnpauseSpeed () {
            currentSpeed = wanderSpeed;
        }

        IEnumerator Wander () {
            ChangeText (state.ToString (), Color.green);
            _animator.SetFloat("Animation Speed", 1f);
            PlayAnimCrossFade ("Enemies Tanker Walk");
            FaceTo (new Vector3 (wanderPos[wanderProperties.currentPosID], 0, 0));

            // sync walking anim
            float timeSlow = 65f / 60;
            float timeUnslow = 130f / 60;
            float animLength = 2.167f;
            float curAnimTime = 0;

            while (state == State.Wander) {
                _transform.Translate (Vector3.right * currentSpeed * base.GetDir () * Time.deltaTime);
                curAnimTime = (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1) * animLength;

                if (curAnimTime >= timeSlow && curAnimTime < timeUnslow)
                    {currentSpeed = wanderSpeed * .5f; print("slow");}
                else
                    currentSpeed = wanderSpeed;

                if (Mathf.Abs (_transform.position.x - wanderPos[wanderProperties.currentPosID]) < .2f) {
                    wanderProperties.ChangePos ();
                    ChangeState (State.Idle);
                }

                if (Vision ()) {
                    ChangeState (State.Chase);
                }

                yield return null;
            }

        }

        IEnumerator Chase () {
            ChangeText (state.ToString (), Color.yellow);
            _animator.SetFloat("Animation Speed", 2f);
            PlayAnimCrossFade ("Enemies Tanker Walk");
            FaceTo (new Vector3 (wanderPos[wanderProperties.currentPosID], 0, 0));
            
            // sync walking anim
            float timeSlow = 65f / 60;
            float timeUnslow = 130f / 60;
            float animLength = 2.167f;
            float curAnimTime = 0;


            float visionTresshold = .5f;
            while (state == State.Chase) {
                if(!target) ChangeState (State.Idle);

                // calculate target range
                FaceTo(target.position);
                float range = GetTargetRange ();
                if (range < visionRange + visionTresshold) {
                    _transform.Translate (Vector3.right * currentSpeed * base.GetDir () * Time.deltaTime);
                    curAnimTime = (_animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1) * animLength;
                    
                    if (curAnimTime >= timeSlow && curAnimTime < timeUnslow)
                        currentSpeed = chaseSpeed * .5f;
                    else
                        currentSpeed = chaseSpeed;

                    if(range < attackRange)
                        ChangeState (State.Attack);    
                } else {
                    ChangeState (State.Idle);
                }
                yield return null;
            }
        }

        IEnumerator Attack () {
            ChangeText (state.ToString (), Color.red);

            while (state == State.Attack) {
                if (!target) ChangeState (State.Idle);
                FaceTo(target.position);
                if(Chance(50f)){
                PlayAnimForce("Enemies Tanker Attack 1", 0);
                yield return new WaitForSeconds(2.5f);
                }else{
                    PlayAnimForce("Enemies Tanker Attack 2", 0);
                    yield return new WaitForSeconds(3f);
                }
                ChangeState (State.Chase);
            }
        }


        bool Chance (float percentage) {
            if (Random.Range (0, 101) < percentage)
                return true;
            else
                return false;
        }

        public override void Hit (float damage, Transform source) {
            base.Hit (damage, source);
            GameObject go = Instantiate (blood, _transform.position, Quaternion.Euler (-90, 0, 0));
            Destroy (go, 2f);
            
            if(state == State.Wander || state == State.Idle)
                ChangeState (State.Attack);

            if (health.Hit (damage)) {
                ChangeState (State.Dead);
            }
        }

        IEnumerator Dead () {
            ChangeText (state.ToString (), Color.black);
            while (state == State.Dead) {
                GameObject go = Instantiate (ragdoll, _transform.position, Quaternion.identity) as GameObject;
                go.GetComponent<IHE_RagdollAuto> ().Explode (_transform.position.x > target.position.x ? true : false);
                Destroy (this.gameObject);
                yield return null;
            }
        }

        private bool Vision () {
            RaycastHit2D hit = Physics2D.Raycast (_transform.position, Vector2.right * base.GetDir (), visionRange, 1 << LayerMask.NameToLayer ("Player"));
            this.target = hit.transform;
            return hit;
        }

        private void OnDrawGizmosSelected () {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine (transform.position, transform.position + (Vector3.right * visionRange * base.GetDir ()));
        }

        private float GetTargetRange () {
            if (!target)
                return 0;

            return Mathf.Abs (_transform.position.x - target.position.x);
        }
    }
}