﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IHE_RagdollAuto : MonoBehaviour {

	private Rigidbody2D[] _rigidbodies;
	private SpriteRenderer[] _sprites;

	void Awake(){
		_sprites = GetComponentsInChildren<SpriteRenderer>();
		foreach(SpriteRenderer t in GetComponentsInChildren<SpriteRenderer>()){
			t.gameObject.AddComponent<BoxCollider2D>();
			t.gameObject.AddComponent<Rigidbody2D>();
		}
		_rigidbodies = GetComponentsInChildren<Rigidbody2D>();
	}

	// void OnEnable(){
	// 	Explode(true);
	// }

	public void Explode(bool right){
		Invoke("DestroySelf", 3f);
		int dir = right ? 1 : -1;
		for (int i = 0; i < _rigidbodies.Length; i++)
		{
			_rigidbodies[i].gravityScale = 2f;
			_rigidbodies[i].AddForce(new Vector2(Random.Range(200, 300) * dir, Random.Range(350, 500)));
			_rigidbodies[i].AddTorque(Random.Range(-50, 50));
		}
		StartCoroutine("Tint");
	}

	private IEnumerator Tint(){
            float amount = 1f;
            while(amount > 0){
                for (int i = 0; i < _sprites.Length; i++)
                {
                    _sprites[i].material.SetColor("_FlashColor", Color.white) ;
                    _sprites[i].material.SetFloat("_FlashAmount", amount) ;
                }
                amount -= Time.deltaTime;
                yield return null;
            }
        }

	void DestroySelf(){
		Destroy(this.gameObject);
	}
}
