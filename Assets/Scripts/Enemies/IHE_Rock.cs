﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SKY {
    public class IHE_Rock : Bullet {

        public bool destroyOnGround = false;
        Rigidbody2D rb;

        Collider2D[] colliders;
        CameraFollow cameraFollow;

        public override void Awake () {
            base.Awake ();
            rb = GetComponent<Rigidbody2D> ();
            colliders = GetComponentsInChildren<Collider2D>();
            cameraFollow = FindObjectOfType<CameraFollow>();
        }

        public override void OnEnable () {
            base.OnEnable();
            colliders[0].enabled = true;
            rb.isKinematic = false;
            colliders[1].enabled = false;
            // Invoke("EnablePlatform", 1.5f);
        }

        public override void OnDisable () {
            // for gravitation
            for (int i = 0; i < currentTween.Count; i++) {
                currentTween[i].Kill ();
            }

            _transform.rotation = Quaternion.identity;
            for (int i = 0; i < currentTween.Count; i++) {
                currentTween[i].Kill ();
            }
            currentTween.Clear ();
            StopAllCoroutines ();
            CancelInvoke ("Recycle");

        }

        // void EnablePlatform(){
        //     colliders[1].enabled = true;
        // }

        public override void Gravity (float force, float angle, float gravityScale) {
            StopCoroutine ("BulletMovement");
            rb.gravityScale = gravityScale;
            Vector2 dir = (Vector2) (Quaternion.Euler (0, 0, angle + 90) * Vector2.right);
            rb.AddForce (dir * force);
        }

        public override void Recycle(){
            if (gameObject.activeSelf)
                gameObject.Recycle ();
        }

        public override void OnTriggerEnter2D(Collider2D other) {
            
            base.OnTriggerEnter2D(other);

            if (other.gameObject.layer == LayerMask.NameToLayer ("Ground")) {
                
                if(destroyOnGround) Recycle();
                else { 
                    
                    this.gameObject.layer = LayerMask.NameToLayer("HangPoint"); 
                    this.bulletType =  BulletType.EmptyBullet;    
                }

                //colliders[0].enabled = false;
                //colliders[1].enabled = true;
                rb.velocity = Vector2.zero;
                rb.isKinematic = true;
                rb.angularVelocity = 0;
            }
            cameraFollow.Vibrate(.5f, .1f, 2f, .2f);
        }

        // private void OnCollisionEnter2D (Collision2D other) {
        //     if (other.gameObject.layer == LayerMask.NameToLayer ("Ground")) {
        //         if(destroyOnGround)
        //             Recycle();

        //         colliders[0].enabled = false;
        //         colliders[1].enabled = true;
        //         rb.velocity = Vector2.zero;
        //         rb.isKinematic = true;
        //         rb.angularVelocity = 0;
        //     }

        // }

        public void RecycleLater(float time){
            Invoke("Recycle", time);
        }
    }
}