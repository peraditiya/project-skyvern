﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Manager : MonoBehaviour {

	public static Manager instance = null;
	public static InputManager inputManager = null;

	private void Awake () {
		if (instance == null) { 
			instance = this; 
		} else if (instance != this) { 
			Destroy (gameObject); 
		}

		DontDestroyOnLoad (gameObject);

		InitManagers();
	}

	private void InitManagers(){
		GameObject inputManagerGO = GameObject.Find("Input Manager");
		if(inputManagerGO == null){
			inputManagerGO = Instantiate(Resources.Load("prefab/Managers/Input Manager"), Vector3.zero, Quaternion.identity) as GameObject;
			inputManagerGO.transform.SetParent(this.transform);
		}
		inputManager = inputManagerGO.GetComponent<InputManager>();
			
		
	}
}