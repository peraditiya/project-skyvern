﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    public class Grappler : MonoBehaviour
    {

        public Body body;

        public float height = 0.16f;

        public float throwSpeed = 0.31f;
        public float throwDistance = 6f;

        public float retractSpeed = 0.103f;
        public float retractSpeedDiagonal = 0.073f;

        public float swingSpeed = 1.5f;
        [HideInInspector]
        public float swingLength;

        [HideInInspector]
        public Vector3 hookedPosition;

        private Vector2 size;
        private bool isResizeCollider;
        private bool isPullingPlayer;

        [HideInInspector]
        public Vector2 grapplingDirection = Vector2.zero;
        private IEnumerator grapplingRoutine;

        int scaler = 0;

        void Awake()
        {

            this.body = new Body(this.gameObject);

            Physics2D.IgnoreCollision(Root.player.unit.collider, this.body.collider, true);

            Root.player.grappler = this;
        }

        void Start()
        {

            this.body.pivot = new Vector2(0, 1);
            this.body.collider.size = this.body.size;
            this.body.collider.offset = new Vector2((0 - (this.height / 2)), (0 - (this.height / 2)));
            this.size = new Vector2(this.height, this.height);

            this.Reset();
        }

        void FixedUpdate()
        {

            if (this.size != Vector2.zero)
            {
                this.body.size = this.size;
                this.body.renderer.size = this.body.size;

                if (this.isResizeCollider == true)
                {

                    this.body.collider.offset = new Vector2(this.body.size.x / 2, 0 - this.body.size.y / 2);
                    this.body.collider.size = this.body.size;

                }
                else
                {

                    this.body.collider.size = new Vector2(0.16f, 0.16f);
                    this.body.collider.offset = new Vector2((this.body.right - this.body.left) - (this.height / 2), this.body.collider.offset.y);
                }

                if (this.isPullingPlayer == true) Root.player.unit.position = (Vector3)this.body.collider.bounds.center;

                this.Reset();
            }

            if (Root.player.unit.state.isBind) this.body.position = Root.player.unit.position;
        }

        void OnTriggerEnter2D(Collider2D collider)
        {

            if (collider.tag == "Hold" && (Root.player.currentHold == null || Root.player.currentHold.name != collider.name))
            {

                RaycastHit2D hookedObject = this.GetHookPoint();
                if(hookedObject.collider != null) {

                    this.hookedPosition = (Vector3)hookedObject.point;
                    Root.player.currentHold = Stage.currentRoom.GetHold(hookedObject.collider.name);
                }
            }
            else if (collider.tag == "Hostile")
            {
                Root.player.currentCatch = Stage.hostiles[collider.name];
            }
            else
                this.scaler = -1;
        }

        void OnTriggerStay2D(Collider2D collider) {

            if (collider.tag == "Hold" && (Root.player.currentHold == null || Root.player.currentHold.name != collider.name))
            {

                RaycastHit2D hookedObject = this.GetHookPoint();
                if(hookedObject.collider != null) {

                    this.hookedPosition = (Vector3)hookedObject.point;
                    Root.player.currentHold = Stage.currentRoom.GetHold(hookedObject.collider.name);
                }
            }
        }

        private RaycastHit2D GetHookPoint()
        {

            RaycastHit2D ret = new RaycastHit2D();

            RaycastHit2D[] hookedObject = Physics2D.RaycastAll(this.transform.position, this.grapplingDirection, this.body.width + 0.5f);

            if (hookedObject.Length > 0)
            {

                foreach (RaycastHit2D o in hookedObject)
                {

                    if (o.collider.tag == "Hold") ret = o;
                }
            }

            return ret;
        }

        public void Grappling(Vector3 direction)
        {

            this.grapplingDirection = direction;

            string grapplingState = Character.GRAPPLING_FORWARD_ID;

            if (this.grapplingDirection.x == 1 && this.grapplingDirection.y == 1)
            {

                grapplingState = Character.GRAPPLING_UP_FORWARD_ID;
                this.body.zAngle = 45;
            }
            else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 1)
            {

                grapplingState = Character.GRAPPLING_UP_FORWARD_ID;
                this.body.angles = new Vector3(0, 180, 45);
            }
            else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 0)
            {

                grapplingState = Character.GRAPPLING_FORWARD_ID;
                this.body.angles = new Vector3(0, 180, 0);
            }
            else if (this.grapplingDirection.x == 0 && this.grapplingDirection.y == 1)
            {

                grapplingState = Character.GRAPPLING_UP_ID;
                this.body.angles = new Vector3(0, 0, 90);
            }

            this.Enable();

            Root.player.spriteAnimator.currentAnimation = grapplingState;

            this.grapplingRoutine = this.GrapplingRoutine();
            this.StartCoroutine(this.grapplingRoutine);
        }

        IEnumerator GrapplingRoutine()
        {

            this.scaler = 1;

            while (Root.player.unit.state.isGrappling == true)
            {

                if (this.body.width > 0)
                {

                    if (Root.player.unit.state.isJump == true) this.body.y = Root.player.unit.y;
                    if (this.scaler == 1) this.scaler = this.body.width >= this.throwDistance ? -1 : 1;
                    if (Root.player.currentHold != null) this.scaler = 1;

                    if (Root.player.unit.state.isJump) this.body.position = Root.player.unit.position;

                    this.Resize(new Vector2(this.body.width + (this.throwSpeed * this.scaler), this.height), true, false);

                    if (Root.player.currentHold != null) Root.player.Retract();
                    else if (Root.player.currentCatch != null) Root.player.Catch();
                }
                else
                    this.GrapplingStop();

                yield return new WaitForFixedUpdate();
            }
        }

        public void GrapplingStop()
        {

            Root.player.unit.state.isGrappling = false;
            Root.player.unit.state.isFreeze = false;
            this.scaler = 0;
            this.Disable();
        }

        public void Hook()
        {
            Body currentHold = Root.player.currentHold.body;

            this.body.size = new Vector2(Vector3.Distance(Root.player.unit.position, this.hookedPosition), 0.16f);
            this.body.renderer.size = this.body.size;

            this.body.collider.size = new Vector2(0.16f, 0.16f);
            this.body.collider.offset = new Vector2((this.body.right - this.body.left) - (this.height / 2), this.body.collider.offset.y);

            this.Reset();

            this.body.position = this.hookedPosition;

            if (this.grapplingDirection.x == 1 && this.grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(180, 180, this.transform.eulerAngles.z);
            else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(0, 0, 315);
            else if (this.grapplingDirection.x == 1 && this.grapplingDirection.y == 0) this.transform.eulerAngles = new Vector3(0, 0, 180);
            else if (this.grapplingDirection.x == -1 && this.grapplingDirection.y == 0) this.transform.eulerAngles = new Vector3(0, 0, 0);
            else if (this.grapplingDirection.x == 0 && this.grapplingDirection.y == 1) this.transform.eulerAngles = new Vector3(180, 0, 90);
        }

        public void Bind()
        {
            Root.player.currentCatch.unit.state.isBound = true;

            this.Reset();
            this.body.collider.size = new Vector2(0.16f, 0.16f);
            this.body.collider.offset = new Vector2((this.body.right - this.body.left) - (this.height / 2), this.body.collider.offset.y);
        }

        public void Resize(Vector2 size, bool isResizeCollider = false, bool isPullingPlayer = true)
        {

            this.size = size;
            this.isResizeCollider = isResizeCollider;
            this.isPullingPlayer = isPullingPlayer;
        }

        public void Enable()
        {

            this.body.position = Root.player.unit.position;
            this.enabled = true;
        }

        public void Disable()
        {

            this.body.position = new Vector3(1000, 1000, 0);
            this.size = new Vector2(0.16f, 0.16f);
            this.body.angles = Vector3.zero;
            //this.enabled = false;
        }

        public void Reset()
        {

            this.size = Vector2.zero;
            this.isResizeCollider = false;
            this.isPullingPlayer = false;
        }
    }
}