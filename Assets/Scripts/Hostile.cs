﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class Hostile : Character {

        protected TimeUtil aiRefreshDelay;
        protected Body trigger;

        private Vector3 _target;

        private IEnumerator hitRoutine;

        public int knockedJumpIndex = 5;

        // Use this for initialization
        public override void Awake() {

            base.Awake();

            this.aiRefreshDelay = new TimeUtil();
            this.target = Vector3.zero;
        }

        public override void Start() {

            Physics2D.IgnoreCollision(Root.player.unit.collider, this.unit.collider, true);
            Stage.hostiles.Add(this.unit.name, this);
        }

        public override void Update() {
            
            /* 
            this.movement = Vector3.zero;

            if (this.target.x != (float)Math.Round(this.unit.x, 1)) this.movement.x = (this.unit.x > this.target.x) ? -0.02f : 0.02f;
            if (this.target.y != (float)Math.Round(this.unit.y, 1)) this.movement.y = (this.unit.y > this.target.y) ? -0.02f : 0.02f;
            if (this.target.z != (float)Math.Round(this.unit.z, 1)) this.movement.z = (this.unit.z > this.target.z) ? -0.02f : 0.02f;

            if (this.movement == Vector3.zero) this.unit.state.isOnTask = false;
            */
        }

        public override void FixedUpdate() {
            
            if(this.unit.state.isBound) this.unit.x = Root.player.grappler.body.collider.bounds.center.x;
            else if(this.unit.state.isKnocked) this.movement.x = Root.player.unit.x > this.unit.x ? -1 : 1;

            base.FixedUpdate();
            //this.unit.movement = this.movement;
        }

        protected Vector3 target {

            get {
                return this._target;
            }

            set {
                this._target = new Vector3((float)Math.Round(value.x, 1), (float)Math.Round(value.y, 1), (float)Math.Round(value.z, 1));
            }
        }

        public void OnCollisionEnter2D (Collision2D collision) {

            if(collision.collider.tag == "Platform" || collision.collider.tag == "Wall") {

                this.unit.rigidBody.velocity = Vector2.zero;
                this.unit.rigidBody.gravityScale = 0;

                if(this.unit.state.isKnocked) this.unit.state.isKnocked = false; 
            }
        }

        public void Hit() {

            if(this.unit.state.isHit == false) {

                this.unit.state.isHit = true; 
                
                this.hitRoutine = this.HitRoutine();
                this.StartCoroutine(this.hitRoutine);
            }
        }

        private IEnumerator HitRoutine()
        {
            int scaler = 1;
            float modifier = 0.1f;

            Color currentColor;
                
            while(this.unit.state.isHit) {
                
                currentColor = this.body.renderer.color;
                this.body.renderer.color = new Color(currentColor.r, currentColor.g + (modifier * scaler), currentColor.b + (modifier * scaler));
                
                if(this.body.renderer.color.g >= 0.7f && scaler == 1) scaler = -1;
                else if(this.body.renderer.color.g <= 0.00f) this.HitStop();

                yield return new WaitForFixedUpdate();
                //yield return new WaitForSeconds(2f);
            }
        }
        
        private void HitStop() {

            this.unit.state.isHit = false;    
        }

        public void Knocked() {

            if(this.unit.state.isKnocked == false) {

                this.unit.state.isKnocked = true; 
                
                this.verticalIndex = this.knockedJumpIndex;

                base.Jump();

                //this.unit.rigidBody.gravityScale = this.gravityScale;
            }
        }
    }
}
