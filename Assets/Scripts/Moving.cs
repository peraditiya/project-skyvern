﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Moving : MonoBehaviour {

		[HideInInspector]
		public Body body;
		
		public float minSpeed = 0.1f;
		public float maxSpeed = 0.1f;

		public Vector3 direction = Vector3.left;

		private List<MovingObject> _childs;

		// Use this for initialization
		
		void Awake () {

			//this.body = new Body(this.gameObject);
		}

		void Start () {

			this.body = Stage.currentRoom.body;
			
			this._childs = new List<MovingObject>();

			foreach(Transform child in this.transform) {

				float speed = (minSpeed == maxSpeed) ? minSpeed : Random.Range(minSpeed, maxSpeed);
				this._childs.Add(new MovingObject(child.gameObject, speed));
			}
		}

		void FixedUpdate () {
			
			foreach(MovingObject child in this._childs) {

				if(this.direction.x < 0 && (child.right + this.body.x) < this.body.left) child.left = this.body.right - this.body.x;
				else if(this.direction.x > 0 && (child.left - this.body.x) > this.body.right) child.right = this.body.left + this.body.x;

				if(this.direction.y < 0 && (child.top + this.body.y) < this.body.bottom) child.bottom = this.body.top - this.body.y;
				else if(this.direction.y > 0 && (child.bottom - this.body.y) > this.body.top) child.top = this.body.bottom + this.body.y;

				child.position += this.direction * child.speed;
			}
		}
	}
}