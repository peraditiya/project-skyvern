﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent(typeof(Rigidbody2D))]

    public class Player : Character
    {

        private int combo = 0;
        private bool isComboTriggered = false;

        private UnitState buttonPressed;
        private Vector3 newMovement;

        private bool _jumpExec = false;
        private bool _grappleExec = false;
        private bool _attackExec = false;
        private bool _attackBlacklist = false;
        private float swingMovement;

        [HideInInspector]
        public Grappler grappler;
        //public Grappler grappler;

        private Gate currentGate;
        private Wall _currentPlatform;

        [HideInInspector]
        public Hold currentHold;

        [HideInInspector]
        public Hostile currentCatch;

        private IEnumerator jumpRoutine;
        private IEnumerator grapplingRoutine;
        private IEnumerator attackRoutine;
        private IEnumerator retractRoutine;
        private IEnumerator swingRoutine;
        private IEnumerator pullRoutine;

        public float swingJumpHeight = 4f;
        private int swingJumpIndex = 1;

        private Wall currentPlatform
        {

            get { return this._currentPlatform; }
            set
            {

                if (this._currentPlatform != null && ((value != null && value.name != this._currentPlatform.name) || value == null)) this._currentPlatform.collider.enabled = true;

                this._currentPlatform = value;
            }
        }

        public void Awake()
        {

            base.Awake();

            Root.player = this;

            this.buttonPressed = new UnitState();

            this.swingJumpIndex = (int)(this.jumpIndex * (this.swingJumpHeight / this.jumpHeight));

            this.attack.CreateSet(Character.ATTACK_01_ID, new Vector3(2, 0, 0), new Vector2(2.5f, 2.4f), 2, 5);
            this.attack.CreateSet(Character.ATTACK_02_ID, new Vector3(2, 0, 0), new Vector2(2.5f, 2.4f), 2, 5);
            this.attack.CreateSet(Character.ATTACK_03_ID, new Vector3(2, 0, 0), new Vector2(2.5f, 2.4f), 2, 5);
            this.attack.CreateSet(Character.ATTACK_FINISHER_ID, new Vector3(2, 0, 0), new Vector2(5f, 5f), 2, 6);
            this.attack.CreateSet(Character.JUMP_ATTACK, new Vector3(2, 0, 0), new Vector2(2.5f, 2.4f), 2, 6);
        }

        public override void Start()
        {

            base.Start();

            this.spriteAnimator = new SpriteAnimator(this.body, "Textures/Zero");

            SpriteAnimation tAnim = this.spriteAnimator.CreateAnimation(Character.IDLE_ID);
            tAnim.AddFrame(0).AddFrame(1).AddFrame(2).AddFrame(3).AddFrame(4).AddFrame(5).loop = true;
            tAnim.delay = 0.15f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.WALK_ID);
            tAnim.AddFrame(6).AddFrame(7).AddFrame(8).AddFrame(9).AddFrame(10).AddFrame(11).AddFrame(12).AddFrame(13).AddFrame(14).AddFrame(15).AddFrame(16).loop = true;
            tAnim.delay = 0.06f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_UP_ID);
            tAnim.AddFrame(17).AddFrame(18).AddFrame(19).AddFrame(20).AddFrame(21).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_DOWN_ID);
            tAnim.AddFrame(22).AddFrame(23).AddFrame(24).AddFrame(25).AddFrame(26).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_FORWARD_ID);
            tAnim.AddFrame(29).AddFrame(30).AddFrame(31).AddFrame(32).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_UP_FORWARD_ID);
            tAnim.AddFrame(33).AddFrame(34).AddFrame(35).AddFrame(36).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.GRAPPLING_UP_ID);
            tAnim.AddFrame(37).AddFrame(38).AddFrame(39).AddFrame(40).delay = 0.1f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.ATTACK_01_ID);
            tAnim.AddFrame(66).AddFrame(67).AddFrame(68).AddFrame(69).AddFrame(70).AddFrame(72).delay = 0.04f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.ATTACK_02_ID);
            tAnim.AddFrame(75).AddFrame(76).AddFrame(77).AddFrame(78).AddFrame(79).AddFrame(83).delay = 0.04f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.ATTACK_03_ID);
            tAnim.AddFrame(84).AddFrame(87).AddFrame(88).AddFrame(89).AddFrame(90).AddFrame(91).delay = 0.04f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.ATTACK_FINISHER_ID);
            tAnim.AddFrame(95).AddFrame(96).AddFrame(97).AddFrame(98).AddFrame(99).AddFrame(100).AddFrame(101).AddFrame(102).AddFrame(103).AddFrame(104).delay = 0.04f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_ATTACK);
            tAnim.AddFrame(71).AddFrame(111).AddFrame(112).AddFrame(113).AddFrame(114).AddFrame(115).AddFrame(116).AddFrame(117).delay = 0.06f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_FORWARD_ID);
            tAnim.AddFrame(54).AddFrame(55).AddFrame(56).AddFrame(57).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_UP_FORWARD_ID);
            tAnim.AddFrame(58).AddFrame(59).AddFrame(60).AddFrame(61).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.JUMP_GRAPPLING_UP_ID);
            tAnim.AddFrame(62).AddFrame(63).AddFrame(64).AddFrame(65).delay = 0.05f;

            tAnim = this.spriteAnimator.CreateAnimation(Character.WITHDRAW);
            tAnim.AddFrame(71).delay = 0.05f;

            this.spriteAnimator.currentAnimation = Character.IDLE_ID;

            this.unit.state.isFalling = true;
            this.Jump();
        }

        // Update is called once per frame
        public override void Update()
        {

            this.newMovement = Vector3.zero;

            if (Input.GetKey(KeyCode.D)) this.newMovement.x = 1;
            else if (Input.GetKey(KeyCode.A)) this.newMovement.x = -1;

            if (Input.GetKey(KeyCode.W)) this.newMovement.y = 1f;
            else if (Input.GetKey(KeyCode.S)) this.newMovement.y = -1f;

            if (Input.GetKey(KeyCode.DownArrow) && this._jumpExec == false) this._jumpExec = this.buttonPressed.isJump = true;
            else if (Input.GetKeyUp(KeyCode.DownArrow)) this._jumpExec = this.buttonPressed.isJump = false;

            if (Input.GetKey(KeyCode.RightArrow) && this._grappleExec == false) this._grappleExec = this.buttonPressed.isGrappling = true;
            else if (Input.GetKeyUp(KeyCode.RightArrow)) this._grappleExec = this.buttonPressed.isGrappling = false;

            if (this._attackBlacklist == false)
            {
                if (Input.GetKey(KeyCode.LeftArrow) && this._attackExec == false) this._attackExec = this.buttonPressed.isAttacking = true;
                else if (Input.GetKeyUp(KeyCode.LeftArrow)) this._attackExec = this.buttonPressed.isAttacking = false;
            }

            if (this.newMovement.x != 0) this.body.renderer.flipX = this.newMovement.x < 0;

            if (!this.isComboTriggered) this.isComboTriggered = this.unit.state.isAttacking && this.buttonPressed.isAttacking;

            if (this.isComboTriggered && this.unit.state.isAttacking == false) this.Attack();
            else if (this.unit.state.isHanging == true) this.Hanging();
            else if (this.unit.state.isBind == true) this.Catch();
            else if (this.unit.state.isSwing == true) this.Swing();
            else if (this.unit.state.isRetract == true) this.Retract();
            else if (this.unit.state.isAirStop == true) this.JumpPause();
            else if (this.buttonPressed.isGrappling == true) this.Grappling();
            else if (this.buttonPressed.isAttacking == true) this.Attack();
            else if (this.buttonPressed.isJump == true) this.Jump();
            else if (this.buttonPressed.isFalling == true) this.Jump();
            else if (this.unit.state.isIdle == true)
            {

                if (this.movement.x != 0)
                {

                    this.spriteAnimator.currentAnimation = Character.WALK_ID;

                    /* 
                    Fixing if player only tipping(landed on very edge) of the platform 
                    collisonEnter and collisionExit isn't registered
                    this will make player state is not falling even the player fall from platform 
                    */
                    if (this._currentPlatform != null && Util.HitScan(this.unit, Vector2.down).collider == null)
                    {

                        this.unit.state.isFalling = true;
                        this.Jump();
                    }
                    /*
                    end fix
                    */
                }
                else { this.spriteAnimator.currentAnimation = Character.IDLE_ID; }
            }

            this.movement.x = this.newMovement.x;

            if (this.unit.state.isFreeze == true && this.unit.state.isJump == false) this.movement.x = 0;

            this.buttonPressed.Reset();
            this.animationState = this.spriteAnimator.Play();

            //Debug.Log(Time.deltaTime);
        }

        public override void FixedUpdate()
        {

            if (this.swingMovement != 0f && this.unit.state.isSwing == true)
            {

                this.grappler.body.zAngle += this.swingMovement;

                this.unit.position = (Vector3)this.grappler.body.collider.bounds.center;

            }
            else
                this.unit.movement = Vector3.Scale(this.movement, this.speed) * Time.deltaTime;

            //if (this.unit.state.isFreeze != true) this.unit.rigidBody.velocity = new Vector2(this.movement.x * this.speed.x * Time.deltaTime, this.unit.rigidBody.velocity.y);
        }

        protected override void Idle()
        {

            this.spriteAnimator.currentAnimation = Character.IDLE_ID;
            this.unit.state.Reset();
        }

        protected override void Attack()
        {

            if (this.unit.state.isAttacking == false)
            {
                this.unit.state.isAttacking = true;
                this.unit.state.isFreeze = true;

                if (this.isComboTriggered && this.unit.state.isHitEnemy && this.combo < 3)
                {

                    this.combo += 1;
                }
                else
                    this.combo = 0;

                this.isComboTriggered = false;
                this.unit.state.isHitEnemy = false;

                this.attackRoutine = this.AttackRoutine();
                this.StartCoroutine(this.attackRoutine);
            }
        }

        private IEnumerator AttackRoutine()
        {
            string attackAnimation = Character.ATTACK_01_ID;

            if (this.combo == 1) attackAnimation = Character.ATTACK_02_ID;
            else if (this.combo == 2) attackAnimation = Character.ATTACK_03_ID;
            else if (this.combo == 3) attackAnimation = Character.ATTACK_FINISHER_ID;

            this.spriteAnimator.currentAnimation = attackAnimation;

            this.animationState = SpriteAnimator.ANIMATION_IS_DELAYED;

            while (this.unit.state.isAttacking == true)
            {
                if (this.spriteAnimator.currentIndex >= 2 && (this.spriteAnimator.currentIndex <= 5 || (this.unit.state.isJump && this.spriteAnimator.currentIndex <= 6)))
                {
                    this.attack.Play(attackAnimation);
                    if (this.unit.state.isJump == true && this.unit.state.isHitEnemy) this.JumpPause();
                }
                else
                    this.attack.Reset();

                if (this.animationState == SpriteAnimator.ANIMATION_IS_DONE && !this.unit.state.isJump) this.AttackStop();
                
                yield return new WaitForFixedUpdate();
            }
        }

        private void AttackStop()
        {

            this.unit.state.isAttacking = false;

            this.attack.Reset();

            if(this.unit.state.isAirStop) {

                if(!this.isComboTriggered || this.combo == 3) this.JumpResume();
            }
            else { this.Idle(); }  
        }

        protected override void Jump()
        {

            if (this.unit.state.isJump == false)
            {

                this.unit.rigidBody.gravityScale = 0f;

                if (this.unit.state.isFalling == false && this.currentPlatform != null && this.currentPlatform.isPlatform == true
                    && this.buttonPressed.isJump == true && this.newMovement.y < 0)
                {

                    this.unit.state.isJump = true;
                    this.unit.state.isFalling = true;
                    this.unit.state.isJumpDown = true;
                    this.currentPlatform.collider.enabled = false;
                }
                else
                {

                    this.unit.state.isJump = true;
                    if (this.verticalIndex <= -100) this.verticalIndex = this.jumpIndex;
                    this.spriteAnimator.currentAnimation = Character.JUMP_UP_ID;
                }

                if (this.unit.state.isFalling == true)
                {

                    this.verticalIndex = 0;
                }

                this.jumpRoutine = this.JumpRoutine();
                this.StartCoroutine(this.jumpRoutine);
            }
        }

        protected override IEnumerator JumpRoutine()
        {

            string animationId = Character.JUMP_UP_ID;

            while (this.unit.state.isJump == true)
            {

                this.movement.y = (this.jumpSpeed * this.verticalIndex);

                if (this.verticalIndex <= 0)
                {

                    if (!this.unit.state.isAttacking && !this.unit.state.isGrappling && this.spriteAnimator.currentAnimation != Character.JUMP_DOWN_ID) this.spriteAnimator.currentAnimation = Character.JUMP_DOWN_ID;

                    this.unit.state.isFalling = true;

                    float scanDistance = Mathf.Abs(this.movement.y * this.speed.y * Time.deltaTime + 0.05f);
                    RaycastHit2D hit = Util.HitScan(this.unit, Vector2.down, scanDistance);

                    if (hit.collider != null && hit.collider.tag == "Wall")
                    {
                        Collider2D leftHit = Util.HitScan(this.unit, Vector2.left, 0.01f, 0.1f).collider;
                        Collider2D rightHit = Util.HitScan(this.unit, Vector2.right, 0.01f, 0.1f).collider;
                        Collider2D upHit = null;

                        bool isLandedValid = false;

                        RaycastHit2D[] upHitInfo = Physics2D.RaycastAll(new Vector2(hit.point.x, this.unit.bottom + 0.5f), Vector2.up, 0.1f);

                        if (upHitInfo.Length > 0)
                        {

                            foreach (RaycastHit2D a in upHitInfo) if (a.collider.tag == "Wall") upHit = a.collider;
                        }

                        if (upHit != null)
                            isLandedValid = false;
                        else if (leftHit != null || rightHit != null)
                        {
                            isLandedValid = Physics2D.Raycast(new Vector2(leftHit != null ? this.unit.right - 0.1f : this.unit.left + 0.1f, this.unit.bottom - 0.01f), Vector2.down, 0.01f).collider != null;
                        }
                        else
                            isLandedValid = true;

                        if (isLandedValid == true)
                        {

                            this.currentPlatform = Stage.currentRoom.GetWall(hit.collider.name);
                            this.JumpStop(0 - hit.distance);
                        }
                    }
                }
                else if (this._jumpExec == false) this.verticalIndex = 0;

                this.verticalIndex--;

                yield return new WaitForFixedUpdate();
            }
        }

        protected void JumpPause()
        {
            if (this.unit.state.isAirStop == false)
            {

                this.unit.state.isAirStop = true;
                this.unit.state.isJump = false;
                this.movement.y = 0;
                this.StopCoroutine(this.jumpRoutine);

            }
            else
            {
                if (this.buttonPressed.isAttacking) this.Attack();
                //else if (this.isComboTriggered == false) this.JumpResume();
            }
        }

        protected void JumpResume()
        {
            
            if (this.unit.state.isAirStop == true)
            {

                this.unit.state.isAirStop = false;
                this.unit.state.isJump = true;
                this._attackBlacklist = true;
                this.StartCoroutine(this.jumpRoutine);
            }
        }

        protected override void JumpStop(float jumpRemain = 0)
        {
            if (this.unit.state.isAttacking) this.AttackStop();

            this.unit.state.isJump = false;
            this.unit.state.isFalling = false;
            this.unit.state.isJumpDown = false;
            this.unit.state.isFreeze = false;
            this._attackBlacklist = false;
            this.unit.rigidBody.gravityScale = this.gravityScale;

            if (this.unit.state.isGrappling) this.grappler.GrapplingStop();

            if (jumpRemain < 0) this.unit.movement = new Vector3(0, jumpRemain, 0);
            this.movement.y = 0;

            this.verticalIndex = -100;

            this.spriteAnimator.currentAnimation = Character.IDLE_ID;
        }

        private void Grappling()
        {

            if (this.unit.state.isGrappling == false)
            {

                this.unit.state.isGrappling = true;
                this.unit.state.isFreeze = true;
                this.currentHold = null;

                this.grappler.Grappling(this.newMovement != Vector3.zero ? (Vector2)this.newMovement : (this.body.renderer.flipX ? Vector2.left : Vector2.right));
            }
        }

        public void Retract(bool isFullRetract = false)
        {

            if (this.unit.state.isRetract == false)
            {

                this.currentPlatform = null;
                this.unit.state.isGrappling = false;
                this.unit.state.isRetract = true;
                this.swingMovement = 0f;

                if (this.unit.state.isSwing == false) this.grappler.Hook();

                this.grappler.swingLength = isFullRetract || this.grappler.body.width <= 2f ? 0f : (this.unit.state.isJump ? this.grappler.body.width : 0.7f * this.grappler.body.width);

                if (this.unit.state.isJump || this.unit.state.isFalling) this.JumpStop();

                this.unit.state.isSwing = false;

                this.unit.rigidBody.gravityScale = 0;

                this.spriteAnimator.currentAnimation = Character.JUMP_UP_ID;

                this.retractRoutine = this.RetractRoutine();
                this.StartCoroutine(this.retractRoutine);
            }
        }

        IEnumerator RetractRoutine()
        {

            while (this.unit.state.isRetract == true)
            {

                if (this.grappler.body.width > this.grappler.swingLength)
                {

                    this.grappler.Resize(new Vector2(this.grappler.body.width - this.grappler.retractSpeed, this.grappler.height), false, true);

                }
                else if (this.grappler.swingLength > 0)
                {

                    this.Swing();

                }
                else
                    this.Hanging();

                yield return new WaitForFixedUpdate();
            }
        }

        public void Swing()
        {

            if (this.unit.state.isSwing == false)
            {

                this.unit.state.isSwing = true;
                this.unit.state.isRetract = false;
                this.StopCoroutine(this.retractRoutine);

                this.spriteAnimator.currentAnimation = Character.JUMP_UP_ID;

                this.swingRoutine = this.SwingRoutine();
                this.StartCoroutine(this.swingRoutine);

            }
            else
            {
                if (this.buttonPressed.isJump)
                {

                    this.verticalIndex = this.swingJumpIndex;
                    this.HangingStop();
                    this.Jump();

                }
                else if (this.buttonPressed.isGrappling)
                    this.Retract(true);
            }
        }

        IEnumerator SwingRoutine()
        {

            int scaler = (int)this.grappler.grapplingDirection.x;
            float amplitude = 4f;

            if (this.grappler.grapplingDirection == Vector2.up)
            {

                scaler = this.body.renderer.flipX ? 1 : -1;
                amplitude = 3f;
            }
            else if (this.grappler.grapplingDirection == Vector2.left || this.grappler.grapplingDirection == Vector2.right)
                amplitude = 5f;

            int acceleration = 0;
            float progress = 0;
            int progressScaler = 1;

            bool doAcceleration = false;
            bool isSwingCompleted = false;

            float speed = this.grappler.swingSpeed;

            while (this.unit.state.isSwing == true)
            {

                Vector3 grapplerDeg = this.grappler.body.angles;

                if (grapplerDeg.z == 0) grapplerDeg.z = 360;

                if (this.newMovement.x != 0)
                {
                    if ((this.newMovement.x < 0 && acceleration == 1) || (this.newMovement.x > 0 && acceleration == -1) || acceleration == 0)
                    {

                        acceleration = (int)this.newMovement.x;

                        if (scaler == acceleration && progressScaler == 1)
                        {

                            doAcceleration = true;
                            speed *= 2;
                        }
                    }
                }

                if (progressScaler == 1 && progress < amplitude) progress += 0.1f * speed;
                else if (progressScaler == -1) progress -= 0.1f * speed;

                this.swingMovement = Mathf.Lerp(0, 10, progress / 10) * scaler;

                if ((grapplerDeg.z < 270 && grapplerDeg.z + this.swingMovement > 270) || (grapplerDeg.z > 270 && grapplerDeg.z + this.swingMovement < 270) || grapplerDeg.z == 270)
                {

                    progressScaler = -1;

                    if (doAcceleration == true)
                    {

                        amplitude = amplitude < 5f ? amplitude + 0.25f : amplitude;
                        progress = amplitude + 2.25f;
                        doAcceleration = false;

                    }
                    else
                        progress = amplitude;
                }

                //41,42,43,44,45,47,49,50,51
                //52,53

                if (this.swingMovement == 0)
                {

                    if (grapplerDeg.z < 270) scaler = 1;
                    else if (grapplerDeg.z > 270) scaler = -1;

                    //if(doAcceleration == true) doAcceleration = false;
                    //else { amplitude = amplitude > 0 ? amplitude - 0.5f : amplitude; }

                    progressScaler = 1;
                    progress = 0;
                    acceleration = 0;
                    speed = this.grappler.swingSpeed;
                }

                yield return new WaitForFixedUpdate();
            }
        }

        public void Catch()
        {

            if (this.unit.state.isBind == false)
            {

                this.unit.state.isBind = true;
                this.unit.state.isGrappling = false;
                //this.unit.state.isFreeze = false;

                this.grappler.Bind();
            }
            else
            {
                //if (this._grappleExec == false)
                //{

                this.Pull();
                //}
            }
        }

        private void Pull()
        {

            if (this.unit.state.isPulling == false)
            {

                this.unit.state.isPulling = true;

                this.pullRoutine = this.PullRoutine();
                this.StartCoroutine(this.pullRoutine);
            }
        }

        IEnumerator PullRoutine()
        {

            float minWidth = this.grappler.throwDistance * 0.2f;

            while (this.unit.state.isPulling == true)
            {

                if (this.grappler.body.width >= minWidth)
                {

                    this.grappler.Resize(new Vector2(this.grappler.body.width - this.grappler.retractSpeed, this.grappler.height), false, false);

                }
                else
                    this.PullStop();

                yield return new WaitForFixedUpdate();
            }
        }

        private void PullStop()
        {

            this.unit.state.isPulling = false;
            this.unit.state.isBind = false;

            this.currentCatch.unit.state.isBound = false;
            this.currentCatch = null;

            this.grappler.GrapplingStop();
        }

        private void Hanging()
        {

            if (this.unit.state.isHanging == false)
            {

                this.unit.state.isHanging = true;
                this.unit.state.isSwing = false;
                this.unit.state.isRetract = false;
                this.grappler.transform.eulerAngles = new Vector3(this.grappler.transform.eulerAngles.x, this.grappler.transform.eulerAngles.y, 270);

                this.StopCoroutine(this.swingRoutine);
            }
            else
            {

                if (this.buttonPressed.isJump == true)
                {
                    this.HangingStop();
                    this.verticalIndex = this.swingJumpIndex;
                    this.Jump();
                }
                else if (this.newMovement.y < 0)
                {

                    if (this.grappler.swingLength == 0) this.grappler.swingLength = this.grappler.throwDistance;

                    if (this.grappler.body.width < this.grappler.swingLength && Physics2D.Raycast(new Vector2(this.unit.x, this.unit.bottom - 0.01f), Vector2.down, 0.1f).collider == null)
                    {

                        this.unit.state.isSwing = true;

                        this.grappler.Resize(new Vector2(this.grappler.body.width + this.grappler.retractSpeed, this.grappler.height), false, true);
                    }
                }
                else
                {

                    if (this.unit.state.isSwing == true)
                    {

                        if (this.newMovement.y > 0)
                        {

                            if (this.grappler.body.width > 0)
                            {

                                this.grappler.Resize(new Vector2(this.grappler.body.width - this.grappler.retractSpeed, this.grappler.height), false, true);
                            }
                            else
                                this.unit.state.isSwing = false;

                        }
                        else if (this.newMovement.x != 0)
                        {

                            this.unit.state.isSwing = false;
                            this.unit.state.isHanging = false;

                            this.grappler.body.zAngle -= this.newMovement.x;
                            this.grappler.grapplingDirection = new Vector2(0 - this.newMovement.x, 2);
                            this.Swing();
                        }
                    }
                }
            }
        }

        private void HangingStop()
        {

            this.unit.state.isHanging = false;
            this.unit.state.isSwing = false;
            this.unit.state.isFreeze = false;
            this.swingMovement = 0f;
            this.grappler.Disable();

            if (this.newMovement.y < 0) this.unit.state.isFalling = true;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.collider.tag == "Wall")
            {
                if (this.unit.state.isSwing)
                {

                    this.unit.state.isFalling = true;

                    this.HangingStop();
                    this.Jump();

                }
                else
                {

                    if (this.verticalIndex > 0 && !Stage.currentRoom.GetWall(collision.collider.name).isPlatform)
                    {

                        RaycastHit2D topHit = Util.HitScan(this.unit, Vector2.up, 0.1f, 0.1f);

                        if (topHit.collider != null) this.verticalIndex = 0;

                    }
                    else
                        this.currentPlatform = Stage.currentRoom.GetWall(collision.collider.name);
                }
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            //if (collision.collider.tag == "Ptform") this.currentPlatform = null;

            if (!(this.unit.state.isHanging || this.unit.state.isRetract || this.unit.state.isSwing || this.unit.state.isJump) && Util.HitScan(this.unit, Vector2.down).collider == null)
            {
                this.unit.state.isFalling = true;
                this.Jump();
            }
        }
    }
}
