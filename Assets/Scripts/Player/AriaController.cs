﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Rewired;
using UnityEngine;

namespace SKY {
    public class AriaController : MonoBehaviour {
        public enum State {
            None,
            Idle,
            Hit,
            Running,
            JumpUp,
            JumpDown,
            Attacking,
            Shooting,
            Grappling,
            Swinging,
            Retracting
        }

        [Header ("Player Stats")]
        public State state;
        private State prevState;
        public LayerMask groundLayer;
        public float groundDetector;
        public float walkingSpeed;
        public float jumpVelocity;

        private Rigidbody2D _rigidbody;
        private Transform _transform;
        public Transform visual;
        private Animator _animator;

        private IHE_UnitHealth health;
        private IHE_Grapple grapple;
        private Vector2 input;
        private bool isGrounded;
        private bool prevIsGrounded;
        public bool attacking = false;
        public bool uninteruptable = false;
        public bool animAttacking = false;

        [Header ("Combo Stats")]
        public float attackTime = 0;
        public float tressHoldTime = 0;
        public bool animAttackPlaying = false;

        public float comboAnimationTresshold = .7f;
        public float[] attackAnimTime = new float[] { 0.444f, 0.5f, 0.611f };
        public float[] attackAirAnimTime;
        public float coyoteTime = .2f;
        public int currentCombo = 1;
        private int prevCombo = 1;
        private int maxCombo = 3;
        // public int comboCount = 0;

        public bool isGrappling = false;
        public bool isSwinging = false;
        public bool isRetracting = false;
        private bool hit = false;
        private bool grappleGround = false;

        private IHE_Sword[] swords;
        private IHE_LaserWeapon laser;
        private SpriteRenderer[] _sprites;

        private Rewired.Player player;

        private string curAnim = "";
        private bool facingRight = false;
        public bool vulnerable = true;

        void Awake () {
            _rigidbody = GetComponent<Rigidbody2D> ();
            _transform = transform;
            _sprites = GetComponentsInChildren<SpriteRenderer> (true);
            _animator = GetComponentInChildren<Animator> ();
            swords = GetComponentsInChildren<IHE_Sword> ();
            laser = GetComponentInChildren<IHE_LaserWeapon> ();
            health = GetComponent<IHE_UnitHealth> ();
            player = ReInput.players.GetPlayer (0);
            grapple = GetComponentInChildren<IHE_Grapple> ();
        }

        void Start () {
            StartCoroutine ("PlayerUpdate");
            StartCoroutine ("PlayerFixedUpdate");

        }

        private void PlayAnim (string animName, State stateCheck) {
            if (prevState != stateCheck) {
                _animator.Play (animName);
                state = stateCheck;
            }
        }
        private void PlayAnimAttack (string animName, State stateCheck) {
            if (prevState != stateCheck || currentCombo != prevCombo) {
                _animator.Play (animName);
                state = stateCheck;
            }
        }

        private void PlayAnimForce (string animName, float normalizedTime) {
            _animator.Play (animName, 0, normalizedTime);
        }

        IEnumerator PlayerUpdate () {
            float turnAround = 0.1f;
            while (true) {
                isGrounded = Physics2D.Raycast (_transform.position, Vector2.down, groundDetector, groundLayer);
                if (hit) {

                } else if (isGrappling) {
                    input = new Vector2 (player.GetAxisRaw ("Move Horizontal"), player.GetAxisRaw ("Move Vertical"));
                    if (isGrounded)
                        input = Vector2.zero;
                } else if (isSwinging) {
                    Swinging ();
                } else if(isRetracting){
                    Retracting();
                } else if (!laser.isShooting) {
                    input = new Vector2 (player.GetAxisRaw ("Move Horizontal"), player.GetAxisRaw ("Move Vertical"));
                    if (attacking) {
                        if (isGrounded)
                            input.x = 0;
                        else
                            input.x *= .5f;
                    }
                    UpdateSprite (_rigidbody.velocity.x);
                    Attack ();
                    Jump ();
                    Grapple ();
                } else {
                    input.x = 0;
                }

                // animation stuff
                if (hit) {
                    PlayAnim ("Aria_Hit", State.Hit);
                } else if (isGrappling) {
                    switch(grapple.grappleType){
                        case IHE_Grapple.GrappleType.Up:
                            PlayAnim ("Aria_Grab (+90)", State.Grappling);
                            break;
                        case IHE_Grapple.GrappleType.Front:
                        case IHE_Grapple.GrappleType.FrontGround:
                            PlayAnim ("Aria_Grab_Front", State.Grappling);
                            break;
                        case IHE_Grapple.GrappleType.Corner:
                            PlayAnim ("Aria_Grab (+45)", State.Grappling);
                            break;
                    }
                    
                } else if (isSwinging) {
                    PlayAnim ("Aria_Hanging", State.Swinging);
                    // float range = grapple.GetGrappleRange();
                    // float normalizedTime = (5f + range) / 10f;
                    // PlayAnimForce("Aria_Swing", normalizedTime);
                    // print(normalizedTime);
                } else if (attacking) {
                    // prevents attacking continue while landed or jump
                    if ((isGrounded && !prevIsGrounded) || (!isGrounded && prevIsGrounded)) {
                        CancelAttack ();
                    } else {
                        if (isGrounded) PlayAnimAttack ("Aria_Attack_Combo0" + currentCombo, State.Attacking);
                        else PlayAnimAttack ("Aria_Aerial_Attack0" + currentCombo, State.Attacking);
                    }
                } else {
                    if (isGrounded) {
                        if (Mathf.Abs (_rigidbody.velocity.x) < .1f) {
                            if (turnAround > 0) turnAround -= Time.deltaTime;
                            else PlayAnim ("Aria_Idle", State.Idle);
                        } else {
                            turnAround = .1f;
                            PlayAnim ("Aria_Run", State.Running);
                        }
                    } else {
                        if (_rigidbody.velocity.y >.05f) {
                            PlayAnim ("Aria_Jump Up", State.JumpUp);
                        } else {
                            PlayAnim ("Aria_Jump Down", State.JumpDown);
                        }
                    }
                }
                yield return null;
                prevState = state;
                prevCombo = currentCombo;
                prevIsGrounded = isGrounded;
            }
        }

        IEnumerator PlayerFixedUpdate () {
            while (true) {
                if (!(isSwinging || isRetracting)) {
                    _rigidbody.velocity = new Vector2 (input.x * walkingSpeed, _rigidbody.velocity.y);
                }
                yield return new WaitForFixedUpdate ();
            }
        }

        void UpdateSprite (float velocity_x) {
            if (velocity_x > 0) {
                visual.localScale = new Vector2 (-.8f, .8f);
                facingRight = true;
            }

            if (velocity_x < 0) {
                visual.localScale = new Vector2 (.8f, .8f);
                facingRight = false;
            }
        }

        void RangeAttack () {
            // if(player.GetButtonDown("Attack")){
            //     laser.Shoot(_visual.flipX);
            // }

            // if(player.GetButtonUp("Attack")){
            //     laser.StopShoot();
            // }
            // _animator.SetBool("laser", laser.isShooting);
        }

        void Grapple () {
            if (player.GetButtonDown ("Grap")) {
                Vector2 temp = input;
                if (temp == Vector2.zero)
                    temp.x = facingRight ? 1 : -1;
                temp.y = temp.y < 0 ? 0 : temp.y;

                grapple.Throw (temp);
            }
        }

        void Swinging () {
            input = new Vector2 (player.GetAxisRaw ("Move Horizontal"), player.GetAxisRaw ("Move Vertical"));
            UpdateSprite (input.x);
            // Vector2 temp = input;
            
            grapple.UpdateForce (input);

            if (isSwinging && isGrounded)
                grapple.CancelGrapple ();

            if (player.GetButtonDown ("Jump")) {
                grapple.CancelGrapple ();
                _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, jumpVelocity);
            }
            if(player.GetButtonDown ("Grap")){
                grapple.AirRetract();
            }
        }

        void Retracting(){
            Vector2 dir = grapple.anchorPoint.position - _transform.position;
            _transform.Translate(grapple.direction * grapple.grappleSpeed * Time.deltaTime);
            if(Mathf.Abs(_transform.position.x - grapple.retractPoint.x) < .5f){
                grapple.CancelGrapple();
                isRetracting = false;
            }
        }

        // public void GrappleGroundChecker(){
        //     StartCoroutine("DoGrappleGroundChecker");
        // }
        // IEnumerator DoGrappleGroundChecker(){
        //     grappleGround = false;
        //     yield return new WaitForSeconds(.1f);
        //     while(!grappleGround){
        //         if(isGrounded){
        //             grappleGround = true;
        //         }
        //     }
        // }

        public void AddForceOnRope (Vector2 val) {
            if (Mathf.Abs (_rigidbody.velocity.magnitude) < 8f)
                _rigidbody.AddForce (val, ForceMode2D.Force);
        }

        void Attack () {
            if (player.GetButtonDown ("Attack")) {
                if (!uninteruptable) {
                    if (tressHoldTime > 0) {
                        currentCombo++;
                        if (currentCombo > maxCombo) currentCombo = 1;
                    }
                    uninteruptable = true;
                    attackTime = isGrounded ? attackAnimTime[currentCombo - 1] : attackAirAnimTime[currentCombo - 1];
                    tressHoldTime = comboAnimationTresshold;
                    int swordID = isGrounded ? 0 : 1;
                    swords[swordID].Activate (currentCombo - 1, facingRight);

                    // StartCoroutine(PauseGravity(attackTime));
                }
            }

            if (attackTime > 0) {
                attacking = true;
                attackTime -= Time.deltaTime;
                if (attackTime < coyoteTime)
                    uninteruptable = false;
            } else {
                attacking = false;
                if (tressHoldTime > 0)
                    tressHoldTime -= Time.deltaTime;
                else currentCombo = 1;
            }
        }

        void CancelAttack () {
            attacking = false;
            currentCombo = 1;
            uninteruptable = false;
            attackTime = 0;
            tressHoldTime = 0;
            swords[0].Cancel ();
            swords[1].Cancel ();
        }

        IEnumerator PauseGravity (float time) {
            float temp = _rigidbody.velocity.y;
            if (temp < 0) {
                _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, 0);
                _rigidbody.gravityScale = 0;
            }
            yield return new WaitForSeconds (time);
            _rigidbody.gravityScale = 3;
            _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, temp);
        }

        void Jump () {
            if (player.GetButtonDown ("Jump") && isGrounded) {
                _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, jumpVelocity);
            }
            if (player.GetButtonUp ("Jump")) {
                _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, _rigidbody.velocity.y * .5f);
            }
        }

        public bool IsGrounded(){
            return this.isGrounded;
        }

        public void Hit (float damage, bool right) {
            if (!vulnerable)
                return;

            if (health.Hit (damage))
                Dead ();

            StopCoroutine ("Tint");
            StartCoroutine ("Tint");
            StartCoroutine (DoHit (right));
        }

        private IEnumerator DoHit (bool right) {
            hit = true;
            vulnerable = false;
            input.x = .5f * (right ? -1 : 1);
            _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, jumpVelocity * .3f);
            // _transform.DOMoveX(_transform.position.x + (.5f * (right ? -1 : 1)), .5f);
            yield return new WaitForSeconds (.5f);
            input.x = 0;
            vulnerable = true;
            hit = false;
        }

        private IEnumerator Tint () {
            float amount = 1f;
            while (amount > 0) {
                for (int i = 0; i < _sprites.Length; i++) {
                    _sprites[i].material.SetFloat ("_FlashAmount", amount);
                }
                amount -= Time.deltaTime * 2;
                yield return null;
            }
        }

        void Dead () {
            StopAllCoroutines ();
            Destroy (this.gameObject);
        }

        void OnDrawGizmosSelected () {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine (transform.position, transform.position + (Vector3.down * groundDetector));
        }
    }
}