﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
	public class IHE_Grapple : MonoBehaviour {

		public enum GrappleType{
			None, Front, Up, Corner, FrontGround
		}

		public GrappleType grappleType;

		public LayerMask hangPointLayer;
		public float maxLength;
		public float swingForce;
		public Vector3 startPos;
		public Vector3 direction = Vector2.up;
		public Transform anchorPoint;
		public Vector2 retractPoint;
		public float grappleSpeed;

		private Transform _transform;
		private LineRenderer line;
		private DistanceJoint2D joint;
		private AriaController aria;

		private bool hitAnchor = false;
		private Vector2 hitPos;
		private HangPos currentHangPos;
		private bool isAirRetract = false;

		private IEnumerator grapplingCoroutine;

		private void Awake () {
			_transform = transform;
			line = GetComponentInChildren<LineRenderer> ();
			joint = GetComponentInParent<DistanceJoint2D> ();
			aria = GetComponentInParent<AriaController> ();
		}

		private void Start () {
			line.enabled = false;
		}

		public void Throw (Vector2 dir) {
			direction = dir;
			if(dir.x != 0 && dir.y == 0){
				if(aria.IsGrounded())
					grappleType = GrappleType.FrontGround;
				else
					grappleType = GrappleType.Front;
			}else if(dir.x != 0 && dir.y !=0){
				grappleType = GrappleType.Corner;
			}else{
				grappleType = GrappleType.Up;
			}
			grapplingCoroutine = DoThrow ();
			StartCoroutine (grapplingCoroutine);
		}

		IEnumerator DoThrow () {
			aria.isGrappling = true;
			line.enabled = true;
			// switch(grappleType){
			// 	case GrappleType.Up:
			// 		startPos = new Vector2(0, 2f);
			// 		break;
			// 	default:
			// 		startPos = new Vector2(0, 1.5f);
			// 		break;
			// }
			_transform.localPosition = startPos;
			// startPos = _transform.position;
			Vector3 currentPos;
			float currentVal = 0;

			RaycastHit2D hit;

			while (Vector3.Distance (_transform.position, anchorPoint.position) < maxLength) {
				currentVal += grappleSpeed * Time.deltaTime;
				currentPos = _transform.position + (direction * currentVal);
				anchorPoint.position = currentPos;
				line.SetPositions (new Vector3[] { _transform.position, anchorPoint.position });
				hit = Physics2D.Linecast(_transform.position, anchorPoint.position, hangPointLayer);
				if (hit) {
					print(hit.transform.name);
					hitPos = hit.collider.bounds.center;
					Hang (hit.collider.GetComponent<HangPos>());
					yield break;
				}
				yield return null;
			}
			if (!hitAnchor) {
				grapplingCoroutine = Pull ();
				StartCoroutine (grapplingCoroutine);
			}
		}

		IEnumerator Pull () {
			grappleType = GrappleType.None;
			Vector3 currentPos;
			float currentVal = Vector3.Distance (_transform.position, anchorPoint.position);
			print(currentVal);
			RaycastHit2D hit;
			while (currentVal >.1f) {
				currentVal -= grappleSpeed * Time.deltaTime;
				currentPos = _transform.position + (direction * currentVal);
				anchorPoint.position = currentPos;
				line.SetPositions (new Vector3[] { _transform.position, anchorPoint.position });
				hit = Physics2D.Linecast(_transform.position, anchorPoint.position, hangPointLayer);
				if (hit) {
					hitPos = hit.collider.bounds.center;
					Hang (hit.collider.GetComponent<HangPos>());
					yield break;
				}
				yield return null;
			}
			line.enabled = false;
			aria.isGrappling = false;
		}

		void Hang (HangPos hp) {
			currentHangPos = hp;
			print(currentHangPos.name);
			currentHangPos.isUsed = true;
			aria.isGrappling = false;
			if(grappleType == GrappleType.FrontGround){
				grapplingCoroutine = Retracting ();
				StartCoroutine (grapplingCoroutine);
			}else{
				_transform.localPosition = new Vector2(0, 3);
				grapplingCoroutine = DoHang ();
				StartCoroutine (grapplingCoroutine);
			}
		}

		IEnumerator DoHang () {
			joint.enabled = true;
			anchorPoint.position = hitPos;
			joint.distance = Vector3.Distance (_transform.position, anchorPoint.position) - .5f;
			if(currentHangPos.movingHangPos){
				anchorPoint.SetParent(currentHangPos.transform);
			}else{
				anchorPoint.SetParent (null);
			}
			yield return new WaitForEndOfFrame();
			aria.isSwinging = true;
			while (true) {
				line.SetPositions (new Vector3[] { _transform.position, anchorPoint.position });

				yield return null;
			}
		}

		IEnumerator Retracting(){
			print("rretrac");
			retractPoint = anchorPoint.position;
			aria.isRetracting = true;
			while(true){
				line.SetPositions (new Vector3[] { _transform.position, retractPoint });
				yield return null;
			}
		}

		public void AirRetract(){
			StartCoroutine("DoAirRetract");
		}

		IEnumerator DoAirRetract(){
			isAirRetract = true;
			while(joint.distance > .2f){
				joint.distance -= 10f * Time.deltaTime;
				yield return null;
			}
			isAirRetract = false;
		}

		public void UpdateForce (Vector2 val) {
			if(!isAirRetract) joint.distance += val.y * -5f * Time.deltaTime;
			joint.distance = Mathf.Clamp (joint.distance, .2f, 10f);
			if (Mathf.Abs (val.x) > 0 && val.y == 0 && _transform.position.y < anchorPoint.position.y) {
				Vector2 playerToHookDirection = ((Vector2) anchorPoint.position - (Vector2) _transform.position).normalized;
				Vector2 perpendicularDirection;
				if (val.x < 0) {
					perpendicularDirection = new Vector2 (-playerToHookDirection.y, playerToHookDirection.x);
					var leftPerpPos = (Vector2) transform.position - perpendicularDirection * -2f;
					Debug.DrawLine (transform.position, leftPerpPos, Color.green, 0f);
				} else {
					perpendicularDirection = new Vector2 (playerToHookDirection.y, -playerToHookDirection.x);
					var rightPerpPos = (Vector2) transform.position + perpendicularDirection * 2f;
					Debug.DrawLine (transform.position, rightPerpPos, Color.green, 0f);
				}

				Vector2 force = perpendicularDirection * swingForce;
				aria.AddForceOnRope (force);
			}
		}

		public float GetGrappleRange(){
			return _transform.position.x - anchorPoint.position.x;
		}

		public void CancelGrapple(){
			if(grapplingCoroutine != null)
				StopCoroutine(grapplingCoroutine);
			joint.enabled = false;
			line.enabled = false;
			anchorPoint.SetParent(transform);
			anchorPoint.localPosition = Vector3.zero;
			aria.isGrappling = false;
			aria.isSwinging = false;
			currentHangPos.isUsed = false;
			currentHangPos = null;
		}
	}
}