﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace SKY
{ 
    public class IHE_LaserWeapon : MonoBehaviour
    {
        public LayerMask enemyLayer;
        // public float maxEnergy = 100f;
        // public float currentEnergy;
        public IHE_UnitEnergy energy;
        public float minWidth = .3f;
        public float maxWidth = .6f;
        public float decreaseEnergyRate = 10f;

		public float fireRange = 10f;
		public float fireRate = .3f;
		public float damage = 10f;
		private float timer;

        public bool isShooting = false;

        private RaycastHit2D[] hits;
        private Transform _transform;

        private LineRenderer line;
        private float initX;
        private Tweener curTween;
        private Tweener longTween;

        int dir = 1;

        void Awake()
        {
            _transform = transform;
            line = GetComponent<LineRenderer>();
        }

        void Start()
        {
            if (!energy){
                GetComponentInParent<IHE_UnitEnergy>();
            }
            initX = _transform.localPosition.x;
        }


        public void Shoot(bool flipX)
        {
            dir = flipX ? -1 : 1;
            _transform.localPosition = new Vector3(flipX ? -initX: initX, _transform.localPosition.y, 0);
            if (!energy.IsEmpty())
            {
                StartCoroutine("DoShoot");
            }
        }

        IEnumerator DoShoot()
        {
            // int dif = 10;
            // float range = fireRange / dif;
            // float amplitude = .2f;
            // line.positionCount = dif;
            // RandomizeLine(dif, range, amplitude);

            isShooting = true;
			timer = fireRate;
            

            // float ampInterval = .05f;
            // float ampTimer = ampInterval;

            float width = minWidth;
            float curLong = 0;
            line.startWidth = .1f;
            line.endWidth = .1f; 
            line.SetPosition(1, new Vector3(fireRange * dir,0,0));
            line.enabled = true;
            yield return new WaitForSeconds(.1f);
            curTween = DOTween.To(()=> width, x => width = x, maxWidth, .1f).SetLoops(-1, LoopType.Yoyo);
            
            // longTween = DOTween.To(()=> curLong, x => curLong = x, fireRange, .1f);
            
            while (!energy.IsEmpty())
            {
				timer -= Time.deltaTime;
                line.startWidth = width;
                line.endWidth = width;     
                // line.SetPosition(1, new Vector3(curLong,0,0));
                // ampTimer -= Time.deltaTime;
                // if(ampTimer <= 0){
                //     RandomizeLine(dif, range, amplitude);
                //     ampTimer = ampInterval;
                // }
                energy.Decrease(decreaseEnergyRate * Time.deltaTime);
				if(timer <= 0){
					hits = Physics2D.BoxCastAll(_transform.position, new Vector2(1, 1), 0f, Vector2.right * dir, fireRange, enemyLayer);
					if (hits.Length > 0)
					{
						for (int i = 0; i < hits.Length; i++)
						{
							IHE_Enemy e = hits[i].transform.GetComponent<IHE_Enemy>();
							if(e)
								e.Hit(damage);
								
						}
					}
					timer = fireRate;
				}
                
                yield return null;
            }
            StopShoot();
        }

        void RandomizeLine(int dif, float range, float amplitude){
            for (int i = 1; i < dif; i++)
            {
                line.SetPosition(i, new Vector3(dir * range * i, Random.Range(-amplitude, amplitude), 0));
            }
        }

        public void StopShoot()
        {
            isShooting = false;
            line.enabled = false;
            curTween.Kill();
            longTween.Kill();
            StopCoroutine("DoShoot");
        }
    }

}