﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
    public class IHE_Player : MonoBehaviour {
        [Header ("Player Stats")]
        public LayerMask groundLayer;
        public float groundDetector;
        public float walkingSpeed;
        public float jumpForce;

        private Rigidbody2D _rigidbody;
        private Transform _transform;
        private SpriteRenderer _visual;
        private Animator _animator;

        private IHE_UnitHealth health;
        private Vector2 input;
        private bool isGrounded;
        public bool attacking = false;

        [Header ("Combo Stats")]
        private float animStart = 0;
        private float lastComboTime = 0;
        public float comboAnimationTresshold = .7f;
        public float animSpeedMultiplier = 2f;
        public float[] attackAnimTime = new float[] { 0.444f, 0.5f, 0.611f };
        private bool coyoteAttack = false;
        public float coyoteTime = .2f;
        private int currentCombo = 1;
        private int maxCombo = 3;
        public int comboCount = 0;

        public bool isGrappling = false;

        private IHE_Sword sword;
        private IHE_LaserWeapon laser;
        private SpriteRenderer[] _sprites;
        //private ARA.WeaponGrappler grappler;

        void Awake () {
            _rigidbody = GetComponent<Rigidbody2D> ();
            _transform = transform;
            _visual = GetComponentInChildren<SpriteRenderer> ();
            _sprites = GetComponentsInChildren<SpriteRenderer> ();
            _animator = GetComponentInChildren<Animator> ();
            sword = GetComponentInChildren<IHE_Sword> ();
            laser = GetComponentInChildren<IHE_LaserWeapon> ();
            health = GetComponent<IHE_UnitHealth> ();

            //this.grappler = GetComponentInChildren<ARA.WeaponGrappler>();
        }

        void Start () {
            StartCoroutine ("PlayerUpdate");
            StartCoroutine ("PlayerFixedUpdate");

            if (animSpeedMultiplier > 1) {
                _animator.SetFloat ("animSpeed", animSpeedMultiplier);
                for (int i = 0; i < attackAnimTime.Length; i++) {
                    attackAnimTime[i] /= animSpeedMultiplier;
                }
            }
        }

        IEnumerator PlayerUpdate () {
            while (true) {
                if (!laser.isShooting && !this.isGrappling) {
                    input = new Vector2 (Input.GetAxisRaw ("Horizontal"), Input.GetAxis ("Vertical"));
                    if (attacking && isGrounded)
                        input.x = 0;

                    UpdateSprite (_rigidbody.velocity.x);

                    Jump ();
                    Attack ();

                } else {
                    input.x = 0;
                }

                //if(this.Grappling() == false) RangeAttack();

                // detect ground
                isGrounded = Physics2D.Raycast (_transform.position, Vector2.down, groundDetector, groundLayer);
                _animator.SetBool ("isGrounded", isGrounded);
                _animator.SetFloat ("velocity_x", Mathf.Abs (_rigidbody.velocity.x));
                _animator.SetFloat ("velocity_y", _rigidbody.velocity.y);
                yield return null;
            }
        }

        IEnumerator PlayerFixedUpdate () {
            while (true) {
                _rigidbody.velocity = new Vector2 (input.x * walkingSpeed, _rigidbody.velocity.y);
                yield return new WaitForFixedUpdate ();
            }
        }

        void UpdateSprite (float velocity_x) {
            if (velocity_x > 0) {
                _visual.flipX = false;
                sword.Flip (true);
            }

            if (velocity_x < 0) {
                _visual.flipX = true;
                sword.Flip (false);
            }
        }

        void RangeAttack () {
            if (Input.GetKeyDown (KeyCode.I) || Input.GetButtonDown ("Fire2")) {
                laser.Shoot (_visual.flipX);
            }

            if (Input.GetKeyUp (KeyCode.I) || Input.GetButtonUp ("Fire2")) {
                laser.StopShoot ();
            }
            _animator.SetBool ("laser", laser.isShooting);
        }

        void Attack () {
            if (Time.time > lastComboTime + comboAnimationTresshold)
                currentCombo = 1;

            if (Input.GetKeyDown (KeyCode.J) || Input.GetButtonDown ("Fire1")) {
                lastComboTime = Time.time;
                if (attacking) {
                    if (Time.time > animStart + attackAnimTime[currentCombo - 1] - coyoteTime && !coyoteAttack) {
                        // print(Time.time + " " + animStart);
                        coyoteAttack = true;
                        comboCount++;
                    }
                } else {
                    comboCount++;
                }
            }

            if (comboCount > 0 && !attacking) {
                animStart = Time.time;
                coyoteAttack = false;
                StartCoroutine ("DoAttack");
                StartCoroutine ("PauseGravity");
            }
        }

        IEnumerator DoAttack () {
            attacking = true;
            comboCount--;
            _animator.SetInteger ("attack", currentCombo);
            sword.Activate (.1f, .1f);
            yield return new WaitForSeconds (attackAnimTime[currentCombo - 1]);
            currentCombo++;
            if (currentCombo > maxCombo) currentCombo = 1;
            _animator.SetInteger ("attack", 0);
            attacking = false;

        }

        IEnumerator PauseGravity () {
            // float velY = _rigidbody.velocity.y;
            // _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, 0);
            // yield return new WaitForSeconds(.07f);
            // _rigidbody.velocity = new Vector2(_rigidbody.velocity.x, velY);
            _rigidbody.velocity = new Vector2 (_rigidbody.velocity.x, _rigidbody.velocity.y * .2f);
            yield return new WaitForSeconds (.1f);
        }

        void Jump () {
            if ((Input.GetKeyDown (KeyCode.K) || Input.GetButtonDown ("Jump")) && isGrounded) {
                _rigidbody.AddForce (Vector2.up * jumpForce);
            }
            if (Input.GetKeyUp (KeyCode.K) || Input.GetButtonUp ("Jump")) {
                _rigidbody.velocity *= .5f;
            }
        }

        public void Hit (float damage) {
            if (health.Hit (damage))
                Dead ();

            StopCoroutine ("Tint");
            StartCoroutine ("Tint");
        }

        private IEnumerator Tint () {
            float amount = 1f;
            while (amount > 0) {
                for (int i = 0; i < _sprites.Length; i++) {
                    _sprites[i].material.SetFloat ("_FlashAmount", amount);
                }
                amount -= Time.deltaTime * 2;
                yield return null;
            }
        }

        void Dead () {
            StopAllCoroutines ();
            Destroy (this.gameObject);
        }

        void OnDrawGizmosSelected () {
            if (_transform) {
                Gizmos.color = Color.blue;
                Gizmos.DrawLine (_transform.position, _transform.position + (Vector3.down * groundDetector));
            }
        }
    }
}