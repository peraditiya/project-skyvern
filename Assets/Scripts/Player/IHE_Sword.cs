﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class IHE_Sword : MonoBehaviour
    {
		[System.Serializable]
		public struct SwordProperties{
			public float damage;
			public Collider2D collider;
			public int timing;
			public float duration;
		}

		public SwordProperties[] prop;

		public enum Owner{
			Player, Enemy
		}

		public Owner owner;

		PolygonCollider2D[] cols;

		float offsetX;

		private float curDamage;
		private Transform _transform;

		private Transform ownerTransform;

		IEnumerator swordCoroutine;

        void Awake()
        {
			cols = GetComponentsInChildren<PolygonCollider2D>(true);
			for (int i = 0; i < prop.Length; i++)
			{
				PolygonCollider2D col = gameObject.AddComponent<PolygonCollider2D>();
				for (int j = 0; j < cols[i].pathCount; j++)
				{
					col.isTrigger = true;
					col.SetPath(j, cols[i].GetPath(j));
					prop[i].collider = col;
				}
				col.enabled = false;
				Destroy(cols[i].gameObject);

			}
			_transform = transform;
			if(owner == Owner.Player)
				ownerTransform = GetComponentInParent<AriaController>().transform;
        }

		void Start(){
			offsetX = transform.localPosition.x;
		}

        void OnTriggerEnter2D(Collider2D col)
        {
			switch(owner){
				case Owner.Player:
					IHE_Enemy e = col.GetComponent<IHE_Enemy>();
					IHE_EnemyPart ep = col.GetComponent<IHE_EnemyPart>();
					if (e){
						e.Hit(curDamage, ownerTransform);
					}else if (ep){
						ep.Hit(curDamage, ownerTransform);
					}
				break;
				case Owner.Enemy:
					IHE_Player p = col.GetComponent<IHE_Player>();
					if(p){
						p.Hit(curDamage);
					}
					AriaController a = col.GetComponent<AriaController>();
					if(a) a.Hit(curDamage, _transform.position.x > col.transform.position.x);
				break;
			}
			
   		}

		private void ActivateCol(int id){
			for (int i = 0; i < prop.Length; i++)
			{
				if(i == id) prop[i].collider.enabled = true;
				else prop[i].collider.enabled = false;
			}
		}

		public void Activate(float delay, float duration){
			swordCoroutine = DoActivate(delay, duration);
			StartCoroutine(swordCoroutine);
		}

		public void Activate(int combo, bool facingRight){
			Flip(facingRight);
			curDamage = prop[combo].damage;
			swordCoroutine = DoActivate(prop[combo].timing / 60f, prop[combo].duration, combo);
			StartCoroutine(swordCoroutine);
		}

		IEnumerator DoActivate(float delay, float duration, int combo = 1){
			yield return new WaitForSeconds(delay);
			ActivateCol(combo);
			// collision works if moving
			transform.Translate(Vector3.right * .05f);
			yield return null;
			transform.Translate(Vector3.left * .05f);

			yield return new WaitForSeconds(duration);
			Deactivate();
		}

		private void Deactivate(){
			for (int i = 0; i < prop.Length; i++)
			{
				if(prop[i].collider.enabled)
					prop[i].collider.enabled = false;
			}
		}

		public void Cancel(){
			if(swordCoroutine != null){
				StopCoroutine(swordCoroutine);
				Deactivate();
			}
		}

		public void Flip(bool right){
			transform.localScale = new Vector2(right ? -1:1, 1);
		}
    }
}
