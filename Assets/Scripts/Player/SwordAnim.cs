﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
	public class SwordAnim : MonoBehaviour {

		public enum Owner {
			Player,
			Enemy
		}

		public Owner owner;
		public float damage;

		private Transform ownerTransorm;

		private void Awake() {
			ownerTransorm = GetComponentInParent<IHE_Enemy>().transform;
		}

		void OnTriggerEnter2D (Collider2D col) {
			switch (owner) {
				case Owner.Player:
					IHE_Enemy e = col.GetComponent<IHE_Enemy> ();
					IHE_EnemyPart ep = col.GetComponent<IHE_EnemyPart> ();
					if (e) {
						e.Hit (damage);
					} else if (ep) {
						ep.Hit (damage, ownerTransorm);
					}
					break;
				case Owner.Enemy:
					AriaController a = col.GetComponent<AriaController> ();
					if (a) {
						a.Hit (damage, ownerTransorm.position.x > col.transform.position.x);
					}
					
					break;
			}

		}
	}
}