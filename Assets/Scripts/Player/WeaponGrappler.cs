﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {
    public class WeaponGrappler : MonoBehaviour {
/* 
        public LineRenderer ropeRenderer { get; private set; }
        public MeshRenderer renderer { get; private set; }
        public LayerMask layerMask { get; private set; }

        [HideInInspector]
        public Vector2 direction;

        public GameObject hangPoint { get; private set; }
        private GameObject hangPointClone;
        public Vector3 hookPoint { get; private set; }

        private Vector3 originalPosition;
        private Vector3 anchor;

        public float throwDistance = 5;
        public float throwSpeed = 0.4f;
        public float throwSpeedDiagonal = 0.28f;
        public float retractSpeed = 0.4f;
        public float retractSpeedDiagonal = 0.28f;
        public float swingSpeed = 0.2f;
        public float crawlSpeed = 0.1f;

        [HideInInspector]
        public float speed;
        [HideInInspector]
        public float length;

        private IEnumerator throwRoutine;
        private IEnumerator retractRoutine;
        private IEnumerator swingRoutine;
        private IEnumerator renderRopeRoutine;

        private int hangPointLayer;

        [HideInInspector]
        public float ropeLength;

        [HideInInspector]
        public float swingLength;

        public AriaController owner;
        private Transform _transform;

        private Collider2D collider;

        // Use this for initialization
        void Start () {

            this._transform = this.transform;
            this.originalPosition = this._transform.localPosition;
            this.owner = this._transform.parent.gameObject.GetComponent<AriaController>();
            
            this.direction = Vector2.zero;
            this.renderer = this.gameObject.GetComponent<MeshRenderer> ();
            this.ropeRenderer = this.gameObject.GetComponent<LineRenderer> ();
            this.hangPointClone = new GameObject ();
            this.hangPointLayer = LayerMask.GetMask ("HangPoint");
            this.collider = this.gameObject.GetComponent<Collider2D>();

            Debug.Log(this.ropeRenderer);

            this.DeActivate ();
        }

        public void Activate () {

            if (!this.owner.isGrappling) {
                
                Vector3 angles = Vector3.zero;

                this._transform.localPosition = this.originalPosition;
                
                string animName = "Aria_Grab";
                
                if (this.direction.x == 1 && this.direction.y == 1) {

                    angles.z = 45;
                    this.anchor.x += (this.throwSpeedDiagonal * 2);
                    animName = "Aria_Grab (+45)";

                } else if (this.direction.x == -1 && this.direction.y == 1) {

                    angles.z = 135;
                    this.anchor.x -= (this.throwSpeedDiagonal * 2);
                    animName = "Aria_Grab (+45)";

                } else if (this.direction.x == -1 && this.direction.y == 0) {

                    angles.z = 180;
                    this.anchor.x -= (this.throwSpeed * 2);

                } else if (this.direction.x == 0 && this.direction.y == 1) {

                    angles.z = 90;
                    this.anchor.y += (this.throwSpeed * 2);
                    animName = "Aria_Grab (+90)";

                } else
                    this.anchor.x += (this.throwSpeed * 2);

                //this.anchor = this._transform.position;

                this.collider.offset = Vector2.zero;

                this._transform.eulerAngles = angles;
                this.collider.enabled = true;

                this.owner.isGrappling = true;
                //this.owner.states.isJump = false;

                this.owner.PlayAnim (animName, AriaController.State.Grappling);

                this.throwRoutine = this.Throw ();
                this.StartCoroutine (this.throwRoutine);
            }
        }

        public void DeActivate () {
            this.Reset ();

            if (this.hangPoint != null) this.hangPoint.transform.parent = this.hangPointClone.transform.parent;

            this.hangPoint = null;
            this.hookPoint = Vector3.zero;
            this.collider.enabled = false;
            this.owner._transform.parent = null;
            this.owner._rigidbody.gravityScale = this.owner.gravityScale;

            if (this.renderRopeRoutine != null) this.StopCoroutine (this.renderRopeRoutine);
            this.renderRopeRoutine = null;
        }

        public void Reset () {
            this.ropeRenderer.enabled = false;
            this.renderer.enabled = false;
            this.length = 0;

            this.owner.isWallHanging = false;
            this.owner.isHanging = false;
            this.owner.isRetract = false;
            this.owner.isSwing = false;
            this.owner.isFreeze = false;
            this.owner.isGrappling = false;

            this._transform.parent = this.owner._transform;
            this._transform.localPosition = this.originalPosition;

            if (this.throwRoutine != null) this.StopCoroutine (this.throwRoutine);
            if (this.retractRoutine != null) this.StopCoroutine (this.retractRoutine);
            if (this.swingRoutine != null) this.StopCoroutine (this.swingRoutine);

            this.throwRoutine = null;
            this.retractRoutine = null;
            this.swingRoutine = null;
        }

        private IEnumerator Throw () {

            float modifier = 1;

            this.speed = this.direction.x != 0 && this.direction.y != 0 ? this.throwSpeedDiagonal : this.throwSpeed;

            this.length = 0;

            yield return new WaitForSeconds (0.2f);

            this.renderer.enabled = true;
            this.ropeRenderer.enabled = true;

            do {

                this.anchor = this.owner._transform.position + this.originalPosition;

                this.length += this.speed * modifier;

                this._transform.position += (new Vector3 (this.speed * this.direction.x, this.speed * this.direction.y, 0) * modifier);
                this.ropeRenderer.SetPositions (new Vector3[] { this.anchor, this._transform.position });

                RaycastHit2D[] hangPoints = Physics2D.BoxCastAll ((Vector2) this.anchor, new Vector2 (0.5f, 0.5f), this._transform.eulerAngles.z, new Vector2 (this.direction.x, this.direction.y), this.length, this.hangPointLayer);

                if (hangPoints.Length > 0) {
                    foreach (RaycastHit2D hangPoint in hangPoints) {

                        if (this.hangPoint == null || hangPoint.collider.name != this.hangPoint.name) {
                            this.hangPoint = hangPoint.collider.gameObject;
                            this.hookPoint = hangPoint.point;

                            this.Hook ();

                            break;
                        }
                    }
                }

                if (modifier == 1) modifier = length > this.throwDistance ? -1 : 1;

                yield return null;

            } while (this.length > 0);

            this.owner.PlayAnim ("Aria_Idle", AriaController.State.Idle);

            if (this.owner.isWallHanging) {

                this.Reset ();

                this.owner.isFreeze = true;
                this.owner.isHanging = false;
                this.owner.isWallHanging = true;
            } else
                this.DeActivate ();
        }

        public void Hook () {
            if (!this.owner.isRetract) {
                
                this.owner._rigidbody.gravityScale = 0;

                this._transform.position = this.hookPoint;


                this.anchor = this._transform.InverseTransformPoint (this.anchor);

                if (this.direction == Vector2.left || this.direction == Vector2.right) {
                    
                    this.collider.offset = new Vector2 (this._transform.localPosition.x / Mathf.Abs (this._transform.localScale.x), 0) * (this.direction.x * -1);
                    this.ropeLength = 0f;

                } else if ((this.direction.x == 1 || this.direction.x == -1) && this.direction.y == 1) {

                    float distance = this._transform.localPosition.x / this._transform.localScale.x;
                    this.collider.offset = new Vector2 (0 - Mathf.Sqrt (Mathf.Pow (distance, 2) * 2), 0);
                    this.ropeLength = Mathf.Abs (this.collider.offset.x) * (!this.owner.isGrounded ? 0.9f : 0.5f);

                } else {

                    this.collider.offset = new Vector2 (0 - this._transform.localPosition.y, 0);
                    this.ropeLength = Mathf.Abs (this.collider.offset.x) * (!this.owner.isGrounded ? 1f : 0.9f);
                }

                this.hangPointClone.transform.position = this.hangPoint.transform.position;
                this.hangPointClone.transform.parent = this.hangPoint.transform.parent;
                this.hangPoint.transform.parent = null;

                this.owner._transform.parent = this.hangPoint.transform;
                this._transform.parent = this.hangPoint.transform;

                this.swingLength = this.ropeLength;

                this.owner.isRetract = true;
                this.owner.isFreeze = true;

                if (this.throwRoutine != null) {
                    this.StopCoroutine (this.throwRoutine);
                    this.throwRoutine = null;
                }

            } else {

                if (this.swingRoutine != null) this.StopCoroutine (this.swingRoutine);

            }
            
            if (this.renderRopeRoutine == null) {
                
                this.renderRopeRoutine = this.RenderRope ();
                this.StartCoroutine (this.renderRopeRoutine);
            }

            this.retractRoutine = this.Retract ();
            this.StartCoroutine (this.retractRoutine);
        }

        private IEnumerator Retract () {

            this.speed = this.direction.x != 0 && this.direction.y != 0 ? this.retractSpeedDiagonal : this.retractSpeed;

            float length = 0;

            this.owner._transform.parent = this.hangPoint.transform;

            this.owner.PlayAnim ("Aria_Hanging", AriaController.State.Grappling_Hanging);

            //yield return null;

            do {
                this.collider.offset += new Vector2 (this.speed, 0);
                length = this.collider.offset.x;

                this.owner._transform.position = this.collider.bounds.center - ((Vector3) this.owner.GetComponent<Collider>().offset * 2);

                yield return new WaitForFixedUpdate ();

            } while (length < (0 - this.ropeLength));

            if (this.ropeLength > 0) {
                this.owner.isRetract = false;
                this.retractRoutine = null;

                if (this.direction != Vector2.up) {

                    this.swingRoutine = (this.direction == Vector2.left || this.direction == Vector2.right) ? this.Swing (4) : this.Swing ();
                    this.StartCoroutine (this.swingRoutine);
                } else
                    this.doHanging = true;

            } else {
                this.Reset ();

                this.doWallHanging = true;

                this.owner.isFreeze = true;
            }
        }

        private IEnumerator Swing (float amplitude = 3f, bool isHanging = false) {
            this.owner.isSwing = true;

            int progressScaler = 1;
            int scaler = (int) this.direction.x;
            int currentAcceleration = 0;
            bool doAcceleration = false;

            float progress = 0f;
            float swingMovement = 0f;

            this.speed = this.swingSpeed;

            if (this.direction.x == 1) {

                this.owner.PlayAnim ("Aria_Swing_Right", AriaController.State.Grappling_Swing_Right, isHanging ? 0 : 0.9f);
                this.owner.UpdateSprite(1);
            } 
            else if (this.direction.x == -1) { 
                
                this.owner.PlayAnim ("Aria_Swing_Left", AriaController.State.Grappling_Swing_Left, isHanging ? 0 : 0.9f); 
                this.owner.UpdateSprite(-1);
            }

            float a = Time.time;

            while (this.owner.isSwing) {
                
                AnimatorStateInfo animatorStateInfo = this.owner._animator.GetCurrentAnimatorStateInfo (0);

                Vector3 angles = this._transform.eulerAngles;
                float normalizedTime = animatorStateInfo.normalizedTime;

                if (!isHanging) {
                    
                    float input = this.owner.player.GetAxisRaw("Move Horizontal");
                    int acceleration = input > 0 ? 1 : (input < 0 ? -1 : 0); 

                    if (acceleration != currentAcceleration) {
                        currentAcceleration = acceleration;

                        if (scaler == acceleration && progressScaler == 1) {

                            doAcceleration = true;
                            this.speed *= 4;
                            normalizedTime = 0.9f;
                        }

                    } else if (scaler == 0) {

                        scaler = acceleration;
                    }

                    if (acceleration == 1 && (animatorStateInfo.IsName ("Aria_Swing_Left") || doAcceleration)) {

                        this.owner.PlayAnim ("Aria_Swing_Right", AriaController.State.Grappling_Swing_Right, isHanging ? 0 : 0.9f);
                        this.owner.UpdateSprite(1);

                    } else if (acceleration == -1 && (animatorStateInfo.IsName ("Aria_Swing_Right") || doAcceleration)) {

                        this.owner.PlayAnim ("Aria_Swing_Left", AriaController.State.Grappling_Swing_Left, isHanging ? 0 : 0.9f); 
                        this.owner.UpdateSprite(-1);
                    }
                }

                if (progressScaler == 1 && progress < amplitude) progress += 0.1f * this.speed;
                else if (progressScaler == -1) progress -= 0.1f * this.speed;

                swingMovement = Mathf.Lerp (0, 10, progress / 10) * scaler;

                if ((angles.z < 90 && angles.z + swingMovement > 90) || (angles.z > 90 && angles.z + swingMovement < 90) || angles.z == 90) {
                    progressScaler = -1;

                    if (doAcceleration == true) {

                        //amplitude = amplitude < 5f ? amplitude + 0.25f : amplitude;
                        amplitude = 3.5f;
                        //progress = amplitude + 3;
                        progress = 8;
                        doAcceleration = false;
                        //this.owner.animator.GetCurrentAnimatorStateInfo(0).speed = 4;
                    } else
                        progress = amplitude;

                    //this.owner.PlayAnim(this.owner.isFacingRight ? "Aria_Swing_Right" : "Aria_Swing_Left", 0, true);
                }

                if (swingMovement == 0) {

                    if (angles.z < 90 || angles.z > 315) scaler = 1;
                    else if (angles.z > 90 && angles.z < 225) scaler = -1;

                    //if(doAcceleration == true) doAcceleration = false;
                    //else { amplitude = amplitude > 0 ? amplitude - 0.5f : amplitude; }

                    progressScaler = 1;
                    progress = 0;

                    currentAcceleration = 0;

                    this.speed = this.swingSpeed;

                    Debug.Log (a - Time.time);
                    a = Time.time;

                    //this.owner.animator.GetCurrentAnimatorStateInfo(0).speed = 1.41f;

                    isHanging = false;

                }

                //if (angles.z < 0) angles.z += 360;

                angles.z += swingMovement;

                this._transform.eulerAngles = angles;

                this.owner._transform.position = this.collider.bounds.center - ((Vector3) this.owner.GetComponent<Collider>().offset * 2);

                yield return new WaitForFixedUpdate ();
            }

            this.swingRoutine = null;
        }

        private IEnumerator RenderRope () {
            while (true) {

                this.hangPoint.transform.position = this.hangPointClone.transform.position;

                this.ropeRenderer.SetPositions (new Vector3[] { this.collider.bounds.center, this._transform.position });
                yield return null;
            }
        }

        public void Crawl (bool goUp = false) {

            Debug.Log (this.collider.offset.x + " - " + this.crawlSpeed + " - " + this.swingLength);

            if (goUp) {

                if (this.collider.offset.x + this.crawlSpeed > 0) {

                    this.collider.offset = Vector2.zero;

                    this.doWallHanging = true;
                } else
                    this.collider.offset = new Vector2 (this.collider.offset.x + this.crawlSpeed, 0);
            } else {
                if (!this.owner.isHanging) this.doWallHanging = false;

                float maxLength = 0 - this.swingLength;

                if (this.collider.offset.x - this.crawlSpeed <= maxLength) this.collider.offset = new Vector2 (maxLength, 0);
                else { this.collider.offset = new Vector2 (this.collider.offset.x - this.crawlSpeed, 0); }
            }

            this.owner._transform.position = this.collider.bounds.center - ((Vector3) this.owner.GetComponent<Collider>().offset * 2);
        }

        public void Push (bool isLeft = false) {

            this.owner.isHanging = false;

            this.direction = isLeft ? Vector2.left : Vector2.right;
            this.direction.y = 1;

            this.swingRoutine = this.Swing (3.5f, true);
            this.StartCoroutine (this.swingRoutine);
        }

        public bool doWallHanging {
            get { return this.owner.isWallHanging; }
            set {
                if (this.owner.isWallHanging != value) {

                    this.owner.isWallHanging = value;
                    this.owner.isHanging = !this.owner.isWallHanging;
                }
            }
        }

        public bool doHanging {
            get { return this.owner.isHanging; }
            set {
                if (!this.owner.isHanging && value) this.swingLength = Mathf.Abs (this.collider.offset.x);

                this.owner.isHanging = value;
            }
        }

     */   
    }
    
}