﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Room : MonoBehaviour {

		public int vCell = 3;
		public int hCell = 3;

		public int x;
		public int y;

        public bool use = false;
        public Vector3 startPoint = new Vector3(3, 1, 0); //(0,0) is left bottom of the map

        public Sprite darkSprite;

		[HideInInspector]
		public Body body;
        [HideInInspector]
        public Body darkBody;

        private Dictionary<string, Gate> _gates = new Dictionary<string, Gate>();
        private Dictionary<string, Hold> _holds = new Dictionary<string, Hold>();
        private Dictionary<string, Wall> _wall = new Dictionary<string, Wall>();
        private Dictionary<string, AiPoint> _aiPoints = new Dictionary<string, AiPoint>();

        public void Awake () {
			
            string name = "(" + this.x + "," + this.y + ")";
            this.gameObject.name = name;
            
            if(!Stage.rooms.ContainsKey(name)) Stage.rooms.Add(name, this);
			
		    this.body = new Body(this.gameObject);
            this.body.size = new Vector3(this.hCell * Root.cellWidth, this.vCell * Root.cellHeight);

            this.body.left = this.x * Root.cellWidth;
            this.body.bottom = this.y * Root.cellHeight;

            this.body.gameObject.SetActive(false);

            if (this.use) {

                Stage.currentRoom = this;

                foreach (KeyValuePair<string, Room> r in Stage.rooms) r.Value.use = false;

                this.use = true;
            }   

            if (this.darkSprite != null && this.darkBody == null) {

                Transform dark = this.body.transform.Find("Dark");

                if (dark == null) {

                    dark = new GameObject("Dark").transform;
                    dark.gameObject.AddComponent<SpriteRenderer>().sprite = this.darkSprite;

                    this.darkBody = new Body(dark.gameObject);
                    this.darkBody.parent = this.body;
                    this.darkBody.transform.localPosition = new Vector3(0, 0, 5);

                    this.darkBody.transform.localScale = new Vector3((this.body.width) / this.darkBody.width, (this.body.height) / this.darkBody.height, 0);

                } else
                    this.darkBody = new Body(dark.gameObject);
            }
        }

		public void Start () {
            
            /* 
            if (Root.player == null) {

                Root.player = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<Player>() as Player;
                Root.player.Awake();
                Root.player.Start();
            }

            if (this.use) Root.player.unit.position = new Vector3(this.body.left + this.startPoint.x, this.body.bottom + this.startPoint.y, 0);
            */
        }
        
        public void AddGate(Gate gate) {

            if (!this._gates.ContainsKey(gate.name)) this._gates.Add(gate.gameObject.name, gate);
        }

        public Wall GetWall(string name) {

            return !this._wall.ContainsKey(name) ? null : this._wall[name];
        }

        public void AddWall(Wall wall) {

            if(!this._wall.ContainsKey(wall.name)) this._wall.Add(wall.name, wall);
        }
        
        public Hold GetHold(string name) {

            return !this._holds.ContainsKey(name) ? null : this._holds[name];
        }

        public void AddHold(Hold hold) {

            if (!this._holds.ContainsKey(hold.name)) this._holds.Add(hold.gameObject.name, hold);
        }

        public AiPoint GetAiPoint(string name) {

            return !this._aiPoints.ContainsKey(name) ? null : this._aiPoints[name];
        }

        public void AddAiPoint(AiPoint aiPoint) {

            if (!this._aiPoints.ContainsKey(aiPoint.name)) this._aiPoints.Add(aiPoint.gameObject.name, aiPoint);
        }
    }
}