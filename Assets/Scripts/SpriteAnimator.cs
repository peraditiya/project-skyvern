﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{

    public class SpriteAnimator
    {

        private Dictionary<string, SpriteAnimation> _animations = new Dictionary<string, SpriteAnimation>();
        private Sprite[] _sprites = new Sprite[0];
        private string _currentAnimationName = "";
        private SpriteAnimation _currentAnimation;
        private Body _body;
        private TimeUtil _timer = new TimeUtil();

        public static int ANIMATION_IS_DELAYED = 2;
        public static int ANIMATION_IS_RUNNING = 1;
        public static int ANIMATION_IS_DONE = 0;

        public int currentIndex = 0;

        public string defaultAnimation = "";

        public SpriteAnimator(Body body, string spriteSheet = "")
        {

            if (spriteSheet != "") this.spriteSheet = spriteSheet;

            this._animations = new Dictionary<string, SpriteAnimation>();
            this._body = body;
            this._timer.phaseGap = 0.02f;

            this._animations.Add("_DIRECT_", null);
        }

        public bool IsAnimationExist(string name)
        {

            return this._animations.ContainsKey(name);
        }

        public SpriteAnimation CreateAnimation(string name)
        {

            if (!this.IsAnimationExist(name)) this._animations.Add(name, new SpriteAnimation());

            return this._animations[name];
        }

        public SpriteAnimation GetAnimation(string name)
        {

            SpriteAnimation ret = null;

            if (this.IsAnimationExist(name)) ret = this._animations[name];

            return ret;
        }

        public SpriteAnimation GetCurrentAnimation()
        {

            return this.GetAnimation(this._currentAnimationName);
        }

        public int Play(string animationName = "")
        {

            int ret = SpriteAnimator.ANIMATION_IS_DELAYED;

            if (this._currentAnimation != null)
            {
                if (this._timer.IsPhasePassed() || (animationName != "" && this.currentAnimation != animationName))
                {

                    this.currentAnimation = animationName;

                    if (this._currentAnimation.isAnimationComplete)
                    {

                        if (!this._currentAnimation.loop) this.currentAnimation = this.defaultAnimation;

                        ret = SpriteAnimator.ANIMATION_IS_DONE;

                    }
                    else
                        ret = SpriteAnimator.ANIMATION_IS_RUNNING;

                    int currentFrameIndex = this._currentAnimation.frame;

                    Sprite currentSprite = this._sprites[currentFrameIndex];

                    this._body.renderer.sprite = currentSprite;

                    this.currentIndex++;
                }
            }

            return ret;
        }

        public void PlayFrame(int frameId)
        {

            if (this._currentAnimation.currentIndex != frameId && frameId < this._currentAnimation.frameLength)
            {

                this._currentAnimation.currentIndex = frameId;

                this._body.renderer.sprite = this._sprites[this._currentAnimation.GetFrame(frameId)];
            }
        }

        public void PlaySprite(int spriteId)
        {

            this.currentAnimation = "_DIRECT_";
            this._body.renderer.sprite = this._sprites[spriteId];
        }

        public bool IsCurrentAnimationComplete()
        {

            return this._currentAnimation.isAnimationComplete;
        }

        public void Reset()
        {

            if (this._currentAnimationName != "") this._animations[this._currentAnimationName].Reset();
        }

        public string currentAnimation
        {

            get
            {

                return this._currentAnimationName;
            }

            set
            {

                if (this._currentAnimationName != value && this._animations.ContainsKey(value))
                {

                    this._currentAnimationName = value;
                    this._currentAnimation = this._animations[this._currentAnimationName];

                    this.currentIndex = 0;
                    
                    if (this._currentAnimation != null)
                    {

                        this._currentAnimation.Reset();
                        this._timer.phaseGap = this._currentAnimation.delay;
                    }
                }
            }
        }

        public string spriteSheet
        {

            get { return spriteSheet; }

            set { this._sprites = Resources.LoadAll<Sprite>(value); }
        }
    }
}