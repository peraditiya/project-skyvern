﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class StaticInput
{
	private static InputManager _manager=null;
	public static InputManager inputManager
	{
		get
		{
			if(_manager==null)
			_manager=GameObject.Instantiate<InputManager>(Resources.Load<InputManager>("InputManager"));

			return _manager;
		}
	}
}