﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IHE_PlayerUI : MonoBehaviour {
	public Image healthBar;
	public Image energyBar;

	private float initHealth;
	private float initEnergy;

	private float initHealthY;
	private float initEnergyY;

	void Start(){
		InitBar();
	}

	void InitBar(){
		initHealth = healthBar.rectTransform.sizeDelta.x;
		initEnergy = energyBar.rectTransform.sizeDelta.x;
		initHealthY = healthBar.rectTransform.sizeDelta.y;
		initEnergyY = energyBar.rectTransform.sizeDelta.y;
	}

	public void UpdateHealthBar (float amount, float max){
		healthBar.rectTransform.sizeDelta  = new Vector2(amount / max * initHealth, initHealthY);
	}

	public void UpdateEnergyBar (float amount, float max){
		energyBar.rectTransform.sizeDelta  = new Vector2(amount / max * initEnergy, initEnergyY);
	}
}
