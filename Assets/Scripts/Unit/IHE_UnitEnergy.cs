﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class IHE_UnitEnergy : MonoBehaviour
    {

		public IHE_PlayerUI playerUI;
		public float maxEnergy = 100f;
        public float currentEnergy = 0;

        void Start()
        {
			currentEnergy = maxEnergy;
        }

        public void Decrease (float amount){
			currentEnergy -= amount;
			if(playerUI)
                playerUI.UpdateEnergyBar(currentEnergy, maxEnergy);
			if(currentEnergy < 0) {
				currentEnergy = 0;
			}
		}

		public bool IsEmpty(){
			return currentEnergy <= 0;
		}
    }

}