﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY
{
    public class IHE_UnitHealth : MonoBehaviour
    {
        [System.Serializable]
        public struct Tresshold {
            public bool started;
            public float healthMin;
        }
        public IHE_PlayerUI playerUI;
        public float maxHealth = 100f;
        public float currentHealth = 0;
        public bool isDead = false;
        public bool vulnerable = true;

        public Tresshold[] phaseTresshold;
        public int currentTresshold;

        void Start()
        {
            currentHealth = maxHealth;
        }

        public bool Hit(float damage)
        {
            if(!vulnerable) return false;

            StartCoroutine("ResetHit");
            currentHealth -= damage;
            if(playerUI)
                playerUI.UpdateHealthBar(currentHealth, maxHealth);

            if (currentHealth <= 0)
            {
                currentHealth = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Heal(float amount)
        {
            currentHealth += amount;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
        }

        IEnumerator ResetHit(){
            vulnerable = false;
            yield return new WaitForSeconds(.2f);
            vulnerable = true;
        }

        public void Reset()
        {
            currentHealth = maxHealth;
        }
    }
}
