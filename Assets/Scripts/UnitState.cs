﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class UnitState {

        public bool isJump;
        public bool isAttacking;
        public bool isGrappling;
        public bool isRetract;
        public bool isHanging;
        public bool isFacingBack;
        public bool isSwing;
        public bool isHit;
        public bool isHitEnemy;
        public bool isFalling;
        public bool isFreeze;
        public bool isBind;
        public bool isBound;
        public bool isPulling;
        public bool isPulled;

        public bool isAirStop;

        public bool isKnocked;

        public bool isJumpDown; //basically same as isFalling but it triggered by user initiate (jump + down)

        //for enemy
        public bool isOnTask;

        public UnitState() { }

        public void Reset() {

            this.isJump = this.isAttacking = this.isGrappling = this.isRetract = this.isHanging = this.isFacingBack = this.isHit  = this.isBind = this.isBound = this.isPulling = this.isPulled = this.isFreeze = this.isKnocked = this.isAirStop = false;
        }

        public bool isIdle {

            get {
                return !this.isAttacking && !this.isJump && !this.isGrappling && !this.isRetract && !this.isHanging && !this.isSwing && !this.isHit && !this.isFalling && !this.isBound && !this.isPulling && !this.isPulled && !this.isKnocked && !this.isAirStop;
            }
        }

        public bool isHooked {

            get {
                return this.isRetract || this.isHanging || this.isSwing;
            }
        }
    }
}