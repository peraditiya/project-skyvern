﻿using System;
using UnityEngine;
using UnityEditor;

namespace SKY {

    [RequireComponent(typeof(PolygonCollider2D))]
    [RequireComponent(typeof(MeshFilter))]
    [RequireComponent(typeof(MeshRenderer))]
    [ExecuteInEditMode]

    public class AutoMesh : MonoBehaviour {

        private Mesh _mesh;
        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;
        protected PolygonCollider2D _polygonCollider;

        public virtual void Start() {

            if (this._meshFilter == null) this._meshFilter = this.gameObject.GetComponent<MeshFilter>();

            if (this._meshRenderer == null) this._meshRenderer = this.gameObject.GetComponent<MeshRenderer>();

            if (this._polygonCollider == null) this._polygonCollider = this.gameObject.GetComponent<PolygonCollider2D>();
        }

        public virtual void Update() {

            if (this._meshFilter != null) {

                int pointCount = this._polygonCollider.GetTotalPointCount();

                Vector2[] points = this._polygonCollider.points;
                Vector3[] vertices = new Vector3[pointCount];
                Vector2[] uv = new Vector2[pointCount];

                for (int j = 0; j < pointCount; j++) {

                    Vector2 actual = points[j];
                    vertices[j] = new Vector3(actual.x, actual.y, 0);
                    uv[j] = actual;
                }

                int[] triangles = new Triangulator(points).Triangulate();

                Mesh mesh = new Mesh();
                mesh.vertices = vertices;
                mesh.triangles = triangles;
                mesh.uv = uv;

                this._meshFilter.mesh = mesh;
            }
        }

        public void Snap() {

            int counter = 0;
            for (int pathIndex = 0; pathIndex < this._polygonCollider.pathCount; pathIndex++) {

                Vector2[] path = this._polygonCollider.GetPath(pathIndex);

                for (long pointIndex = 0; pointIndex < path.Length; pointIndex++) {

                    counter++;
                    Vector2 point = path[pointIndex];
                    //path[pointIndex] = new Vector2(SKY.Util.SmoothDecimal(point.x), SKY.Util.SmoothDecimal(point.y));
                    path[pointIndex] = new Vector2(SKY.Util.SmoothDecimal(point.x), SKY.Util.SmoothDecimal(point.y));
                }

                this._polygonCollider.points = path;
                this._polygonCollider.SetPath(pathIndex, path);
            }
            print("Snapped " + counter + " points.");
        }
    }
}