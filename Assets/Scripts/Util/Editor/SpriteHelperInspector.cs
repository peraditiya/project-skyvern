﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteHelper))]
[CanEditMultipleObjects]

public class SpriteHelperInspector : Editor {
	public override void OnInspectorGUI()
    {
		DrawDefaultInspector();
        SpriteHelper myTarget = (SpriteHelper)target;
        
		if(GUILayout.Button("Change material")){
			myTarget.UpdateRenderer();
		}
    }
}
