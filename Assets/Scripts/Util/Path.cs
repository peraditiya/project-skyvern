﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Path : MonoBehaviour {

	public bool showLine = false;

	// private Vector3[] dots;
	Transform[] pos;
	
	private void OnEnable() {
		pos = transform.GetComponentsInChildren<Transform>();
	}

	private void OnDrawGizmos() {
		if(showLine){
			for (int i = 1; i < pos.Length-1; i++)
			{
				Gizmos.DrawLine(pos[i].position, pos[i+1].position);
			}
		}
	}


}
