﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ValueType{
	Constant, Random
}

[System.Serializable]
public class ShooterValueFloat{
	[SerializeField]
	public ValueType type = ValueType.Constant;
	[SerializeField]
	public float min = 0;
	[SerializeField]
	public float max = 0;

	public float GetValue(){
		return type == ValueType.Constant ? min : Random.Range(min, max);
	}

}

[System.Serializable]
public class ShooterValueInt{
	[SerializeField]
	public ValueType type;
	[SerializeField]
	public int min;
	[SerializeField]
	public int max;

	public int GetValue(){
		return type == ValueType.Constant ? min : Random.Range(min, max+1);
	}
}
