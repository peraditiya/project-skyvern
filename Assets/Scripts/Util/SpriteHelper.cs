﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteHelper : MonoBehaviour {

	public Material newMaterial;
	[SerializeField]
	public string newLayer;

	public void UpdateRenderer(){
		if(!newMaterial)
			return;
		
		foreach(SpriteRenderer sr in GetComponentsInChildren<SpriteRenderer>(true)){
			sr.sharedMaterial = newMaterial;
			sr.sortingLayerName = newLayer;
		}
	}
}
