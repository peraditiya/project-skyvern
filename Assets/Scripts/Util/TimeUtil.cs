using UnityEngine;

namespace SKY {

	public class TimeUtil {

		public float phaseGap = 0.02f;
		public bool IsPhaseStarted;

		private float _lastPhase; 
		
		public TimeUtil() {

			this.Reset();
		}

		public bool IsPhasePassed() {
			
			this._lastPhase = this._lastPhase == 0f ? this.phaseGap + 0.001f : this._lastPhase + UnityEngine.Time.deltaTime;

			bool ret = this._lastPhase >= this.phaseGap;

			if(ret) this._lastPhase = 0.001f;
			
			return ret;
		}

		public void Reset() {

			this._lastPhase = 0f;
			this.IsPhaseStarted = false;
		}
	}
}