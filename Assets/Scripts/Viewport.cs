﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

	public class Viewport : MonoBehaviour {

		private Camera _camera;
		private Vector2 _cameraImp = Vector2.zero;
		private Vector3 _previousPosition;

		//public Rect bound;

		public Body body;

		public List<Parallax> parallax;

		public List<Background> background;

		public bool isXAxisLocked = false;
		public bool isYAxisLocked = false;

        public void Awake() {
			
			if(GetComponent<Camera>() == null) this._camera = Camera.main;
			else { this._camera = GetComponent<Camera>(); }

			this.body = new Body(this._camera.gameObject);
			this.parallax   = new List<Parallax>();
			this.background = new List<Background>();

			Root.viewport = this;
        }

        public void Start() {

            this.body.size = new Vector2((this._camera.orthographicSize * ((float)Screen.width / (float)Screen.height) ), this._camera.orthographicSize) * 2f;
            
			this._cameraImp = new Vector2(this.body.width/Stage.currentRoom.body.width, this.body.height/Stage.currentRoom.body.height);
        }

        public void FixedUpdate() {

			Vector3 newCameraPosition = Root.player.unit.position;
			newCameraPosition.y = newCameraPosition.y + 3.5f;
			newCameraPosition.z = this.body.z;
			//newCameraPosition.y = Root.player.unit.state.isJump == true ? this.body.y : newCameraPosition.y + 3.5f;
			
			newCameraPosition = Vector3.Lerp(this.body.position, newCameraPosition, 5f * Time.deltaTime);

            /*
            if (!this.isXAxisLocked) {

                if(newCameraPosition.x + this.body.widthH > Stage.currentRoom.body.right) newCameraPosition.x = Stage.currentRoom.body.right - this.body.widthH;
				else if(newCameraPosition.x - this.body.widthH < Stage.currentRoom.body.left) newCameraPosition.x = Stage.currentRoom.body.left + this.body.widthH;
                
            } else
				newCameraPosition.x = 0;

			if(!this.isYAxisLocked) {
			
				if(newCameraPosition.y + this.body.heightH > Stage.currentRoom.body.top) newCameraPosition.y = Stage.currentRoom.body.top - this.body.heightH;
				else if(newCameraPosition.y - this.body.heightH < Stage.currentRoom.body.bottom) newCameraPosition.y = Stage.currentRoom.body.bottom + this.body.heightH;
			
			} else
				newCameraPosition.y = 0;
            */

			this.body.position = newCameraPosition;
			this._previousPosition = newCameraPosition;
		}
	}
}