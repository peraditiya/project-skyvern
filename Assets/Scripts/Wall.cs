﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKY {

    public class Wall : AutoMesh {
        
        private bool _isPlatform = false;

        [HideInInspector]
        public string name{
            
            get { return this.gameObject.name; }
        }
        
        [HideInInspector]
        public bool isPlatform {
            get { return this._isPlatform; }
        }

        [HideInInspector]
        public Collider2D collider{
            
            get { return this._polygonCollider; }
        }

        // Use this for initialization
        public override void Start() {

            base.Start();
            
            this._isPlatform = this.gameObject.GetComponent<PlatformEffector2D>() != null;

            this.gameObject.tag = "Wall";
            
            Transform parent = this.gameObject.transform.parent.parent;
            if(parent != null) parent.GetComponent<Room>().AddWall(this);
        }

        /* 
        public bool isActive {

            get { return Physics2D.GetIgnoreCollision(Root.player.unit.collider, this.collider); }
            set { Physics2D.IgnoreCollision(Root.player.unit.collider, this.collider, !value); }
        }
        */

        /* 
        public bool isActive {

            get { return this.collider.isTrigger; }
            set { this.collider.isTrigger = !value; }
        }
        */
    }
}